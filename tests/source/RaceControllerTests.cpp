// (C) king.com Ltd 2019
#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "DialogEditBLEConf.h"
#include "SoloRaceModel.h"
#include "catch.hpp"

TEST_CASE("Test 1 SoloRaceModel", "[SoloRaceModel]") { SoloRaceModel soloRace; }
TEST_CASE("Test 2 DialogBLE", "[DialogBLE]")
{
    DialogEditBLEConf bleConf;
    bleConf.exec();

    if (bleConf.result() == QDialog::Accepted) {
    }
}
