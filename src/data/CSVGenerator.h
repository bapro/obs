/*
 * CSVGenerator.h
 *
 *  Created on: 25 mai 2016
 *      Author: bapti
 */

#ifndef SRC_DATA_CSVGENERATOR_H_
#define SRC_DATA_CSVGENERATOR_H_

#include "RaceModel.h"
#include "Runner.h"
#include "Team.h"
#include "qstring.h"

class CSVGenerator {
public:
    CSVGenerator(const QString& p_fileToWrite);
    virtual ~CSVGenerator();

    void Generate(RaceModelPtr p_raceModel);

private:
    void Generate(const std::vector<RunnerPtr> p_runnerList);
    void Generate(const std::vector<TeamPtr> p_teamList);

    QString WriteRunner(const Runner& p_runner, const bool& p_writeBibNumber);

private:
    QString m_fileToWrite;
};

#endif /* SRC_DATA_CSVGENERATOR_H_ */
