/*
 * TagUnicityManager.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_TAGUNICITYMANAGER_H_
#define SRC_DATA_TAGUNICITYMANAGER_H_

#include <memory>
#include <mutex>
#include <qset.h>
#include <qstring.h>

class TagUnicityManager {
public:
    TagUnicityManager();
    virtual ~TagUnicityManager();

    void AddUsedTag(const QString& p_rfidTag);

    void RemoveFromUsedTag(const QString& p_rfidTag);

    bool IsAlreadyUsed(const QString& p_rfidTag) const;

    void Reset();

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TagUnicityManager> p_baseConf);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TagUnicityManager> p_baseConf);

private:
    int m_version;

    QSet<QString> m_tagContainer;
};

#endif /* SRC_DATA_TAGUNICITYMANAGER_H_ */
