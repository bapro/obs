/*
 * Settings.cpp
 *
 *  Created on: 2 juin 2016
 *      Author: bapti
 */

#include "qapplication.h"
#include <Settings.h>
#include <qdebug.h>

std::shared_ptr<Settings> Settings::m_instance = nullptr;
std::mutex Settings::m_mutex;

Settings::Settings()
    : m_fileSettings(QApplication::instance()->applicationName() + QString(".ini"), QSettings::IniFormat)
{
}

Settings::~Settings() {}

template <typename T> T Settings::value(const QString&, const T&)
{
    qDebug() << "ERROR: "
             << "Use unspecialized value(): T";
}

template <> QString Settings::value(const QString& p_key, const QString& p_defaultValue)
{
    QVariant var = p_defaultValue;
    var = m_fileSettings.value(p_key, var);
    m_fileSettings.setValue(p_key, var);
    return var.toString();
}

template <> int Settings::value(const QString& p_key, const int& p_defaultValue)
{
    QVariant var = QVariant(p_defaultValue);
    var = m_fileSettings.value(p_key, var);
    m_fileSettings.setValue(p_key, var);
    return var.toInt();
}

template <> bool Settings::value(const QString& p_key, const bool& p_defaultValue)
{
    QVariant var = QVariant(p_defaultValue);
    var = m_fileSettings.value(p_key, var);
    m_fileSettings.setValue(p_key, var);
    return var.toBool();
}

std::shared_ptr<Settings> Settings::instance()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    if (!m_instance) {
        m_instance = std::make_shared<Settings>();
    }
    return m_instance;
}
