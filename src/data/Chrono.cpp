/*
 * Chrono.cpp
 *
 *  Created on: 27 févr. 2016
 *      Author: baptiste
 */

#include <Chrono.h>
#include <QDataStream>

Chrono::Chrono()
    : m_version(1)
    , m_started(false)
    , m_timer()
    , m_timerUpdate()
{
    connect(&m_timerUpdate, SIGNAL(timeout()), this, SLOT(OnUpdateTime()));
}

Chrono::~Chrono() {}

void Chrono::Start()
{
    m_started = true;
    m_timer.start();
    m_timerUpdate.start(500);
}

void Chrono::Stop()
{
    m_started = false;
    m_timerUpdate.stop();
}

int Chrono::ElapsedTime() { return m_timer.elapsed(); }

bool Chrono::IsStarted() const { return m_started; }

void Chrono::SetStartedTime(const QTime& p_time)
{
    if (m_started) {
        m_timerUpdate.stop();
    }
    m_started = true;
    m_timer = p_time;
    m_timerUpdate.start(500);
}

void Chrono::OnUpdateTime()
{
    QTime time = QTime::fromMSecsSinceStartOfDay(m_timer.elapsed());
    emit UpdateRaceTime(time);
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const Chrono& p_chrono)
{
    p_out << p_chrono.m_version;
    p_out << p_chrono.m_started;
    p_out << p_chrono.m_timer;

    return p_out;
}
// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, Chrono& p_chrono)
{
    p_in >> p_chrono.m_version;
    p_in >> p_chrono.m_started;
    p_in >> p_chrono.m_timer;

    if (p_chrono.m_started) {
        p_chrono.m_timerUpdate.start(500);
        // TODO Start m_timer ?
    }

    return p_in;
}
