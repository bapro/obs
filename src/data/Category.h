/*
 * Category.h
 *
 *  Created on: 14 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_CATEGORY_H_
#define SRC_DATA_CATEGORY_H_
#pragma once

#include "qdatetime.h"
#include "qdebug.h"

namespace Category {

enum eCategory {
    CAT_POUSSIN, /*0*/
    CAT_BENJAMIN, /*0*/
    CAT_MINIME, /*0*/
    CAT_CADET, /*0*/
    CAT_JUNIOR, /*1*/
    CAT_ESPOIR, /*2*/
    CAT_SENIOR, /*3*/
    CAT_MASTER_0, /*4*/
    CAT_MASTER_1, /*5*/
    CAT_MASTER_2, /*6*/
    CAT_MASTER_3, /*7*/
    CAT_MASTER_4, /*8*/
    CAT_MASTER_5, /*9*/
    CAT_MASTER_6, /*9*/
    CAT_MASTER_7, /*9*/
    CAT_MASTER_8, /*9*/
    CAT_MASTER_9, /*9*/
    CAT_MASTER_10, /*9*/
    CAT_ALL /*9*/
};

inline QString CategoryToString(const eCategory& p_cat)
{
    QString ret;
    switch (p_cat) {
    case CAT_POUSSIN: {
        ret = "PO";
        break;
    }
    case CAT_BENJAMIN: {
        ret = "BE";
        break;
    }
    case CAT_MINIME: {
        ret = "MI";
        break;
    }
    case CAT_CADET: {
        ret = "CA";
        break;
    }
    case CAT_JUNIOR: {
        ret = "JU";
        break;
    }
    case CAT_ESPOIR: {
        ret = "ES";
        break;
    }
    case CAT_SENIOR: {
        ret = "SE";
        break;
    }
    case CAT_MASTER_0: {
        ret = "M0";
        break;
    }
    case CAT_MASTER_1: {
        ret = "M1";
        break;
    }
    case CAT_MASTER_2: {
        ret = "M2";
        break;
    }
    case CAT_MASTER_3: {
        ret = "M3";
        break;
    }
    case CAT_MASTER_4: {
        ret = "M4";
        break;
    }
    case CAT_MASTER_5: {
        ret = "M5";
        break;
    }
    case CAT_MASTER_6: {
        ret = "M6";
        break;
    }
    case CAT_MASTER_7: {
        ret = "M7";
        break;
    }
    case CAT_MASTER_8: {
        ret = "M8";
        break;
    }
    case CAT_MASTER_9: {
        ret = "M9";
        break;
    }
    case CAT_MASTER_10: {
        ret = "M10";
        break;
    }
    case CAT_ALL: {
        ret = "Scratch";
        break;
    }
    default: {
        qDebug() << "Unknown category asked: " << p_cat;
        ret = "Unknown";
        break;
    }
    }
    return ret;
}

inline eCategory CategoryFromDate(const QDate& p_birthDate)
{
    const auto currentDate = QDate::currentDate();

    int age = currentDate.year() - p_birthDate.year();
    if (currentDate.month() > 11) {
        age++;
    }

    eCategory cat = CAT_CADET;

    if (age >= 10 && age <= 11) {
        cat = CAT_POUSSIN;
    } else if (age >= 12 && age <= 13) {
        cat = CAT_BENJAMIN;
    } else if (age >= 14 && age <= 15) {
        cat = CAT_MINIME;
    } else if (age >= 16 && age <= 17) {
        cat = CAT_CADET;
    } else if (age >= 18 && age <= 19) {
        cat = CAT_JUNIOR;
    } else if (age >= 20 && age <= 22) {
        cat = CAT_ESPOIR;
    } else if (age >= 23 && age <= 34) {
        cat = CAT_SENIOR;
    } else if (age >= 35 && age <= 39) {
        cat = CAT_MASTER_0;
    } else if (age >= 40 && age <= 44) {
        cat = CAT_MASTER_1;
    } else if (age >= 45 && age <= 49) {
        cat = CAT_MASTER_2;
    } else if (age >= 50 && age <= 54) {
        cat = CAT_MASTER_3;
    } else if (age >= 55 && age <= 59) {
        cat = CAT_MASTER_4;
    } else if (age >= 60 && age <= 64) {
        cat = CAT_MASTER_5;
    } else if (age >= 65 && age <= 69) {
        cat = CAT_MASTER_6;
    } else if (age >= 70 && age <= 74) {
        cat = CAT_MASTER_7;
    } else if (age >= 75 && age <= 79) {
        cat = CAT_MASTER_8;
    } else if (age >= 80 && age <= 84) {
        cat = CAT_MASTER_9;
    } else if (age >= 85) {
        cat = CAT_MASTER_10;
    }
    return cat;
}

/**
 * Convenience function, string should respect the format: dd/M/yyyy
 */
inline eCategory CategoryFromDate(const QString& p_strAsDate)
{
    QString format("dd/MM/yyyy");
    return CategoryFromDate(QDate::fromString(p_strAsDate, format));
}

}
#endif /* SRC_DATA_CATEGORY_H_ */
