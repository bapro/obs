/*
 * Runner.h
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_RUNNER_H_
#define SRC_DATA_RUNNER_H_

#include "Category.h"
#include "ICompetitor.h"
#include <qdatetime.h>
#include <qstring.h>

class Runner : public ICompetitor {
public:
    Runner();
    Runner(const int& p_idDatabase);
    Runner(
        const int& p_idDatabase,
        const QString& p_firstName,
        const QString& p_lastName,
        const QString& p_email,
        const QString& p_phoneNumber,
        const eSexe& p_sexe,
        const QString& p_city,
        const QString& p_club,
        const QString& p_license,
        const QDate& p_birthDate,
        const int& p_bibNumber,
        const QString& p_chipNumber);
    virtual ~Runner() override;

    virtual QString toLogString() override;

    virtual void Merge(std::shared_ptr<ICompetitor> p_competitor) override;

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const Runner& p_runner);
    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, Runner& p_runner);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const std::shared_ptr<Runner> p_runner);
    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<Runner> p_runner);

public:
    int m_version;
    QString m_firstName;
    QString m_lastName;
    QString m_email;
    QString m_phoneNumber;
    QString m_city;
    QString m_club;
    QString m_license;
    QDate m_birthDate;
};

typedef std::shared_ptr<Runner> RunnerPtr;

#endif /* SRC_DATA_RUNNER_H_ */
