/*
 * CompetitorState.h
 *
 *  Created on: 25 mai 2017
 *      Author: bapti
 */

#ifndef SRC_DATA_COMPETITORSTATE_H_
#define SRC_DATA_COMPETITORSTATE_H_
#include <qdatastream.h>
#include <qstring.h>
class CompetitorState {
public:
    enum eState {
        NOT_CHECKED, // 0
        CHECKED, // 1
        RUNNING, // 2
        DISQUALIFIED, // 3
        ABORTED, // 4
        FINISHED // 5
    };

    CompetitorState();
    CompetitorState(const eState& p_state, const int& p_count);
    virtual ~CompetitorState();

    static QString StateToString(const eState& p_stateType);

    eState GetState() const;

    void SetState(eState p_state);

    int GetStateCount() const;

    void Update(const CompetitorState& p_compState);

    // ostream ICompetitor << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const CompetitorState& p_state);
    // istream ICompetitor >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, CompetitorState& p_state);

private:
    eState m_state;
    int m_count;
};

#endif /* SRC_DATA_COMPETITORSTATE_H_ */
