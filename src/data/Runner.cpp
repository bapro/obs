/*
 * Runner.cpp
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#include <QDataStream>
#include <qdebug.h>
#include <qvector.h>
#include <src/data/Runner.h>

Runner::Runner()
    : ICompetitor()
    , m_version(1)
    , m_firstName()
    , m_lastName()
    , m_email()
    , m_phoneNumber()
    , m_city()
    , m_club()
    , m_license()
    , m_birthDate()
{
}

Runner::Runner(const int& p_idDatabase)
    : ICompetitor(p_idDatabase)
    , m_version(1)
    , m_firstName()
    , m_lastName()
    , m_email()
    , m_phoneNumber()
    , m_city()
    , m_club()
    , m_license()
    , m_birthDate()
{
}

Runner::Runner(
    const int& p_idDatabase,
    const QString& p_firstName,
    const QString& p_lastName,
    const QString& p_email,
    const QString& p_phoneNumber,
    const eSexe& p_sexe,
    const QString& p_city,
    const QString& p_club,
    const QString& p_license,
    const QDate& p_birthDate,
    const int& p_bibNumber,
    const QString& p_chipNumber)
    : ICompetitor(p_idDatabase, p_bibNumber, p_chipNumber, p_sexe, Category::CategoryFromDate(p_birthDate))
    , m_version(1)
    , m_firstName(p_firstName)
    , m_lastName(p_lastName)
    , m_email(p_email)
    , m_phoneNumber(p_phoneNumber)
    , m_city(p_city)
    , m_club(p_club)
    , m_license(p_license)
    , m_birthDate(p_birthDate)
{
}

Runner::~Runner() {}

void Runner::Merge(std::shared_ptr<ICompetitor> p_competitor)
{
    RunnerPtr runr = std::static_pointer_cast<Runner>(p_competitor);

    if (m_firstName.isEmpty() && m_lastName.isEmpty()) {
        *this = *runr;
    }
}

QString Runner::toLogString()
{
    QString toLog = QString("[%1]\t%2\t%3").arg(m_bibNumber).arg(m_lastName).arg(m_firstName);
    return toLog;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const Runner& p_runner)
{
    p_out << static_cast<const ICompetitor&>(p_runner);
    p_out << p_runner.m_version;
    p_out << p_runner.m_firstName;
    p_out << p_runner.m_lastName;
    p_out << p_runner.m_email;
    p_out << p_runner.m_phoneNumber;
    p_out << p_runner.m_city;
    p_out << p_runner.m_club;
    p_out << p_runner.m_license;
    p_out << p_runner.m_birthDate;

    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, Runner& p_runner)
{
    p_in >> static_cast<ICompetitor&>(p_runner);
    p_in >> p_runner.m_version;
    p_in >> p_runner.m_firstName;
    p_in >> p_runner.m_lastName;
    p_in >> p_runner.m_email;
    p_in >> p_runner.m_phoneNumber;
    p_in >> p_runner.m_city;
    p_in >> p_runner.m_club;
    p_in >> p_runner.m_license;
    p_in >> p_runner.m_birthDate;

    return p_in;
    ;
}
// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const std::shared_ptr<Runner> p_runner) { return p_out << *p_runner; }

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<Runner> p_runner) { return p_in >> *p_runner; }
