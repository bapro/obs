/*
 * CompetitorState.cpp
 *
 *  Created on: 25 mai 2017
 *      Author: bapti
 */

#include <CompetitorState.h>

CompetitorState::CompetitorState()
    : m_state(NOT_CHECKED)
    , m_count(0)
{
}

CompetitorState::CompetitorState(const eState& p_state, const int& p_count)
    : m_state(p_state)
    , m_count(p_count)
{
}

CompetitorState::~CompetitorState() {}

QString CompetitorState::StateToString(const eState& p_stateType)
{
    QString ret;

    switch (p_stateType) {
    case NOT_CHECKED: {
        ret = QObject::tr("Missing");
        break;
    }
    case CHECKED: {
        ret = QObject::tr("Checked");
        break;
    }
    case RUNNING: {
        ret = QObject::tr("Running");
        break;
    }
    case DISQUALIFIED: {
        ret = QObject::tr("Disqualified");
        break;
    }
    case ABORTED: {
        ret = QObject::tr("Aborted");
        break;
    }
    case FINISHED: {
        ret = QObject::tr("Finished");
        break;
    }
    }
    return ret;
}

CompetitorState::eState CompetitorState::GetState() const { return m_state; }

int CompetitorState::GetStateCount() const { return m_count; }

void CompetitorState::SetState(eState p_state)
{
    if (m_state != p_state) {
        m_state = p_state;
        m_count++;
    }
}

void CompetitorState::Update(const CompetitorState& p_compState)
{
    if (m_count < p_compState.GetStateCount()) {
        m_count = p_compState.GetStateCount();
        m_state = p_compState.GetState();
    }
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const CompetitorState& p_state)
{
    p_out << p_state.m_state;
    p_out << p_state.m_count;
    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, CompetitorState& p_state)
{
    p_in >> (int&)p_state.m_state;
    p_in >> p_state.m_count;

    return p_in;
}
