/*
 * Team.cpp
 *
 *  Created on: 30 avr. 2016
 *      Author: baptiste
 */

#include <QDataStream>
#include <Team.h>
#include <qvector.h>

Team::Team()
    : ICompetitor()
    , m_version(1)
    , m_teamName()
    , m_teamMembers()
{
}

Team::Team(const int& p_databaseId)
    : ICompetitor(p_databaseId)
    , m_version(1)
{
    // TODO REMOVE and do it dynamically
    for (int i = 0; i < 3; ++i) {
        m_teamMembers.push_back(Runner());
    }
}

Team::Team(const QString& p_teamName, const int& p_idDatabase, const int& p_bibNumber)
    : ICompetitor(p_idDatabase, p_bibNumber, QString(), eSexe::ALL, Category::CAT_ALL)
    , m_version(1)
    , m_teamName(p_teamName)
    , m_teamMembers()
{
}

Team::~Team()
{
    // TODO Auto-generated destructor stub
}

void Team::ComputeSexeAndCategory()
{
    bool isFirst = true;
    ICompetitor::eSexe sexe = ICompetitor::ALL;
    Category::eCategory cat = Category::CAT_ALL;

    for (auto rnr : m_teamMembers) {
        if (isFirst) {
            sexe = rnr.GetSexe();
            cat = rnr.GetCategory();
            isFirst = false;
        } else {
            if (sexe != rnr.GetSexe()) {
                sexe = ICompetitor::ALL;
            }

            if (cat != rnr.GetCategory()) {
                cat = Category::CAT_ALL;
            }
        }
    }

    SetCategory(cat);
    SetSexe(sexe);
}

QString Team::toLogString()
{
    QString toLog = QString("[%1]\t%2").arg(m_bibNumber).arg(m_teamName);
    return toLog;
}

void Team::Merge(std::shared_ptr<ICompetitor> p_competitor)
{
    if (m_teamName.isEmpty()) {
        *this = *std::static_pointer_cast<Team>(p_competitor);
    }
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const Team& p_team)
{
    p_out << static_cast<const ICompetitor&>(p_team);
    p_out << p_team.m_version;
    p_out << p_team.m_teamName;
    p_out << QVector<Runner>::fromStdVector(p_team.m_teamMembers);
    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, Team& p_team)
{
    p_in >> static_cast<ICompetitor&>(p_team);
    p_in >> p_team.m_version;
    p_in >> p_team.m_teamName;

    QVector<Runner> vectToCopy;
    p_in >> vectToCopy;
    p_team.m_teamMembers = vectToCopy.toStdVector();

    return p_in;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const std::shared_ptr<Team> p_team) { return p_out << *p_team; }

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<Team> p_team) { return p_in >> *p_team; }
