/*
 * BaseStation.cpp
 *
 *  Created on: 16 mars 2016
 *      Author: baptiste
 */

#include <BaseStation.h>
#include <qdatastream.h>

BaseStation::BaseStation()
    : m_version(1)
    , m_name()
    , m_id(-1)
    , m_isCurrent(false)
{
}
BaseStation::BaseStation(const QString& p_name, const int& p_id)
    : m_version(1)
    , m_name(p_name)
    , m_id(p_id)
    , m_isCurrent(false)
{
}

BaseStation::~BaseStation() {}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const BaseStation& p_baseStation)
{
    p_out << p_baseStation.m_version;
    p_out << p_baseStation.m_name;
    p_out << p_baseStation.m_id;
    p_out << p_baseStation.m_isCurrent;

    return p_out;
}
// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, BaseStation& p_baseStation)
{
    p_in >> p_baseStation.m_version;
    p_in >> p_baseStation.m_name;
    p_in >> p_baseStation.m_id;
    p_in >> p_baseStation.m_isCurrent;

    return p_in;
}
