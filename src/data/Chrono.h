/*
 * Chrono.h
 *
 *  Created on: 27 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_CHRONO_H_
#define SRC_DATA_CHRONO_H_

#include "qobject.h"
#include <qdatetime.h>
#include <qtimer.h>

class Chrono : public QObject {
    Q_OBJECT
public:
    Chrono();
    virtual ~Chrono();

    void Start();

    void Stop();
    /**
     * return elsapsed time in seconds
     */
    int ElapsedTime();

    bool IsStarted() const;

    void SetStartedTime(const QTime& p_time);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const Chrono& p_chrono);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, Chrono& p_chrono);

public slots:
    void OnUpdateTime();

signals:
    void UpdateRaceTime(QTime);

private:
    int m_version;
    bool m_started;
    QTime m_timer;
    QTimer m_timerUpdate;
};

#endif /* SRC_DATA_CHRONO_H_ */
