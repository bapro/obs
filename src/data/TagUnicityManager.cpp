/*
 * TagUnicityManager.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include <TagUnicityManager.h>
#include <qdatastream.h>
#include <qdebug.h>

TagUnicityManager::TagUnicityManager()
    : m_version(1)
    , m_tagContainer()
{
}

TagUnicityManager::~TagUnicityManager() {}

void TagUnicityManager::AddUsedTag(const QString& p_rfidTag) { m_tagContainer.insert(p_rfidTag); }

void TagUnicityManager::RemoveFromUsedTag(const QString& p_rfidTag) { m_tagContainer.remove(p_rfidTag); }

bool TagUnicityManager::IsAlreadyUsed(const QString& p_rfidTag) const
{
    return m_tagContainer.find(p_rfidTag) != m_tagContainer.end();
}

void TagUnicityManager::Reset() { m_tagContainer.clear(); }

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TagUnicityManager> p_tagManager)
{
    p_out << p_tagManager->m_version;
    p_out << p_tagManager->m_tagContainer;

    return p_out;
}
// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TagUnicityManager> p_tagManager)
{
    p_in >> p_tagManager->m_version;
    p_in >> p_tagManager->m_tagContainer;

    return p_in;
}
