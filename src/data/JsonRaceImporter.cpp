/*
 * JsonRaceImporter.cpp
 *
 *  Created on: 29 mai 2017
 *      Author: bapti
 */

#include <JsonRaceImporter.h>
#include <RaceController.h>

#include <qfile.h>
#include <qjsonarray.h>
#include <qjsondocument.h>
#include <qjsonobject.h>

namespace {

const QString RACE_INFOS("raceInfos");
const QString RACE_TYPE("type");
const QString RACE_NAME("name");
const QString RACE_DATE("date");
const QString RACE_ID("id");
const QString RACE_SPLIT_NB("splitNumber");
const QString RACE_RUNNER_PER_TEAM("runnerPerTeam");
const QString RACE_RUNNERS_LIST("runners");
const QString RACE_TEAMS_LIST("teams");

const QString RACE_TYPE_SOLO("solo");
const QString RACE_TYPE_TEAM("team");

const QString JSON_ID = "id";
const QString JSON_BIB_NUMBER = "bib";
const QString JSON_FIRST_NAME = "firstName";
const QString JSON_LAST_NAME = "lastName";
const QString JSON_SEXE = "gender";
const QString JSON_BIRTH_DATE = "birthDate";
const QString JSON_TEL = "tel";
const QString JSON_EMAIL = "email";
const QString JSON_CITY = "city";
const QString JSON_CLUB = "club";
const QString JSON_LICENSE = "license";
const QString JSON_TEAM_NAME = "name";

const QString RUNNERS("runners");

}

JsonRaceImporter::JsonRaceImporter(const QString& p_fileName)
    : m_fileName(p_fileName)
    , m_jsonRootObj()
    , m_raceConf()
{
    QFile file(m_fileName);

    if (file.open(QIODevice::ReadOnly)) {
        QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
        m_jsonRootObj = jsonDoc.object();

        QJsonObject raceInfosObj = m_jsonRootObj.value(RACE_INFOS).toObject();
        int splitNb = raceInfosObj.value(RACE_SPLIT_NB).toString().toInt();

        if (raceInfosObj.value(RACE_TYPE).toString() == RACE_TYPE_SOLO) {
            m_raceConf.SetRaceType(Race::SOLO);
        } else if (raceInfosObj.value(RACE_TYPE).toString() == RACE_TYPE_TEAM) {
            m_raceConf.SetRaceType(Race::TEAM);
            m_raceConf.SetNbRunnerInTeam(raceInfosObj.value(RACE_RUNNER_PER_TEAM).toString().toInt());
        }
        m_raceConf.SetRaceDate(raceInfosObj.value(RACE_DATE).toString());
        m_raceConf.SetRaceName(raceInfosObj.value(RACE_NAME).toString());
        m_raceConf.SetRaceID(raceInfosObj.value(RACE_ID).toString().toInt());

        BaseStation baseStart(QObject::tr("Start Station"), RaceConfiguration::BASE_STATION_START);
        m_raceConf.AddBaseStation(baseStart);
        int baseId = 1;
        for (baseId = 1; baseId <= splitNb; ++baseId) {
            BaseStation base(QString::number(baseId), baseId);
            m_raceConf.AddBaseStation(base);
        }
        BaseStation base(QObject::tr("Finish Station"), ++baseId);
        m_raceConf.AddBaseStation(base);
    } else {
        qDebug() << "Open file FAILED : " << m_fileName;
    }
}

JsonRaceImporter::~JsonRaceImporter() {}

RaceConfiguration JsonRaceImporter::GetRaceConfiguration() { return m_raceConf; }

std::list<ICompetitorPtr> JsonRaceImporter::GetCompetitors()
{
    std::list<ICompetitorPtr> competitorsList;

    if (m_raceConf.GetRaceType() == Race::SOLO) {
        QJsonArray runners = m_jsonRootObj.value(RACE_RUNNERS_LIST).toArray();
        for (auto raw : runners) {
            QJsonObject obj = raw.toObject();
            RunnerPtr runr = CreateRunnerFromJson(obj);

            competitorsList.push_back(runr);
        }
    } else if (m_raceConf.GetRaceType() == Race::TEAM) {
        QJsonArray teams = m_jsonRootObj.value(RACE_TEAMS_LIST).toArray();
        for (auto raw : teams) {
            QJsonObject objTeam = raw.toObject();
            TeamPtr team = CreateTeamFromJson(objTeam);

            competitorsList.push_back(team);
        }
    }

    return competitorsList;
}

TeamPtr JsonRaceImporter::CreateTeamFromJson(const QJsonObject& p_obj)
{
    QString teamName = p_obj.value(JSON_TEAM_NAME).toString();
    int idTeam = p_obj.value(JSON_ID).toString().toInt();
    int bibNumber = p_obj.value(JSON_BIB_NUMBER).toString().toInt();

    // Build team
    TeamPtr team = std::make_shared<Team>(teamName, idTeam, bibNumber);

    QJsonObject runners = p_obj.value(RACE_RUNNERS_LIST).toObject();
    for (int i = 0; i < m_raceConf.GetNbRunnerInTeam(); ++i) {
        QString relay = "relay_" + QString::number(i + 1);
        RunnerPtr runr = CreateRunnerFromJson(runners.value(relay).toObject());
        team->AddTeamMember(*runr);
    }

    team->ComputeSexeAndCategory();

    return team;
}

RunnerPtr JsonRaceImporter::CreateRunnerFromJson(const QJsonObject& p_obj)
{
    QString format("dd/MM/yyyy");
    int idRunner = p_obj.value(JSON_ID).toString().toInt();
    QString firstName = p_obj.value(JSON_FIRST_NAME).toString();
    QString lastName = p_obj.value(JSON_LAST_NAME).toString();
    QString email = p_obj.value(JSON_EMAIL).toString();
    QString tel = p_obj.value(JSON_TEL).toString();
    QString sexe = p_obj.value(JSON_SEXE).toString();
    QString city = p_obj.value(JSON_CITY).toString();
    QString club = p_obj.value(JSON_CLUB).toString();
    QString license = p_obj.value(JSON_LICENSE).toString();
    QDate birthDate = QDate::fromString(p_obj.value(JSON_BIRTH_DATE).toString(), format);
    int bibNumber = p_obj.value(JSON_BIB_NUMBER).toString().toInt();

    Runner::eSexe eSexe = Runner::FEMALE;
    if (sexe.compare("male") == 0) {
        eSexe = Runner::MALE;
    }

    RunnerPtr runr = std::make_shared<Runner>(
        idRunner, firstName, lastName, email, tel, eSexe, city, club, license, birthDate, bibNumber, QString());
    return runr;
}
