/*
 * JsonRaceImporter.h
 *
 *  Created on: 29 mai 2017
 *      Author: bapti
 */

#ifndef SRC_DATA_JSONRACEIMPORTER_H_
#define SRC_DATA_JSONRACEIMPORTER_H_
#include <list>
#include <qjsonobject.h>
#include <qstring.h>

#include "RaceConfiguration.h"
#include "Runner.h"
#include "Team.h"

class RaceController;

class JsonRaceImporter {
public:
    JsonRaceImporter(const QString& p_fileName);
    virtual ~JsonRaceImporter();

    RaceConfiguration GetRaceConfiguration();

    std::list<ICompetitorPtr> GetCompetitors();

    RunnerPtr CreateRunnerFromJson(const QJsonObject& p_obj);
    TeamPtr CreateTeamFromJson(const QJsonObject& p_obj);

private:
    QString m_fileName;
    QJsonObject m_jsonRootObj;
    RaceConfiguration m_raceConf;
};

#endif /* SRC_DATA_JSONRACEIMPORTER_H_ */
