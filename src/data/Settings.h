/*
 * Settings.h
 *
 *  Created on: 2 juin 2016
 *      Author: bapti
 */

#pragma once

#include <memory>
#include <mutex>
#include <qsettings.h>

const QString SETTINGS_SKIN_STYLE = "skin/style";
const QString SETTINGS_LOG_CUSTOM = "log/custom";
const QString SETTINGS_LOG_FILE = "log/file";
const QString SETTINGS_HTTP_HASH = "http/hash";
const QString SETTINGS_HTTP_SERVER = "http/server";
const QString SETTINGS_DETECTOR_TYPE = "detector/type";
class Settings {
public:
    Settings();
    static std::shared_ptr<Settings> instance();
    virtual ~Settings();

    template <typename T> T value(const QString&, const T&);

private:
    static std::shared_ptr<Settings> m_instance;
    static std::mutex m_mutex;
    QSettings m_fileSettings;
};

