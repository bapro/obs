/*
 * Team.h
 *
 *  Created on: 30 avr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_TEAM_H_
#define SRC_DATA_TEAM_H_

#include "Runner.h"
#include <ICompetitor.h>
#include <vector>

class Team : public ICompetitor {
public:
    Team();
    Team(const int& p_databaseId);
    Team(const QString& p_teamName, const int& p_idDatabase, const int& p_bibNumber);

    virtual ~Team() override;

    void ComputeSexeAndCategory();

    QString toLogString() override;

    const std::vector<Runner>& GetTeamMembers() const { return m_teamMembers; }

    void SetTeamMembers(const std::vector<Runner>& teamMember) { m_teamMembers = teamMember; }

    void RemoveAllTeamMembers() { m_teamMembers.clear(); }

    const QString& GetTeamName() const { return m_teamName; }

    void SetTeamName(const QString& teamName) { m_teamName = teamName; }

    void AddTeamMember(const Runner& p_runner) { m_teamMembers.push_back(p_runner); }

    virtual void Merge(std::shared_ptr<ICompetitor> p_competitor) override;

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const Team& p_team);
    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, Team& p_team);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const std::shared_ptr<Team> p_team);
    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<Team> p_team);

private:
    int m_version;
    QString m_teamName;
    std::vector<Runner> m_teamMembers;
};

typedef std::shared_ptr<Team> TeamPtr;

#endif /* SRC_DATA_TEAM_H_ */
