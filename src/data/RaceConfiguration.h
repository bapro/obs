/*
 * RaceConfiguration.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_RACECONFIGURATION_H_
#define SRC_DATA_RACECONFIGURATION_H_

#include "BaseStation.h"
#include "RaceType.h"
#include <memory>
#include <mutex>
#include <qstring.h>
#include <vector>

class RaceConfiguration {
public:
    RaceConfiguration();
    virtual ~RaceConfiguration();

    int GetRaceID() const;

    void SetRaceID(const int& p_raceId);

    Race::RaceType GetRaceType() const;

    void SetRaceType(const Race::RaceType& p_type);

    const QString& GetRaceDate() const;

    void SetRaceDate(const QString& raceDate);

    const QString& GetRaceName() const;

    void SetRaceName(const QString& raceName);

    void AddBaseStation(const BaseStation& p_baseStation);

    std::vector<BaseStation> GetBaseStations() const;

    BaseStation GetFirstBastStation();

    BaseStation GetLastBastStation();

    void ClearBaseStations();

    BaseStation GetCurrentBaseStation() const;

    bool IsCurrentMainStation() const;

    void Reset();

    bool IsSafetyCheck() const;

    void SetSafetyCheck(bool safetyCheck);

    int GetInterval() const;

    void SetInterval(const int& p_interval);

    int GetNbRunnerInTeam() const;

    void SetNbRunnerInTeam(const int& p_nbRunner);

    static const int BASE_STATION_START;

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<RaceConfiguration> p_baseConf);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<RaceConfiguration> p_baseConf);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const RaceConfiguration& p_baseConf);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, RaceConfiguration& p_baseConf);

private:
    int m_version;
    int m_raceId;
    Race::RaceType m_type;
    QString m_raceName;
    QString m_raceDate;
    bool m_safetyCheck;
    int m_interval;
    int m_nbRunnerInTeam;

    std::vector<BaseStation> m_stations;
};

#endif /* SRC_DATA_RACECONFIGURATION_H_ */
