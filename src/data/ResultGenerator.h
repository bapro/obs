/*
 * ResultGenerator.h
 *
 *  Created on: 3 avr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_RESULTGENERATOR_H_
#define SRC_DATA_RESULTGENERATOR_H_

#include "RaceConfiguration.h"
#include "Runner.h"
#include "Team.h"
#include "qobject.h"

class ResultGenerator : public QObject {
    Q_OBJECT

public:
    ResultGenerator(const std::vector<ICompetitorPtr>& p_competitorDico, const RaceConfiguration& p_raceConfig);

    virtual ~ResultGenerator();

    void SaveToCsv(const QString& p_filePath);

private:
    void Build(const std::vector<RunnerPtr>& p_runnerDico);
    void Build(const std::vector<TeamPtr>& p_teamDico);

    template <typename T> void AddItemToTable(const T p_item, const int& p_rank);

    template <typename T> void GenerateTableHeader();

    template <typename T>
    void GenerateResult(
        const std::vector<T>& p_rDico,
        const Category::eCategory& p_category,
        const ICompetitor::eSexe& p_sexe);

    template <typename T>
    std::vector<T> ComputeRanking(
        const std::vector<T>& p_runners,
        const Category::eCategory& p_category,
        const ICompetitor::eSexe& p_sexe);

    void AddAllTimes(const ICompetitorPtr p_item);

    void GenerateTimesHeader();

private:
    QString m_content;
    QString m_contentFFA;
    RaceConfiguration m_raceConfig;
};

#endif /* SRC_DATA_RESULTGENERATOR_H_ */
