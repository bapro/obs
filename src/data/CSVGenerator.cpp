/*
 * CSVGenerator.cpp
 *
 *  Created on: 25 mai 2016
 *      Author: bapti
 */

#include "Category.h"
#include "qfile.h"
#include <CSVGenerator.h>

const QString SEPARATOR = ";";
const QString ENDLINE = "\n";

CSVGenerator::CSVGenerator(const QString& p_fileToWrite)
    : m_fileToWrite(p_fileToWrite)
{
}

CSVGenerator::~CSVGenerator() {}

void CSVGenerator::Generate(RaceModelPtr p_raceModel)
{
    switch (p_raceModel->GetRaceConfiguration().GetRaceType()) {
    case Race::TIME_TRIAL:
    case Race::SOLO: {
        std::vector<RunnerPtr> runners;
        for (ICompetitorPtr competitor : p_raceModel->GetAllItems()) {
            runners.push_back(std::static_pointer_cast<Runner>(competitor));
        }
        Generate(runners);
        break;
    }
    case Race::TEAM: {
        std::vector<TeamPtr> teams;
        for (ICompetitorPtr competitor : p_raceModel->GetAllItems()) {
            teams.push_back(std::static_pointer_cast<Team>(competitor));
        }
        Generate(teams);
        break;
    }
    case Race::UNDEFINED: {
        break;
    }
    }
}

void CSVGenerator::Generate(const std::vector<RunnerPtr> p_runnerList)
{
    QFile file(m_fileToWrite);

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);

        for (RunnerPtr rnr : p_runnerList) {
            out << WriteRunner(*rnr, true);
        }
    }
    file.close();
}

void CSVGenerator::Generate(const std::vector<TeamPtr> p_teamList)
{
    QFile file(m_fileToWrite);

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);

        for (TeamPtr tm : p_teamList) {
            out << QString::number(tm->GetBibNumber()) << SEPARATOR << tm->GetTeamName() << ENDLINE;
            for (auto rnr : tm->GetTeamMembers()) {
                out << WriteRunner(rnr, false);
            }
            out << ENDLINE;
        }
    }
    file.close();
}

QString CSVGenerator::WriteRunner(const Runner& p_runner, const bool& p_writeBibNumber)
{
    QString content;
    if (p_writeBibNumber) {
        content += QString::number(p_runner.GetBibNumber()) + SEPARATOR;
    }
    content += p_runner.m_lastName + SEPARATOR;
    content += p_runner.m_firstName + SEPARATOR;
    //	content += p_runner.m_phoneNumber + SEPARATOR;
    content += Category::CategoryToString(p_runner.GetCategory()) + SEPARATOR;
    content += p_runner.m_club + ENDLINE;

    return content;
}
