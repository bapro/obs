/*
 * BaseStation.h
 *
 *  Created on: 16 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_BASESTATION_H_
#define SRC_DATA_BASESTATION_H_

#include "qstring.h"

class BaseStation {
public:
    BaseStation();
    BaseStation(const QString& p_name, const int& p_id);
    virtual ~BaseStation();

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const BaseStation& p_baseStation);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, BaseStation& p_baseStation);

    int GetId() const { return m_id; }

    void SetId(int id) { m_id = id; }

    const QString& GetName() const { return m_name; }

    void SetName(const QString& name) { m_name = name; }

    bool IsCurrent() const { return m_isCurrent; }

    void SetIsCurrent(bool isCurrent) { m_isCurrent = isCurrent; }

private:
    int m_version;
    QString m_name;
    int m_id;
    bool m_isCurrent;
};

#endif /* SRC_DATA_BASESTATION_H_ */
