/*
 * ResultGenerator.cpp
 *
 *  Created on: 3 avr. 2016
 *      Author: baptiste
 */

#include "Sorting.h"
#include "qdebug.h"
#include "qfile.h"
#include "qtextdocument.h"
#include "qtextdocumentwriter.h"
#include <ResultGenerator.h>

ResultGenerator::ResultGenerator(
    const std::vector<ICompetitorPtr>& p_competitorDico,
    const RaceConfiguration& p_raceConfig)
    : m_raceConfig(p_raceConfig)
{
    switch (m_raceConfig.GetRaceType()) {
    case Race::SOLO: {
        std::vector<RunnerPtr> runners;
        for (ICompetitorPtr competitor : p_competitorDico) {
            runners.push_back(std::static_pointer_cast<Runner>(competitor));
        }
        Build(runners);
        break;
    }
    case Race::TEAM: {
        std::vector<TeamPtr> teams;
        for (ICompetitorPtr competitor : p_competitorDico) {
            teams.push_back(std::static_pointer_cast<Team>(competitor));
        }
        Build(teams);
        break;
    }
    case Race::TIME_TRIAL: {
        std::vector<RunnerPtr> runners;
        for (ICompetitorPtr competitor : p_competitorDico) {
            runners.push_back(std::static_pointer_cast<Runner>(competitor));
        }
        Build(runners);
        break;
    }
    case Race::UNDEFINED: {
        break;
    }
    }
}

void ResultGenerator::Build(const std::vector<RunnerPtr>& p_runnerDico)
{
    GenerateResult(p_runnerDico, Category::CAT_ALL, Runner::ALL);
    /*
    GenerateResult(p_runnerDico,Category::CAT_ALL, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_ALL, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_SENIOR, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_SENIOR, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_1, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_1, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_2, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_2, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_3, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_3, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_4, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_4, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_5, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_MASTER_5, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_ESPOIR, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_ESPOIR, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_CADET, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_CADET, Runner::FEMALE);
    GenerateResult(p_runnerDico,Category::CAT_JUNIOR, Runner::MALE);
    GenerateResult(p_runnerDico,Category::CAT_JUNIOR, Runner::FEMALE);
    */
}

void ResultGenerator::Build(const std::vector<TeamPtr>& p_teamDico)
{
    GenerateResult(p_teamDico, Category::CAT_ALL, Runner::ALL);
    //	GenerateResult(p_teamDico,Category::CAT_ALL, Runner::MALE);
    //	GenerateResult(p_teamDico,Category::CAT_ALL, Runner::FEMALE);
}

ResultGenerator::~ResultGenerator() {}

template <typename T>
void ResultGenerator::GenerateResult(
    const std::vector<T>& p_competitorDico,
    const Category::eCategory& p_category,
    const ICompetitor::eSexe& p_sexe)
{
    std::vector<T> data = ComputeRanking(p_competitorDico, p_category, p_sexe);

    if (!data.empty()) {
        QString title = m_raceConfig.GetRaceName() + " - " + Category::CategoryToString(p_category) + " "
            + Runner::SexeToString(p_sexe);

        /*
        m_content += "<h2 class=\"break\">" + title + "</h2>";
        m_content += "<table width=\"100%\">";

        */
        GenerateTableHeader<T>();
        int rank = 0;

        for (auto rnr : data) {
            AddItemToTable(rnr, ++rank);
        }

        //		m_content += "</table>";
    }
}

template <> void ResultGenerator::GenerateTableHeader<RunnerPtr>()
{
    m_content += ";Classement";
    m_content += ";Dossard";
    m_content += ";Nom";
    m_content += ";Prénom";
    m_content += ";Sexe";
    m_content += ";Date naissance";
    m_content += ";Catégorie";
    GenerateTimesHeader();
    m_content += "\n";
}
template <> void ResultGenerator::GenerateTableHeader<Team>()
{
    m_content += "Classement";
    m_content += ";Dossard";
    m_content += ";Equipe";
    GenerateTimesHeader();
    m_content += "\n";
}

template <typename T> void ResultGenerator::GenerateTableHeader() {}

template <typename T> void ResultGenerator::AddItemToTable(const T, const int&) {}

template <> void ResultGenerator::AddItemToTable(const RunnerPtr p_runer, const int& p_rank)
{
    QString sexeStr = "F";
    if (p_runer->GetSexe() == ICompetitor::MALE) {
        sexeStr = "M";
    } else if (p_runer->GetSexe() == ICompetitor::ALL) {
        sexeStr = "-";
    }

    m_content += ";" + QString::number(p_rank);
    m_content += ";" + QString::number(p_runer->GetBibNumber());
    m_content += ";" + p_runer->m_lastName;
    m_content += ";" + p_runer->m_firstName;
    m_content += ";" + sexeStr;
    m_content += ";" + p_runer->m_birthDate.toString("dd/MM/yyyy");
    m_content += ";" + Category::CategoryToString(p_runer->GetCategory());
    AddAllTimes(p_runer);
    m_content += "\n";

    // FFA

    QString time = p_runer->GetRaceTimeAtBaseStation(m_raceConfig.GetLastBastStation().GetId()).toString();
    time.remove(":");
    m_contentFFA += "" + QString::number(p_runer->GetBibNumber());
    m_contentFFA += "\t" + p_runer->m_license;
    m_contentFFA += "\t" + p_runer->m_lastName;
    m_contentFFA += "\t" + p_runer->m_firstName;
    m_contentFFA += "\t" + QString("FRA");
    m_contentFFA += "\t\t\t\t\t\t\t\t\t\t";
    m_contentFFA += "\t" + p_runer->m_birthDate.toString("yyyy");
    m_contentFFA += "\t" + Category::CategoryToString(p_runer->GetCategory());
    m_contentFFA += "\t" + sexeStr;
    m_contentFFA += "\t\t\t\t\t\t";
    m_contentFFA += "\t" + time;
    m_contentFFA += "\t" + QString::number(p_rank);
    m_contentFFA += "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
    m_contentFFA += "\n";
}

template <> void ResultGenerator::AddItemToTable(const TeamPtr p_team, const int& p_rank)
{
    QString time = p_team->GetRaceTime().toString();

    if (time.isEmpty() || !p_team->GetSplitTimeFromBaseStation(m_raceConfig.GetLastBastStation().GetId()).IsValid()) {
        time = CompetitorState::StateToString(p_team->GetState());
    }

    m_content += QString::number(p_rank);
    m_content += ";" + QString::number(p_team->GetBibNumber());
    m_content += ";" + p_team->GetTeamName();
    AddAllTimes(p_team);
    m_content += "\n";
}

void ResultGenerator::GenerateTimesHeader()
{
    for (auto base : m_raceConfig.GetBaseStations()) {
        if (base.GetId() != m_raceConfig.GetFirstBastStation().GetId()) {
            QString baseName = base.GetName();

            if (base.GetId() == m_raceConfig.GetLastBastStation().GetId()) {
                baseName = "Temps";
            }
            m_content += ";" + baseName;
        }
    }
}

void ResultGenerator::AddAllTimes(const ICompetitorPtr p_item)
{
    for (auto base : m_raceConfig.GetBaseStations()) {
        if (base.GetId() != m_raceConfig.GetFirstBastStation().GetId()) {
            QString time = p_item->GetRaceTimeAtBaseStation(base.GetId()).toString();
            if (time.isEmpty()) {
                time = "-";
            }

            if (m_raceConfig.GetLastBastStation().GetId() == base.GetId()
                && (p_item->GetState() == CompetitorState::ABORTED
                    || p_item->GetState() == CompetitorState::DISQUALIFIED || time == "-")) {
                time = CompetitorState::StateToString(p_item->GetState());
            }
            m_content += ";" + time;
        }
    }
}

void ResultGenerator::SaveToCsv(const QString& p_filePath)
{
    {
        qDebug() << "Save result to: " << p_filePath;
        QFile file(p_filePath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        out << m_content << "\n";
    }

    QFile templateFFA(":/template/rc/template/FFA-template.txt");
    templateFFA.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream contentTemplate(&templateFFA);

    QString FFA_ContentPath(p_filePath);
    FFA_ContentPath.remove(".csv");
    FFA_ContentPath.append("_FFA.txt");
    qDebug() << "Save FFA result to: " << FFA_ContentPath;
    QFile fileFFA(FFA_ContentPath);
    if (!fileFFA.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream outFFA(&fileFFA);
    outFFA << contentTemplate.readAll() << m_contentFFA << "\n";
}

template <typename T>
std::vector<T> ResultGenerator::ComputeRanking(
    const std::vector<T>& p_container,
    const Category::eCategory& p_category,
    const ICompetitor::eSexe& p_sexe)
{
    std::vector<T> tmpCont;

    for (T item : p_container) {
        if ((item->GetCategory() == p_category || Category::CAT_ALL == p_category)
            && (item->GetSexe() == p_sexe || Runner::ALL == p_sexe)) {
            tmpCont.push_back(item);
        }
    }

    std::sort(
        tmpCont.begin(),
        tmpCont.end(),
        CompareRaceTimeAtBaseStation(m_raceConfig.GetLastBastStation(), m_raceConfig.GetRaceType()));
    return tmpCont;
}
