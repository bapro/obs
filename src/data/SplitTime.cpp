/*
 * SplitTime.cpp
 *
 *  Created on: 28 févr. 2016
 *      Author: baptiste
 */

#include <QDataStream>
#include <SplitTime.h>

SplitTime::SplitTime()
    : m_version(1)
    , m_baseId(-1)
    , m_databaseId(-1)
    , m_chipNumber()
    , m_time()

{
}

SplitTime::SplitTime(const int& p_baseId, const QString& p_chipNumber)
    : m_version(1)
    , m_baseId(p_baseId)
    , m_databaseId(-1)
    , m_chipNumber(p_chipNumber)
    , m_time(QTime::currentTime())
{
}

SplitTime::SplitTime(const int& p_baseId, const int& p_databaseId)
    : m_version(1)
    , m_baseId(p_baseId)
    , m_databaseId(p_databaseId)
    , m_chipNumber()
    , m_time(QTime::currentTime())
{
}

SplitTime::~SplitTime() {}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const SplitTime& p_split)
{
    p_out << p_split.m_version;
    p_out << p_split.m_baseId;
    p_out << p_split.m_databaseId;
    p_out << p_split.m_chipNumber;
    p_out << p_split.m_time;
    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, SplitTime& p_split)
{
    p_in >> p_split.m_version;
    p_in >> p_split.m_baseId;
    p_in >> p_split.m_databaseId;
    p_in >> p_split.m_chipNumber;
    p_in >> p_split.m_time;
    return p_in;
}
