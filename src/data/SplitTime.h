/*
 * SplitTime.h
 *
 *  Created on: 28 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DATA_SPLITTIME_H_
#define SRC_DATA_SPLITTIME_H_

#include "qdatetime.h"

class SplitTime {
public:
    SplitTime();
    SplitTime(const int& p_baseId, const QString& p_chipNumber);
    SplitTime(const int& p_baseId, const int& p_databaseId);
    virtual ~SplitTime();

    int GetBaseId() const { return m_baseId; }

    int GetDatabaseId() const { return m_databaseId; }

    const QString& GetChipNumber() const { return m_chipNumber; }

    QTime GetTime() const { return m_time; }

    void SetTime(const QTime& p_time) { m_time = p_time; }

    bool IsValid() const { return m_baseId != -1; }

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const SplitTime& p_split);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, SplitTime& p_split);

private:
    int m_version;
    int m_baseId;
    int m_databaseId;
    QString m_chipNumber;
    QTime m_time;
};

#endif /* SRC_DATA_SPLITTIME_H_ */
