/*
 * RaceConfiguration.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include <RaceConfiguration.h>
#include <qdatastream.h>
#include <qvector.h>

const int RaceConfiguration::BASE_STATION_START = 0;

RaceConfiguration::RaceConfiguration()
    : m_version(1)
    , m_raceId(-1)
    , m_type(Race::UNDEFINED)
    , m_raceName()
    , m_raceDate()
    , m_safetyCheck()
    , m_interval(0)
    , m_nbRunnerInTeam(0)
    , m_stations()
{
}

RaceConfiguration::~RaceConfiguration() {}

int RaceConfiguration::GetRaceID() const { return m_raceId; }

void RaceConfiguration::SetRaceID(const int& p_raceId) { m_raceId = p_raceId; }

Race::RaceType RaceConfiguration::GetRaceType() const { return m_type; }

void RaceConfiguration::SetRaceType(const Race::RaceType& p_type) { m_type = p_type; }

const QString& RaceConfiguration::GetRaceDate() const { return m_raceDate; }

void RaceConfiguration::SetRaceDate(const QString& raceDate) { m_raceDate = raceDate; }

const QString& RaceConfiguration::GetRaceName() const { return m_raceName; }

void RaceConfiguration::SetRaceName(const QString& raceName) { m_raceName = raceName; }

void RaceConfiguration::AddBaseStation(const BaseStation& p_baseStation) { m_stations.push_back(p_baseStation); }

std::vector<BaseStation> RaceConfiguration::GetBaseStations() const { return m_stations; }
BaseStation RaceConfiguration::GetFirstBastStation()
{
    BaseStation base;
    if (m_stations.size() > 0) {
        base = *m_stations.begin();
    }
    return base;
}

BaseStation RaceConfiguration::GetLastBastStation()
{
    BaseStation base;
    if (m_stations.size() > 0) {
        base = m_stations.back();
    }
    return base;
}

void RaceConfiguration::Reset()
{
    m_raceName = QString();
    m_raceDate = QString();
    m_stations.clear();
    m_safetyCheck = false;
}

void RaceConfiguration::ClearBaseStations() { m_stations.clear(); }

BaseStation RaceConfiguration::GetCurrentBaseStation() const
{
    BaseStation retBase;

    if (IsCurrentMainStation()) {
        retBase = m_stations.back();
    } else {
        for (const BaseStation& base : m_stations) {
            if (base.IsCurrent()) {
                retBase = base;
                break;
            }
        }
    }

    return retBase;
}

bool RaceConfiguration::IsCurrentMainStation() const
{
    bool isMain = false;

    if (m_stations.begin()->IsCurrent() && m_stations.back().IsCurrent()) {
        isMain = true;
    }

    return isMain;
}

bool RaceConfiguration::IsSafetyCheck() const { return m_safetyCheck; }

void RaceConfiguration::SetSafetyCheck(bool safetyCheck) { m_safetyCheck = safetyCheck; }

int RaceConfiguration::GetInterval() const { return m_interval; }

void RaceConfiguration::SetInterval(const int& p_interval) { m_interval = p_interval; }
int RaceConfiguration::GetNbRunnerInTeam() const { return m_nbRunnerInTeam; }

void RaceConfiguration::SetNbRunnerInTeam(const int& p_nbRunner) { m_nbRunnerInTeam = p_nbRunner; }

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<RaceConfiguration> p_baseConf)
{

    return p_out << *p_baseConf;
}
// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<RaceConfiguration> p_baseConf)
{
    return p_in >> *p_baseConf;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const RaceConfiguration& p_baseConf)
{
    p_out << p_baseConf.m_version;
    p_out << p_baseConf.m_raceId;
    p_out << (int)p_baseConf.m_type;
    p_out << p_baseConf.m_raceName;
    p_out << p_baseConf.m_raceDate;
    p_out << p_baseConf.m_safetyCheck;
    p_out << p_baseConf.m_interval;
    p_out << p_baseConf.m_nbRunnerInTeam;

    QVector<BaseStation> vect2Serialize = QVector<BaseStation>::fromStdVector(p_baseConf.m_stations);
    p_out << vect2Serialize;

    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, RaceConfiguration& p_baseConf)
{
    int type = 0;

    p_in >> p_baseConf.m_version;
    p_in >> p_baseConf.m_raceId;
    p_in >> type;
    p_in >> p_baseConf.m_raceName;
    p_in >> p_baseConf.m_raceDate;
    p_in >> p_baseConf.m_safetyCheck;
    p_in >> p_baseConf.m_interval;
    p_in >> p_baseConf.m_nbRunnerInTeam;

    p_baseConf.m_type = (Race::RaceType)type;

    QVector<BaseStation> vectToCopy;
    p_in >> vectToCopy;
    p_baseConf.m_stations = vectToCopy.toStdVector();

    return p_in;
}
