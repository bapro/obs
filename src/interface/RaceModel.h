/*
 * RaceModel.h
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#ifndef SRC_RUNNERMODEL_H_
#define SRC_RUNNERMODEL_H_

#include <QAbstractTableModel>
#include <map>
#include <memory>
#include <mutex>
#include <qstring.h>

#include "BaseStation.h"
#include "Chrono.h"
#include "ICompetitor.h"
#include "RaceConfiguration.h"
#include "SplitTime.h"
#include "TagUnicityManager.h"
#include "qvector.h"
#include "vector"

class RaceModel : public QAbstractTableModel {
    Q_OBJECT

public:
    RaceModel(QObject* p_parent = 0);
    virtual ~RaceModel();

    /**
     * QAbstract Item Model Inheritance
     */
    virtual int rowCount(const QModelIndex&) const;

    virtual int columnCount(const QModelIndex&) const;

    virtual QVariant data(const QModelIndex&, int) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const = 0;

    virtual QVariant GetDataFromColumn(const ICompetitorPtr p_runr, const int& p_column) const = 0;

    virtual QVariant GetTextColorFromItem(const ICompetitorPtr p_runr) const;

    virtual ICompetitorPtr GetNextEmptyItem() = 0;

    virtual bool IsRaceStarted();

    virtual void SetStartedTime(const QTime& p_time);

    void ComputeRanking();

    void ComputeTagManager();

    int ComputeNextBib() const;

    int ComputeNextDbId() const;

    void AddFullList(const std::list<ICompetitorPtr>& p_list);

    void AddItem(const ICompetitorPtr p_item);

    void RemoveItem(ICompetitorPtr p_item);

    /**
     * Convenience function that retrieves the CompetitorPtr from index
     */
    void RemoveItem(const QModelIndex& p_index);

    /**
     * Bulk remove
     */
    void RemoveItems(const std::list<ICompetitorPtr>& p_competitorsToRemove);

    ICompetitorPtr GetItem(const QModelIndex& p_index);

    ICompetitorPtr GetItem(const QString& p_rfidTag);

    ICompetitorPtr GetItem(const int& p_technicalID);

    ICompetitorPtr GetNextItem(const int& p_technicalID);

    std::vector<ICompetitorPtr> GetAllItems() const;

    void UpdateItem(const ICompetitorPtr p_item);

    std::vector<ICompetitorPtr> GetAllItems();

    void AutoSetBibNumber(const int& p_bibNumberStart);

    /**
     * Auto generate p_count item in the model
     */
    virtual void AutoGenerateItems(const int& p_count) = 0;

    /**
     * Merge current model with the given one. Add SplitTime from the
     * other one.
     */
    virtual void Merge(std::shared_ptr<RaceModel> p_modelToMerge);

    /**
     * Find in the runner list if the rfid tag is used by a runner.
     * If found, then the runner state is set to CHECKED and return
     */
    ICompetitorPtr SafetyCheck(const QString& p_rfidTag);

    virtual void StartRace();

    virtual void StopRace();

    ICompetitorPtr AddSplitTimeToItem(SplitTime p_time);

    void Update();

    void Reset();

    int GetRank(const ICompetitorPtr p_runner, const BaseStation& p_currentBase) const;

    RaceConfiguration GetRaceConfiguration() const;

    const TagUnicityManager& GetTagUnicityManager() const;

    void SetRaceConfiguration(const RaceConfiguration& raceConfig);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const RaceModel& p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, RaceModel& p_raceModel);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<RaceModel> p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<RaceModel> p_raceModel);

signals:
    void UpdateRaceTime(QTime);

public slots:
    void OnRaceStop();
    void UpdateTimes(const std::list<SplitTime>& p_times);
    void UpdateStates(const std::map<int, CompetitorState>& p_states);

private:
    int m_version;

protected:
    RaceConfiguration m_raceConfig;
    Chrono m_chrono;
    TagUnicityManager m_tagManager;
    int m_colCount;
    std::vector<ICompetitorPtr> m_items;
    std::map<int, std::map<int, int>> m_ranking;
    std::vector<std::pair<QString, BaseStation>> m_dynamicBaseStationInfos;
    int m_nextDbId = 0;
};

typedef std::shared_ptr<RaceModel> RaceModelPtr;

#endif /* SRC_RUNNERMODEL_H_ */
