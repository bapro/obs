/*
 * RaceType.h
 *
 *  Created on: 24 sept. 2016
 *      Author: baptiste
 */

#ifndef SRC_INTERFACE_RACETYPE_H_
#define SRC_INTERFACE_RACETYPE_H_

namespace Race {

enum RaceType {
    UNDEFINED,
    SOLO, /*1*/
    TEAM, /*2*/
    TIME_TRIAL /*3*/
};

} // END NAMESPACE RACE

#endif /* SRC_INTERFACE_RACETYPE_H_ */
