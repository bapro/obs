/*
 * RaceModel.cpp
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#include "RaceModel.h"
#include "Runner.h"
#include "Sorting.h"
#include "Team.h"
#include "qapplication.h"
#include "qmessagebox.h"
#include <QDataStream>
#include <RaceModel.h>
#include <qdebug.h>
#include <qfile.h>
#include <qtextstream.h>

struct MatchDatabaseID {
    MatchDatabaseID(const int& p_dbID)
        : m_databaseID(p_dbID)
    {
    }

    bool operator()(const ICompetitorPtr p_competitor) const { return p_competitor->GetDatabaseId() == m_databaseID; }

private:
    const int m_databaseID;
};

struct MatchChipNumber {
    MatchChipNumber(const QString& p_chipNumber)
        : m_chipNumber(p_chipNumber)
    {
    }

    bool operator()(const ICompetitorPtr p_item) const { return p_item->GetChipNumber() == m_chipNumber; }

private:
    const QString m_chipNumber;
};

RaceModel::RaceModel(QObject* p_parent)
    : QAbstractTableModel(p_parent)
    , m_version(1)
    , m_raceConfig()
    , m_chrono()
    , m_tagManager()
    , m_colCount(0)
    , m_items()
    , m_ranking()
{
    connect(&m_chrono, &Chrono::UpdateRaceTime, this, &RaceModel::UpdateRaceTime);
}

RaceModel::~RaceModel() {}

int RaceModel::rowCount(const QModelIndex&) const { return (int)m_items.size(); }

int RaceModel::columnCount(const QModelIndex&) const
{
    return m_colCount + ((int)m_raceConfig.GetBaseStations().size() * 2 - 1);
}

QVariant RaceModel::data(const QModelIndex& p_index, int p_role) const
{
    QVariant ret(QVariant::Invalid);

    ICompetitorPtr competitor;
    if (p_index.row() <= (int)m_items.size()) {
        competitor = m_items.at((unsigned int)p_index.row());
    }

    if (p_role == Qt::DisplayRole && competitor) {
        ret = GetDataFromColumn(competitor, p_index.column());
    } else if (p_role == Qt::ForegroundRole && competitor) {
        ret = GetTextColorFromItem(competitor);
    }
    return ret;
}

QVariant RaceModel::GetTextColorFromItem(const ICompetitorPtr p_runr) const
{
    QColor color(Qt::black);
    if (p_runr->GetSexe() == ICompetitor::FEMALE) {
        color = QColor(Qt::magenta);
    }

    if (m_chrono.IsStarted()) {
        if (p_runr->GetState() == CompetitorState::NOT_CHECKED) {
            color = QColor(Qt::lightGray);
        } else if (p_runr->GetState() == CompetitorState::DISQUALIFIED) {
            color = QColor(Qt::red);
        } else if (p_runr->GetState() == CompetitorState::ABORTED) {
            color = QColor(Qt::darkYellow);
        }
    }

    return color;
}

bool RaceModel::IsRaceStarted() { return m_chrono.IsStarted(); }

void RaceModel::UpdateTimes(const std::list<SplitTime>& p_times)
{
    emit layoutAboutToBeChanged();
    for (auto splt : p_times) {
        int currentID = m_raceConfig.GetCurrentBaseStation().GetId();
        int spltID = splt.GetBaseId();
        if (((!m_raceConfig.IsCurrentMainStation() && spltID != currentID)
             || (m_raceConfig.IsCurrentMainStation() && spltID != currentID
                 && spltID != RaceConfiguration::BASE_STATION_START))
            && splt.IsValid()) {
            std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
                m_items.begin(), m_items.end(), MatchDatabaseID(splt.GetDatabaseId()));

            if (itSearch != m_items.end()) {
                (*itSearch)->UpdateSplitTime(splt);
            }
        }
    }
    ComputeRanking();
    emit layoutChanged();
}

void RaceModel::UpdateStates(const std::map<int, CompetitorState>& p_states)
{
    emit layoutAboutToBeChanged();
    std::map<int, CompetitorState>::const_iterator it = p_states.begin();

    for (it = p_states.begin(); it != p_states.end(); ++it) {
        std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
            m_items.begin(), m_items.end(), MatchDatabaseID(it->first));

        if (itSearch != m_items.end()) {
            (*itSearch)->GetCompetitorState().Update(it->second);
        }
    }
    emit layoutChanged();
}
void RaceModel::SetStartedTime(const QTime& p_time)
{
    m_chrono.Stop();
    emit layoutAboutToBeChanged();

    for (ICompetitorPtr item : m_items) {
        if ((m_raceConfig.IsSafetyCheck() && item->GetState() == CompetitorState::CHECKED)
            || !m_raceConfig.IsSafetyCheck() || !m_raceConfig.IsCurrentMainStation()) {
            item->SetState(CompetitorState::RUNNING);
            SplitTime splitStart(RaceConfiguration::BASE_STATION_START, item->GetChipNumber());
            splitStart.SetTime(p_time);

            item->UpdateSplitTime(splitStart);
        }
    }
    ComputeRanking();
    m_chrono.SetStartedTime(p_time);
    emit layoutChanged();
}

void RaceModel::ComputeRanking()
{
    std::vector<ICompetitorPtr> tmpItem = m_items;
    m_ranking.clear();

    for (auto base : m_raceConfig.GetBaseStations()) {
        m_ranking.insert(std::make_pair(base.GetId(), std::map<int, int>()));
        std::sort(tmpItem.begin(), tmpItem.end(), CompareRaceTimeAtBaseStation(base, m_raceConfig.GetRaceType()));

        int rank = 0;
        for (auto item : tmpItem) {
            if (item->GetSplitTimeFromBaseStation(base.GetId()).IsValid()) {
                std::pair<int, int> p = std::make_pair(item->GetDatabaseId(), ++rank);
                m_ranking.at(base.GetId()).insert(p);
            }
        }
    }
}

void RaceModel::ComputeTagManager()
{
    m_tagManager.Reset();
    for (ICompetitorPtr item : m_items) {
        m_tagManager.AddUsedTag(item->GetChipNumber());
    }
}

int RaceModel::ComputeNextBib() const
{
    int nextBib = 1;
    for (ICompetitorPtr item : m_items) {
        nextBib = std::max(nextBib, item->GetBibNumber() + 1);
    }
    return nextBib;
}

int RaceModel::ComputeNextDbId() const
{
    int nextId = 1;
    for (ICompetitorPtr item : m_items) {
        nextId = std::max(nextId, item->GetDatabaseId() + 1);
    }
    return nextId;
}

int RaceModel::GetRank(const ICompetitorPtr p_item, const BaseStation& p_currentBase) const
{
    int rank = -1;

    if (m_ranking.find(p_currentBase.GetId()) != m_ranking.end()) {
        std::map<int, int> baseStationRanking = m_ranking.at(p_currentBase.GetId());

        if (baseStationRanking.find(p_item->GetDatabaseId()) != baseStationRanking.end()) {
            rank = baseStationRanking.find(p_item->GetDatabaseId())->second;
        }
    }
    return rank;
}

RaceConfiguration RaceModel::GetRaceConfiguration() const { return m_raceConfig; }

const TagUnicityManager& RaceModel::GetTagUnicityManager() const { return m_tagManager; }

void RaceModel::AddFullList(const std::list<ICompetitorPtr>& p_list)
{
    for (ICompetitorPtr item : p_list) {
        AddItem(item);
    }
}

void RaceModel::AddItem(const ICompetitorPtr p_item)
{
    emit layoutAboutToBeChanged();
    m_items.push_back(p_item);
    m_tagManager.AddUsedTag(p_item->GetChipNumber());
    emit layoutChanged();
}

void RaceModel::RemoveItem(ICompetitorPtr p_item)
{
    // Remove based on the database ID
    std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
        m_items.begin(), m_items.end(), MatchDatabaseID(p_item->GetDatabaseId()));

    if (itSearch != m_items.end()) {
        emit layoutAboutToBeChanged();
        m_tagManager.RemoveFromUsedTag(p_item->GetChipNumber());
        m_items.erase(itSearch);
        emit layoutChanged();
    }
}

void RaceModel::RemoveItem(const QModelIndex& p_index)
{
    ICompetitorPtr competitorToRemove = GetItem(p_index);

    if (competitorToRemove) {
        RemoveItem(competitorToRemove);
    }
}

void RaceModel::RemoveItems(const std::list<ICompetitorPtr>& p_competitorsToRemove)
{
    emit layoutAboutToBeChanged();

    for (auto competitor : p_competitorsToRemove) {
        if (competitor) {
            std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
                m_items.begin(), m_items.end(), MatchDatabaseID(competitor->GetDatabaseId()));

            if (itSearch != m_items.end()) {
                m_tagManager.RemoveFromUsedTag(competitor->GetChipNumber());
                m_items.erase(itSearch);
            }
        }
    }

    emit layoutChanged();
}

ICompetitorPtr RaceModel::GetItem(const QModelIndex& p_index)
{
    ICompetitorPtr item;
    if (p_index.row() <= (int)m_items.size()) {
        item = m_items.at((unsigned int)p_index.row());
    }
    return item;
}

ICompetitorPtr RaceModel::GetItem(const QString& p_rfidTag)
{
    ICompetitorPtr item;
    std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
        m_items.begin(), m_items.end(), MatchChipNumber(p_rfidTag));
    if (itSearch != m_items.end()) {
        item = *itSearch;
    }
    return item;
}
ICompetitorPtr RaceModel::GetItem(const int& p_technicalID)
{
    ICompetitorPtr item;
    std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
        m_items.begin(), m_items.end(), MatchDatabaseID(p_technicalID));

    if (itSearch != m_items.end()) {
        item = *itSearch;
    }
    return item;
}

ICompetitorPtr RaceModel::GetNextItem(const int& p_technicalID)
{
    ICompetitorPtr nextItem;
    std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
        m_items.begin(), m_items.end(), MatchDatabaseID(p_technicalID));

    if (itSearch != m_items.end() && itSearch != std::prev(m_items.end())) {
        std::advance(itSearch, 1);
        nextItem = *(itSearch);
    }
    return nextItem;
}

std::vector<ICompetitorPtr> RaceModel::GetAllItems() const { return m_items; }

void RaceModel::UpdateItem(const ICompetitorPtr p_item)
{
    emit layoutAboutToBeChanged();

    std::vector<ICompetitorPtr>::iterator itFound = std::find_if(
        m_items.begin(), m_items.end(), MatchDatabaseID(p_item->GetDatabaseId()));

    if (itFound != m_items.end()) {
        m_tagManager.RemoveFromUsedTag((*itFound)->GetChipNumber());
        *itFound = p_item;
        m_tagManager.AddUsedTag(p_item->GetChipNumber());
    }
    ComputeRanking();
    emit layoutChanged();
}

std::vector<ICompetitorPtr> RaceModel::GetAllItems() { return m_items; }

void RaceModel::AutoSetBibNumber(const int& p_bibNumberStart)
{
    int firstBib = p_bibNumberStart;
    for (ICompetitorPtr item : m_items) {
        item->SetBibNumber(firstBib++);
    }
    Update();
}

void RaceModel::Merge(std::shared_ptr<RaceModel> p_modelToMerge)
{
    emit layoutAboutToBeChanged();
    int currentID = m_raceConfig.GetCurrentBaseStation().GetId();

    for (ICompetitorPtr item : m_items) {
        std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
            p_modelToMerge->m_items.begin(), p_modelToMerge->m_items.end(), MatchDatabaseID(item->GetDatabaseId()));

        if (itSearch != p_modelToMerge->m_items.end()) {
            ICompetitorPtr competitoMerge = (*itSearch);
            item->Merge(competitoMerge);
            for (auto split : competitoMerge->GetSplitTimes()) {
                if (((!m_raceConfig.IsCurrentMainStation() && split.GetBaseId() != currentID)
                     || (m_raceConfig.IsCurrentMainStation() && split.GetBaseId() != currentID
                         && split.GetBaseId() != RaceConfiguration::BASE_STATION_START))
                    && split.IsValid()) {
                    item->UpdateSplitTime(split);
                }
            }
        }
        //		else
        //		{
        //			AddItem(item);
        //		}
    }
    ComputeRanking();
    emit layoutChanged();
}

ICompetitorPtr RaceModel::SafetyCheck(const QString& p_rfidTag)
{
    ICompetitorPtr item;
    std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
        m_items.begin(), m_items.end(), MatchChipNumber(p_rfidTag));
    if (itSearch != m_items.end()) {
        (*itSearch)->SetState(CompetitorState::CHECKED);
        item = *itSearch;
    }

    return item;
}

void RaceModel::StartRace()
{
    emit layoutAboutToBeChanged();
    m_chrono.Start();

    for (ICompetitorPtr item : m_items) {
        if ((m_raceConfig.IsSafetyCheck() && item->GetState() == CompetitorState::CHECKED)
            || !m_raceConfig.IsSafetyCheck()) {
            item->SetState(CompetitorState::RUNNING);
            SplitTime splitStart(RaceConfiguration::BASE_STATION_START, item->GetChipNumber());
            item->ClearSplitTimes();
            item->AddSplitTime(splitStart);
        }
    }

    m_ranking.clear();
    for (auto base : m_raceConfig.GetBaseStations()) {
        std::pair<int, std::map<int, int>> p = std::make_pair(base.GetId(), std::map<int, int>());
        m_ranking.insert(p);
    }
    emit layoutChanged();
}

void RaceModel::StopRace()
{
    m_chrono.Stop();
    QMessageBox::information(QApplication::activeWindow(), tr("Race Stop"), tr("Race is stopped"));
}

ICompetitorPtr RaceModel::AddSplitTimeToItem(SplitTime p_time)
{
    ICompetitorPtr itemRet;
    if (m_chrono.IsStarted()) {
        std::vector<ICompetitorPtr>::iterator itSearch = std::find_if(
            m_items.begin(), m_items.end(), MatchChipNumber(p_time.GetChipNumber()));
        if (itSearch != m_items.end()) {
            // Safety check allow partial base station to add time
            if (!m_raceConfig.IsCurrentMainStation() || (*itSearch)->GetState() == CompetitorState::RUNNING) {
                (*itSearch)->AddSplitTime(p_time);

                // Check if it's the finish time
                if (m_ranking.find(p_time.GetBaseId()) != m_ranking.end()) {
                    qInfo() << m_raceConfig.GetRaceName() << " - Arrival: " << (*itSearch)->toLogString()
                            << (*itSearch)->GetRaceTime();
                    ComputeRanking();
                } else {
                    qDebug() << m_raceConfig.GetRaceName() << " - split: " << (*itSearch)->toLogString();
                }
                itemRet = *itSearch;
                Update();
            }
        } else {
            qDebug() << "Runner not found";
        }
    }
    return itemRet;
}

void RaceModel::Update()
{
    int rankNumber = 0;
    if (m_dynamicBaseStationInfos.size() != m_raceConfig.GetBaseStations().size() * 2) {
        m_dynamicBaseStationInfos.clear();
        for (auto base : m_raceConfig.GetBaseStations()) {
            m_dynamicBaseStationInfos.push_back(std::make_pair(base.GetName(), base));

            // Add rank only for split time base station and finish (not the start)
            if (base.GetId() != m_raceConfig.GetFirstBastStation().GetId()) {
                m_dynamicBaseStationInfos.push_back(
                    std::make_pair(tr("Rank") + QString(" ") + QString::number(++rankNumber), base));
            }
        }
    }
    emit layoutAboutToBeChanged();
    emit layoutChanged();
}

void RaceModel::Reset()
{
    m_items.clear();
    m_ranking.clear();
    m_chrono.Stop();
    m_dynamicBaseStationInfos.clear();
    Update();
}

void RaceModel::OnRaceStop() { StopRace(); }

void RaceModel::SetRaceConfiguration(const RaceConfiguration& raceConfig) { m_raceConfig = raceConfig; }

/**
 *
 * TEMPLATE & SERIALIZATION STUFF
 *
 * DO NOT TOUCH UNLESS YOU KNOW WHAT YOU'RE DOING (BLESS Kimi R.)
 *
 *
 *
 */
// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const RaceModel& p_raceModel)
{
    p_out << p_raceModel.m_version;
    p_out << p_raceModel.m_chrono;
    p_out << p_raceModel.m_colCount;
    return p_out;
}

QDataStream& operator>>(QDataStream& p_in, RaceModel& p_raceModel)
{
    p_in >> p_raceModel.m_version;
    p_in >> p_raceModel.m_chrono;
    p_in >> p_raceModel.m_colCount;

    p_raceModel.ComputeTagManager();
    p_raceModel.ComputeRanking();
    p_raceModel.Update();

    return p_in;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<RaceModel> p_raceModel) { return p_out << *p_raceModel; }

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<RaceModel> p_raceModel) { return p_in >> *p_raceModel; }
