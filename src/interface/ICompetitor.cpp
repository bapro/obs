/*
 * ICompetitor.cpp
 *
 *  Created on: 26 avr. 2016
 *      Author: baptiste
 */

#include "ICompetitor.h"
#include "qobject.h"
#include <QDataStream>
#include <qvector.h>

struct MatchBaseStationId {
    MatchBaseStationId(const int& p_baseStationID)
        : m_baseID(p_baseStationID)
    {
    }

    bool operator()(const SplitTime& p_splitTime) const { return p_splitTime.GetBaseId() == m_baseID; }

private:
    int m_baseID;
};

ICompetitor::ICompetitor()
    : m_version(1)
    , m_idDatabase(-1)
    , m_bibNumber(-1)
    , m_chipNumber()
    , m_sexe(MALE)
    , m_category()
    , m_state()
    , m_splitTimes()
{
}

ICompetitor::ICompetitor(const int& p_idDatabase)
    : m_version(1)
    , m_idDatabase(p_idDatabase)
    , m_bibNumber(-1)
    , m_chipNumber()
    , m_sexe(MALE)
    , m_category()
    , m_state()
    , m_splitTimes()
{
}

ICompetitor::ICompetitor(
    const int& p_idDatabase,
    const int& p_bibNumber,
    const QString& p_chipNumber,
    const eSexe& p_sexe,
    const Category::eCategory& p_category)
    : m_version(1)
    , m_idDatabase(p_idDatabase)
    , m_bibNumber(p_bibNumber)
    , m_chipNumber(p_chipNumber)
    , m_sexe(p_sexe)
    , m_category(p_category)
    , m_state()
    , m_splitTimes()
{
}

ICompetitor::~ICompetitor() {}

bool ICompetitor::IsValid() const { return m_idDatabase != -1; }

int ICompetitor::GetDatabaseId() const { return m_idDatabase; }

void ICompetitor::AddSplitTime(const SplitTime& p_split)
{
    std::vector<SplitTime>::iterator itSearch = std::find_if(
        m_splitTimes.begin(), m_splitTimes.end(), MatchBaseStationId(p_split.GetBaseId()));
    if (itSearch == m_splitTimes.end()) {
        m_splitTimes.push_back(p_split);
    }
}

std::vector<SplitTime> ICompetitor::GetSplitTimes() const { return m_splitTimes; }

SplitTime ICompetitor::GetSplitTimeFromBaseStation(const int& p_baseId) const
{
    SplitTime retSplit;

    std::vector<SplitTime>::const_iterator itSearch = std::find_if(
        m_splitTimes.begin(), m_splitTimes.end(), MatchBaseStationId(p_baseId));

    if (itSearch != m_splitTimes.end()) {
        retSplit = *itSearch;
    }
    return retSplit;
}

void ICompetitor::UpdateSplitTime(const SplitTime& p_split)
{
    std::vector<SplitTime>::iterator itSearch = std::find_if(
        m_splitTimes.begin(), m_splitTimes.end(), MatchBaseStationId(p_split.GetBaseId()));

    if (itSearch != m_splitTimes.end()) {
        itSearch->SetTime(p_split.GetTime());
    } else {
        m_splitTimes.push_back(p_split);
    }
}

QTime ICompetitor::GetRaceTime() const
{
    QTime delta;
    if (m_splitTimes.size() > 1) {
        QTime first = m_splitTimes.begin()->GetTime();
        QTime last = m_splitTimes.back().GetTime();

        delta = QTime::fromMSecsSinceStartOfDay(first.msecsTo(last));
    }

    return delta;
}

QTime ICompetitor::GetRaceTimeAtBaseStation(const int& p_baseId) const
{
    QTime delta;
    std::vector<SplitTime>::const_iterator itSearch = std::find_if(
        m_splitTimes.begin(), m_splitTimes.end(), MatchBaseStationId(p_baseId));

    if (itSearch != m_splitTimes.end()) {
        SplitTime split = *itSearch;
        QTime first = m_splitTimes.begin()->GetTime();
        QTime last = split.GetTime();

        delta = QTime::fromMSecsSinceStartOfDay(first.msecsTo(last));
    }
    return delta;
}
void ICompetitor::ClearSplitTimes() { m_splitTimes.clear(); }

QString ICompetitor::SexeToString(const eSexe& p_sexeType)
{
    QString ret;
    switch (p_sexeType) {
    case MALE: {
        ret = QObject::tr("Male");
        break;
    }
    case FEMALE: {
        ret = QObject::tr("Female");
        break;
    }
    case ALL: {
        ret = QObject::tr("Both");
        break;
    }
    }
    return ret;
}

bool ICompetitor::operator==(const ICompetitor& p_lhs) { return p_lhs.m_idDatabase == m_idDatabase; }
bool ICompetitor::operator<(const ICompetitor& p_lhs) const { return p_lhs.m_idDatabase > m_idDatabase; }
// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const ICompetitor& p_measurable)
{
    p_out << p_measurable.m_version;
    p_out << p_measurable.m_idDatabase;
    p_out << p_measurable.m_bibNumber;
    p_out << p_measurable.m_chipNumber;
    p_out << p_measurable.m_sexe;
    p_out << p_measurable.m_category;
    p_out << p_measurable.m_state;
    p_out << QVector<SplitTime>::fromStdVector(p_measurable.m_splitTimes);
    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, ICompetitor& p_measurable)
{
    p_in >> p_measurable.m_version;
    p_in >> p_measurable.m_idDatabase;
    p_in >> p_measurable.m_bibNumber;
    p_in >> p_measurable.m_chipNumber;
    p_in >> (int&)p_measurable.m_sexe;
    p_in >> (int&)p_measurable.m_category;
    p_in >> p_measurable.m_state;
    QVector<SplitTime> vectToCopy;
    p_in >> vectToCopy;
    p_measurable.m_splitTimes = vectToCopy.toStdVector();

    return p_in;
}

// ostream ICompetitorPtr << overloading
QDataStream& operator<<(QDataStream& p_out, const ICompetitorPtr p_measurable) { return p_out << *p_measurable; }

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, ICompetitorPtr p_measurable) { return p_in >> *p_measurable; }
