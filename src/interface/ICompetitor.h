/*
 * ICompetitor.h
 *
 *  Created on: 26 avr. 2016
 *      Author: baptiste
 */

#ifndef SRC_INTERFACE_ICOMPETITOR_H_
#define SRC_INTERFACE_ICOMPETITOR_H_

#include "Category.h"
#include "CompetitorState.h"
#include "SplitTime.h"
#include <memory>
#include <mutex>

class ICompetitor {
public:
    enum eSexe { MALE, FEMALE, ALL };

    ICompetitor();
    ICompetitor(const int& p_idDatabase);
    ICompetitor(
        const int& p_idDatabase,
        const int& p_bibNumber,
        const QString& p_chipNumber,
        const eSexe& p_sexe,
        const Category::eCategory& p_category);
    virtual ~ICompetitor();

    virtual QString toLogString() = 0;

    bool IsValid() const;

    int GetDatabaseId() const;

    void AddSplitTime(const SplitTime& p_split);

    std::vector<SplitTime> GetSplitTimes() const;

    SplitTime GetSplitTimeFromBaseStation(const int& p_baseId) const;

    void UpdateSplitTime(const SplitTime& p_split);

    QTime GetRaceTime() const;

    QTime GetRaceTimeAtBaseStation(const int& p_baseId) const;

    void ClearSplitTimes();

    static QString SexeToString(const eSexe& p_sexeType);

    int GetBibNumber() const { return m_bibNumber; }

    void SetBibNumber(int bibNumber) { m_bibNumber = bibNumber; }

    Category::eCategory GetCategory() const { return m_category; }

    void SetCategory(Category::eCategory category) { m_category = category; }

    const QString& GetChipNumber() const { return m_chipNumber; }

    void SetChipNumber(const QString& chipNumber) { m_chipNumber = chipNumber; }

    eSexe GetSexe() const { return m_sexe; }

    void SetSexe(eSexe sexe) { m_sexe = sexe; }

    CompetitorState::eState GetState() const { return m_state.GetState(); }

    void SetState(CompetitorState::eState state) { m_state.SetState(state); }

    CompetitorState& GetCompetitorState() { return m_state; }

    virtual void Merge(std::shared_ptr<ICompetitor> p_competitor) = 0;

    // ostream ICompetitor << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const ICompetitor& p_runner);
    // istream ICompetitor >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, ICompetitor& p_runner);

    // ostream ICompetitorPtr << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const std::shared_ptr<ICompetitor> p_runner);
    // istream ICompetitorPtr >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<ICompetitor> p_runner);

    bool operator==(const ICompetitor& p_lhs);
    bool operator<(const ICompetitor& p_lhs) const;

protected:
    int m_version;
    int m_idDatabase;
    int m_bibNumber;
    QString m_chipNumber;
    eSexe m_sexe;
    Category::eCategory m_category;
    CompetitorState m_state;
    std::vector<SplitTime> m_splitTimes;
};

typedef std::shared_ptr<ICompetitor> ICompetitorPtr;

#endif /* SRC_INTERFACE_ICOMPETITOR_H_ */
