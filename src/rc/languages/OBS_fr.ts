<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DialogEditRaceConf</name>
    <message>
        <location filename="../../view/DialogEditRaceConf.cpp" line="96"/>
        <source>Start Station</source>
        <translation>Départ</translation>
    </message>
    <message>
        <location filename="../../view/DialogEditRaceConf.cpp" line="105"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../../view/DialogEditRaceConf.cpp" line="117"/>
        <source>Finish Station</source>
        <translation>Arrivée</translation>
    </message>
</context>
<context>
    <name>DialogEditRunner</name>
    <message>
        <location filename="../../view/DialogEditRunner.cpp" line="49"/>
        <source>Ctrl+return</source>
        <translation>Ctrl+return</translation>
    </message>
</context>
<context>
    <name>DialogEditStartedTime</name>
    <message>
        <location filename="../../view/DialogEditStartedTime.cpp" line="19"/>
        <source>Change started time</source>
        <translation>Modification départ</translation>
    </message>
    <message>
        <location filename="../../view/DialogEditStartedTime.cpp" line="20"/>
        <source>Warning you are going to change the started time !</source>
        <translation>Attention l&apos;heure de départ va etre modifée !</translation>
    </message>
</context>
<context>
    <name>DialogResults</name>
    <message>
        <location filename="../../view/DialogResults.cpp" line="26"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../../view/DialogResults.cpp" line="63"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
</context>
<context>
    <name>DialogTimeTrial</name>
    <message>
        <location filename="../../view/DialogTimeTrial.cpp" line="25"/>
        <source>Time Trial</source>
        <translation>Contre la montre</translation>
    </message>
</context>
<context>
    <name>EditRunner</name>
    <message>
        <location filename="../../view/EditRunner.ui" line="14"/>
        <source>Edit Runner</source>
        <translation>Fiche Coureur</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="24"/>
        <source>Infos</source>
        <translation>Coureur</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="127"/>
        <source>Club</source>
        <translation>Club</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="30"/>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="44"/>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="137"/>
        <source>Chip number</source>
        <translation>Puce</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="57"/>
        <source>BirthDate</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="64"/>
        <source>Sexe</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="104"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="77"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="94"/>
        <source>Bib number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <source>ZIP</source>
        <translation type="vanished">Code postal</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="84"/>
        <source>Tel</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="117"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="147"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="155"/>
        <source>Race</source>
        <translation>Course</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="163"/>
        <source>State</source>
        <translation>Etat</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="194"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../view/EditRunner.ui" line="201"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>EditStartedTime</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <location filename="../../view/EditStartedTime.ui" line="14"/>
        <source>Edit started Time</source>
        <translation>Heure de départ</translation>
    </message>
    <message>
        <location filename="../../view/EditStartedTime.ui" line="37"/>
        <source>HH:mm:ss</source>
        <translation>HH:mm:ss</translation>
    </message>
</context>
<context>
    <name>EditTeam</name>
    <message>
        <location filename="../../view/EditTeam.ui" line="14"/>
        <source>Edit Team</source>
        <translation>Fiche équipe</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="24"/>
        <source>Infos</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="30"/>
        <source>Chip number</source>
        <translation>Puce</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="37"/>
        <source>Team name</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="50"/>
        <source>Race number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="65"/>
        <source>Up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="72"/>
        <source>Down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="95"/>
        <source>Race</source>
        <translation>Course</translation>
    </message>
    <message>
        <location filename="../../view/EditTeam.ui" line="103"/>
        <source>State</source>
        <translation>Etat</translation>
    </message>
</context>
<context>
    <name>OBS</name>
    <message>
        <location filename="../../view/OBS.ui" line="38"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="49"/>
        <source>Edit</source>
        <translation>Edition</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="61"/>
        <source>Open Race</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="66"/>
        <source>Set Interval</source>
        <translation>Modification Interval</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="71"/>
        <source>Start</source>
        <translation>Départ</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="76"/>
        <source>Load Website</source>
        <translation>Chargement Site Web</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="81"/>
        <source>Save Race</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="86"/>
        <source>Race configuration</source>
        <translation>Paramétres Course</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="91"/>
        <source>RFID Configuration</source>
        <translation>Configuration RFID</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="96"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="101"/>
        <source>Export Results</source>
        <translation>Exporter Résultats</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="106"/>
        <source>New Race</source>
        <translation>Créer Course</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="111"/>
        <source>Export Team Results</source>
        <translation>Exporter Résultats équipes</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="116"/>
        <source>Clear Live Timing</source>
        <translation>RAZ suivi live</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="121"/>
        <source>Merge Race</source>
        <translation>Fusionner</translation>
    </message>
    <message>
        <location filename="../../view/OBS.ui" line="126"/>
        <source>Import Race</source>
        <translation>Importer</translation>
    </message>
</context>
<context>
    <name>OBSMainView</name>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="71"/>
        <source>Server setup</source>
        <translation>Configuration serveur</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="72"/>
        <source>Server connexion is not properly setup.</source>
        <translation>Mauvaise configuration serveur</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="85"/>
        <location filename="../../view/OBSMainView.cpp" line="184"/>
        <location filename="../../view/OBSMainView.cpp" line="212"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier course</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="85"/>
        <location filename="../../view/OBSMainView.cpp" line="122"/>
        <location filename="../../view/OBSMainView.cpp" line="212"/>
        <source>Files (*.obs)</source>
        <translation>Fichier (*.obs)</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="115"/>
        <source>Import File </source>
        <translation>Importer Fichier Course</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="122"/>
        <source>Save file</source>
        <translation>Sauvegarder course</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="136"/>
        <source>Clear Live Timing </source>
        <translation>RAZ suivi live</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="137"/>
        <source>You&apos;re going to clear all live timing informations.</source>
        <translation>Vous etes sur le point de supprimer tous les temps du suivi live.</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="184"/>
        <source>Files (*.json)</source>
        <translation>Fichier (*.json)</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="220"/>
        <source>Race Merge </source>
        <translation>Fusion des Courses</translation>
    </message>
    <message>
        <location filename="../../view/OBSMainView.cpp" line="220"/>
        <source>Impossible to merge.</source>
        <translation>Impossible de fusionner les courses.</translation>
    </message>
</context>
<context>
    <name>OBS_MainView</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">Ouvrir un fichier course</translation>
    </message>
    <message>
        <source>Files (*.obs)</source>
        <translation type="vanished">Fichier (*.obs)</translation>
    </message>
    <message>
        <source>Import File </source>
        <translation type="vanished">Importer Fichier Course</translation>
    </message>
    <message>
        <source>Save file</source>
        <translation type="vanished">Sauvegarder</translation>
    </message>
    <message>
        <source>Clear Live Timing </source>
        <translation type="vanished">RAZ suivi live</translation>
    </message>
    <message>
        <source>You&apos;re going to clear all live timing informations.</source>
        <translation type="vanished">Vous etes sur le point de supprimer tous les temps du suivi live.</translation>
    </message>
    <message>
        <source>Files (*.json)</source>
        <translation type="vanished">Fichier (*.json)</translation>
    </message>
    <message>
        <source>Race Merge </source>
        <translation type="vanished">Fusion des Courses</translation>
    </message>
    <message>
        <source>Impossible to merge.</source>
        <translation type="vanished">Impossible de fusionner les courses.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../data/JsonRaceImporter.cpp" line="72"/>
        <source>Start Station</source>
        <translation>Départ</translation>
    </message>
    <message>
        <location filename="../../data/JsonRaceImporter.cpp" line="79"/>
        <source>Finish Station</source>
        <translation>Arrivée</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="30"/>
        <source>Missing</source>
        <translation>Absent</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="34"/>
        <source>Checked</source>
        <translation>Présent</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="38"/>
        <source>Running</source>
        <translation>En course</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="42"/>
        <source>Disqualified</source>
        <translation>Disqualifié</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="46"/>
        <source>Aborted</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <location filename="../../data/CompetitorState.cpp" line="50"/>
        <source>Finished</source>
        <translation>Finisher</translation>
    </message>
    <message>
        <location filename="../../interface/ICompetitor.cpp" line="143"/>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <location filename="../../interface/ICompetitor.cpp" line="147"/>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <location filename="../../interface/ICompetitor.cpp" line="151"/>
        <source>Both</source>
        <translation>Mixte</translation>
    </message>
</context>
<context>
    <name>RFIDReaderConf</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="14"/>
        <source>RFID Configuration</source>
        <translation>Configuration RFID</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="25"/>
        <source>RFID Reader</source>
        <translation>Lecteur RFID</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="32"/>
        <source>Connect</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="52"/>
        <source>Emit sound</source>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="59"/>
        <source>Clear</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="../../view/RFIDConf.ui" line="83"/>
        <source>RFID Reader connected</source>
        <translation>Lecteur RFID connecté</translation>
    </message>
</context>
<context>
    <name>RaceConf</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="14"/>
        <source>Race Configuration</source>
        <translation>Paramètres Course</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="20"/>
        <source>Race Type</source>
        <translation>Type de course</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="29"/>
        <source>Solo</source>
        <translation>Solo</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="36"/>
        <source>Team (3)</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="43"/>
        <source>Chrono</source>
        <translation>CLM</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="58"/>
        <source>Race name</source>
        <translation>Course</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="68"/>
        <source>Race ID</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="75"/>
        <source>Race date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="88"/>
        <source>Interval</source>
        <translation>Intervalle CLM</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="95"/>
        <source>Nb Runner</source>
        <translation>Coureur par equipe</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="107"/>
        <source>Safety check</source>
        <translation>Vérification avant départ</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="121"/>
        <source>Base Stations configuration</source>
        <translation>Point de controle</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="139"/>
        <source>Start</source>
        <translation>Départ</translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="146"/>
        <source>Finish</source>
        <translation>Arrivée </translation>
    </message>
    <message>
        <location filename="../../view/RaceConf.ui" line="158"/>
        <source>Add Station</source>
        <translation>Ajouter Point de controle</translation>
    </message>
</context>
<context>
    <name>RaceController</name>
    <message>
        <location filename="../../controller/RaceController.cpp" line="39"/>
        <source>Waiting for Tag</source>
        <translation>Attente puce</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="39"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="53"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="54"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="112"/>
        <source>Save Error </source>
        <translation>Erreur de sauvegarde</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="113"/>
        <source>Failed to save race to: </source>
        <translation>Impossible de sauvegarder la course vers :</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="194"/>
        <location filename="../../controller/RaceController.cpp" line="198"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="194"/>
        <source>Nothing to delete</source>
        <translation>Sélection vide</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="199"/>
        <source>You are about to delete %1 items. Continue ?</source>
        <translation>Vous etes sur le point de supprimer %1 coureurs, continuer ?</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="225"/>
        <source>Clear Live Timing </source>
        <translation>RAZ suivi live</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="226"/>
        <source>You&apos;re going to clear all live timing informations.</source>
        <translation>Vous etes sur le point de supprimer tous les temps du suivi live.</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="240"/>
        <source>Clear live timing</source>
        <translation>RAZ suivi live</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="241"/>
        <source>Synchronize competitors</source>
        <translation>Synchro Coureurs</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="260"/>
        <source>Race Configuration</source>
        <translation>Paramètres Course</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="261"/>
        <source>Autoset</source>
        <translation>Dossards automatique</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="262"/>
        <location filename="../../controller/RaceController.cpp" line="310"/>
        <source>Auto-generation</source>
        <translation>Génération de coureurs</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="263"/>
        <source>Fast Chip registration</source>
        <translation>Mode enregistrement puce</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="264"/>
        <source>Safety Check</source>
        <translation>Vérification avant départ</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="265"/>
        <source>Set started time</source>
        <translation>Modification départ</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="266"/>
        <source>Find owner tag</source>
        <translation>Recherche par puce</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="239"/>
        <source>Force send time</source>
        <translation>Force l&apos;envoie de données live</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="267"/>
        <source>Export CSV</source>
        <translation>Exporter CSV</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="268"/>
        <source>Export results</source>
        <translation>Exporter résultats</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="269"/>
        <source>Open results view</source>
        <translation>Vue résultats</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="270"/>
        <source>Save</source>
        <translation>Sauvegarder course</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="307"/>
        <source>Autoset Bib number</source>
        <translation>Dossards automatique</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="307"/>
        <source>Starting bib number:</source>
        <translation>Premier dossard:</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="310"/>
        <source>Number to generate:</source>
        <translation>Nombre à générer automatiquement:</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="334"/>
        <location filename="../../controller/RaceController.cpp" line="339"/>
        <source>Save Results</source>
        <translation>Sauvegarder résultats</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="334"/>
        <location filename="../../controller/RaceController.cpp" line="339"/>
        <source>Files (*.csv)</source>
        <translation>Fichier (*.csv)</translation>
    </message>
    <message>
        <source>Files (*.pdf)</source>
        <translation type="vanished">Fichier (*.pdf)</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="349"/>
        <source>Save file</source>
        <translation>Sauvegarder Course</translation>
    </message>
    <message>
        <location filename="../../controller/RaceController.cpp" line="349"/>
        <source>Files (*.obs)</source>
        <translation>Fichier (*.obs)</translation>
    </message>
</context>
<context>
    <name>RaceModel</name>
    <message>
        <location filename="../../interface/RaceModel.cpp" line="424"/>
        <source>Race Stop</source>
        <translation>Course arrétée</translation>
    </message>
    <message>
        <location filename="../../interface/RaceModel.cpp" line="424"/>
        <source>Race is stopped</source>
        <translation>La course est arrétée</translation>
    </message>
    <message>
        <location filename="../../interface/RaceModel.cpp" line="467"/>
        <source>Rank</source>
        <translation>Classement</translation>
    </message>
</context>
<context>
    <name>RaceView</name>
    <message>
        <location filename="../../controller/RaceView.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
</context>
<context>
    <name>Results</name>
    <message>
        <location filename="../../view/Results.ui" line="14"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
</context>
<context>
    <name>RunnerInfos</name>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="14"/>
        <source>Runner Infos</source>
        <translation>Fiche coureur</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="20"/>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="30"/>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="40"/>
        <source>BirthDate</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="50"/>
        <source>Sexe</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="60"/>
        <source>Club</source>
        <translation>Club</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="70"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="80"/>
        <source>Tel</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="90"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="100"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../../view/RunnerInfos.ui" line="110"/>
        <source>ZIP</source>
        <translation>Code postal</translation>
    </message>
</context>
<context>
    <name>SafetyCheck</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="14"/>
        <source>Safety Check</source>
        <translation>Vérification avant départ</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="37"/>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="44"/>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="51"/>
        <source>Bib Number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="90"/>
        <source>Team Name</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="97"/>
        <source>Bib number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="127"/>
        <source>Rank</source>
        <translation>Classement</translation>
    </message>
    <message>
        <location filename="../../view/SafetyCheck.ui" line="134"/>
        <source>Race Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>SoloRaceController</name>
    <message>
        <location filename="../../controller/SoloRaceController.cpp" line="67"/>
        <source>Not Found</source>
        <translation>Non trouvée</translation>
    </message>
    <message>
        <location filename="../../controller/SoloRaceController.cpp" line="67"/>
        <source>RFID Tag not associated : </source>
        <translation>Puce RFID non associée :</translation>
    </message>
</context>
<context>
    <name>SoloRaceModel</name>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="30"/>
        <source>Bib Number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="34"/>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="38"/>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="42"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="46"/>
        <source>Sexe</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="50"/>
        <source>Race time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="54"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../../model/SoloRaceModel.cpp" line="118"/>
        <source>Rank</source>
        <translation>Classement</translation>
    </message>
</context>
<context>
    <name>TeamRaceController</name>
    <message>
        <location filename="../../controller/TeamRaceController.cpp" line="68"/>
        <source>Not Found</source>
        <translation>Non trouvée</translation>
    </message>
    <message>
        <location filename="../../controller/TeamRaceController.cpp" line="68"/>
        <source>RFID Tag not associated : </source>
        <translation>Puce RFID non associée :</translation>
    </message>
</context>
<context>
    <name>TeamRaceModel</name>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="30"/>
        <source>Bib Number</source>
        <translation>Dossard</translation>
    </message>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="34"/>
        <source>Team Name</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="38"/>
        <source>Sexe</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="42"/>
        <source>Race time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="46"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../../model/TeamRaceModel.cpp" line="93"/>
        <source>Rank</source>
        <translation>Classement</translation>
    </message>
</context>
<context>
    <name>TimeTrial</name>
    <message>
        <location filename="../../view/TimeTrial.ui" line="14"/>
        <source>Time Trial</source>
        <translation>Contre la montre</translation>
    </message>
    <message>
        <location filename="../../view/TimeTrial.ui" line="112"/>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>TimeTrialRaceController</name>
    <message>
        <location filename="../../controller/TimeTrialRaceController.cpp" line="92"/>
        <source>Not Found</source>
        <translation>Non trouvée</translation>
    </message>
    <message>
        <location filename="../../controller/TimeTrialRaceController.cpp" line="92"/>
        <source>RFID Tag not associated : </source>
        <translation>Puce RFID non associée :</translation>
    </message>
</context>
<context>
    <name>editRunner</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <source>Infos</source>
        <translation type="vanished">Informations coureur</translation>
    </message>
    <message>
        <source>Club</source>
        <translation type="vanished">Club</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>First name</source>
        <translation type="vanished">Prénom</translation>
    </message>
    <message>
        <source>Chip number</source>
        <translation type="vanished">Puce</translation>
    </message>
    <message>
        <source>BirthDate</source>
        <translation type="vanished">Date de naissance</translation>
    </message>
    <message>
        <source>Sexe</source>
        <translation type="vanished">Sexe</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Catégorie</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="vanished">Email</translation>
    </message>
    <message>
        <source>Bib number</source>
        <translation type="vanished">Dossard</translation>
    </message>
    <message>
        <source>ZIP</source>
        <translation type="vanished">Code postal</translation>
    </message>
    <message>
        <source>Tel</source>
        <translation type="vanished">Téléphone</translation>
    </message>
    <message>
        <source>City</source>
        <translation type="vanished">Ville</translation>
    </message>
    <message>
        <source>Race</source>
        <translation type="vanished">Course</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="vanished">Etat</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="vanished">Précédent</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">Suivant</translation>
    </message>
</context>
<context>
    <name>editTeam</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Fenetre</translation>
    </message>
    <message>
        <source>Infos</source>
        <translation type="vanished">Informations équipe</translation>
    </message>
    <message>
        <source>Chip number</source>
        <translation type="vanished">Puce</translation>
    </message>
    <message>
        <source>Team name</source>
        <translation type="vanished">Equipe</translation>
    </message>
    <message>
        <source>Race number</source>
        <translation type="vanished">Dossard</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="vanished">Monter</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="vanished">Descendre</translation>
    </message>
    <message>
        <source>Race</source>
        <translation type="vanished">Course</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="vanished">Etat</translation>
    </message>
</context>
<context>
    <name>runnerInfos</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>First name</source>
        <translation type="vanished">Prénom</translation>
    </message>
    <message>
        <source>BirthDate</source>
        <translation type="vanished">Date de naissance</translation>
    </message>
    <message>
        <source>Sexe</source>
        <translation type="vanished">Sexe</translation>
    </message>
    <message>
        <source>Club</source>
        <translation type="vanished">Club</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Catégorie</translation>
    </message>
    <message>
        <source>Tel</source>
        <translation type="vanished">Téléphone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="vanished">Email</translation>
    </message>
    <message>
        <source>City</source>
        <translation type="vanished">Ville</translation>
    </message>
    <message>
        <source>ZIP</source>
        <translation type="vanished">Code postal</translation>
    </message>
</context>
</TS>
