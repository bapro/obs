// (C) king.com Ltd 2020

#include "BLEDiscovery.h"

BLEDiscovery::BLEDiscovery()
{
    mDiscoveryAgent = std::make_unique<QBluetoothDeviceDiscoveryAgent>();
    mDiscoveryAgent->setLowEnergyDiscoveryTimeout(5000);
    connect(mDiscoveryAgent.get(), &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &BLEDiscovery::addDevice);
    connect(
        mDiscoveryAgent.get(),
        QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error),
        this,
        &BLEDiscovery::deviceScanError);
    connect(mDiscoveryAgent.get(), &QBluetoothDeviceDiscoveryAgent::finished, this, &BLEDiscovery::deviceScanFinished);
}
std::vector<QBluetoothDeviceInfo> BLEDiscovery::getDevices() { return mDevices; }

void BLEDiscovery::startDeviceDiscovery()
{
    mDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);

    if (mDiscoveryAgent->isActive()) {
        setUpdate("Scanning for devices ...");
    }
}

void BLEDiscovery::addDevice(const QBluetoothDeviceInfo& info)
{
    if (info.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration && !info.name().isEmpty()) {

        qWarning() << "device added: " << info.name() << info.deviceUuid().toString();
        auto itFind = std::find_if(mDevices.begin(), mDevices.end(), [info](const QBluetoothDeviceInfo& devInfo) {
            return devInfo.deviceUuid() == info.deviceUuid();
        });

        if (itFind != mDevices.end()) {
            if (itFind->name().isEmpty() && !info.name().isEmpty()) {
                *itFind = info;
            }
            return;
        }
        mDevices.push_back(info);
        setUpdate("Last device added: " + info.name());
    }
}
void BLEDiscovery::deviceScanFinished()
{
    emit devicesDiscoveryFinished();
    if (mDevices.empty())
        setUpdate("No Low Energy devices found...");
    else
        setUpdate("Done! Scan Again!");
}
void BLEDiscovery::deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    if (error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
        setUpdate("The Bluetooth adaptor is powered off, power it on before doing discovery.");
    else if (error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
        setUpdate("Writing or reading from the device resulted in an error.");
    else {
        setUpdate("Error: " + QString::number(error));
    }

    emit devicesDiscoveryFinished();
}

void BLEDiscovery::setUpdate(const QString& message) { emit updateChanged(message); }
