#pragma once

#include "CharacteristicInfo.h"
#include "ServiceInfo.h"
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothServiceInfo>
#include <QList>
#include <QLowEnergyController>
#include <QObject>
#include <QVariant>
#include <qbluetoothlocaldevice.h>

QT_FORWARD_DECLARE_CLASS (QBluetoothServiceInfo)

class BLEDevice : public QObject
{
    Q_OBJECT
public:
    BLEDevice() = delete;
    BLEDevice(const QBluetoothDeviceInfo &device);
    ~BLEDevice();
    std::vector<std::shared_ptr<ServiceInfo>> getServices();
    QList<CharacteristicInfo *> getCharacteristics();
    bool hasControllerError() const;

    void link();
    void connectToService(std::shared_ptr<ServiceInfo> service);

    void unlink();

    void setAutoRelink(bool enabled);

    enum class State{
        CONNECTED,
        DISCONNECTED
    };

    bool operator==(const BLEDevice& rhs) const;
    bool operator==(const QBluetoothDeviceInfo& rhs) const;

    State state();
private slots:
    // QLowEnergyController realted
    void addLowEnergyService(const QBluetoothUuid &uuid);
    void deviceConnected();
    void errorReceived(QLowEnergyController::Error);
    void serviceScanDone();
    void deviceDisconnected();

    // QLowEnergyService related
    void serviceDetailsDiscovered(QLowEnergyService::ServiceState newState);

Q_SIGNALS:

    void servicesDiscoveryFinished();
    void characteristicsDiscoveryFinished();

    void updateChanged(QString);
    void disconnected();

private:
    void setUpdate(const QString &message);
    void onTryReLink();

    QBluetoothDeviceInfo mCurrentDevice;
    std::vector<std::shared_ptr<ServiceInfo>> mServices;
    QList<CharacteristicInfo *> mCharacteristics;
    std::unique_ptr<QLowEnergyController> mController;
    State mState;
    bool mAutoReLink = false;
};
