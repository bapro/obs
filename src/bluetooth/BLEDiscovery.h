// (C) king.com Ltd 2020

#pragma once

#include "CharacteristicInfo.h"
#include "ServiceInfo.h"
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothServiceInfo>
#include <QList>
#include <QLowEnergyController>
#include <QObject>

QT_FORWARD_DECLARE_CLASS (QBluetoothDeviceInfo)

class BLEDiscovery : public QObject {
    Q_OBJECT
public:
    BLEDiscovery();
    ~BLEDiscovery() = default;

    std::vector<QBluetoothDeviceInfo> getDevices();

    void startDeviceDiscovery();

private slots:
    // QBluetoothDeviceDiscoveryAgent related
    void addDevice(const QBluetoothDeviceInfo&);
    void deviceScanFinished();
    void deviceScanError(QBluetoothDeviceDiscoveryAgent::Error);

Q_SIGNALS:

    void devicesDiscoveryFinished();
    void updateChanged(QString);

private:
    void setUpdate(const QString& message);
    std::unique_ptr<QBluetoothDeviceDiscoveryAgent> mDiscoveryAgent;
    std::vector<QBluetoothDeviceInfo> mDevices;
};
