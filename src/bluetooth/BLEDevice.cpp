#include "BLEDevice.h"

#include <QDebug>
#include <QList>
#include <QMetaEnum>
#include <QTimer>
#include <qbluetoothdeviceinfo.h>
#include <qbluetoothservicediscoveryagent.h>

namespace {
constexpr int msTimer = 5000;
}
BLEDevice::BLEDevice(const QBluetoothDeviceInfo& device)
    : mCurrentDevice(device)
{
}

BLEDevice::~BLEDevice()
{
    // TODO Disconnect ?
    mServices.clear();
    mCharacteristics.clear();
}

std::vector<std::shared_ptr<ServiceInfo>> BLEDevice::getServices() { return mServices; }

QList<CharacteristicInfo*> BLEDevice::getCharacteristics() { return mCharacteristics; }

void BLEDevice::link()
{
    if (!mCurrentDevice.isValid()) {
        qWarning() << "Not a valid device";
        setUpdate("Invalid device!");
        return;
    }

    qDeleteAll(mCharacteristics);
    mCharacteristics.clear();
    mServices.clear();

    setUpdate("(Connecting to device: " + mCurrentDevice.name());
    // We need the current device for service discovery.
    if (mController) {
        mController->disconnectFromDevice();
        mController.reset();
    }

    if (!mController) {
        // Connecting signals and slots for connecting to LE services.
        mController = std::unique_ptr<QLowEnergyController>(QLowEnergyController::createCentral(mCurrentDevice));
        connect(mController.get(), &QLowEnergyController::connected, this, &BLEDevice::deviceConnected);
        connect(
            mController.get(),
            QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error),
            this,
            &BLEDevice::errorReceived);
        connect(mController.get(), &QLowEnergyController::disconnected, this, &BLEDevice::deviceDisconnected);
        connect(mController.get(), &QLowEnergyController::serviceDiscovered, this, &BLEDevice::addLowEnergyService);
        connect(mController.get(), &QLowEnergyController::discoveryFinished, this, &BLEDevice::serviceScanDone);
        // connect(
        //  mController.get(), &QLowEnergyController::discoveryFinished, this, &BLEDevice::servicesDiscoveryFinished);
    }

    mController->setRemoteAddressType(QLowEnergyController::PublicAddress);
    mController->connectToDevice();
}

void BLEDevice::addLowEnergyService(const QBluetoothUuid& serviceUuid)
{
    QLowEnergyService* service = mController->createServiceObject(serviceUuid);
    if (!service) {
        qWarning() << "Cannot create service for uuid";
        return;
    }
    mServices.push_back(std::make_shared<ServiceInfo>(service));
}
//! [les-service-1]

void BLEDevice::serviceScanDone()
{
    setUpdate("Service scan done!");
    // force UI in case we didn't find anything
    emit servicesDiscoveryFinished();
}

void BLEDevice::connectToService(std::shared_ptr<ServiceInfo> srv)
{
    QLowEnergyService* service = srv->service();

    if (!service)
        return;

    qDeleteAll(mCharacteristics);
    mCharacteristics.clear();

    if (service->state() == QLowEnergyService::DiscoveryRequired) {
        //! [les-service-3]
        connect(service, &QLowEnergyService::stateChanged, this, &BLEDevice::serviceDetailsDiscovered);
        service->discoverDetails();
        setUpdate("(Discovering details...)");
        //! [les-service-3]
        return;
    }

    // discovery already done
    const QList<QLowEnergyCharacteristic> chars = service->characteristics();
    for (const QLowEnergyCharacteristic& ch : chars) {
        auto cInfo = new CharacteristicInfo(ch);
        mCharacteristics.append(cInfo);
    }

    QTimer::singleShot(0, this, &BLEDevice::characteristicsDiscoveryFinished);
}

void BLEDevice::deviceConnected()
{
    setUpdate("(Discovering services...)");
    mState = State::CONNECTED;
    //! [les-service-2]
    mController->discoverServices();
    //! [les-service-2]
}

void BLEDevice::errorReceived(QLowEnergyController::Error /*error*/)
{
    qWarning() << "Error: " << mController->errorString();
    setUpdate(QString("Error\n(%1)").arg(mController->errorString()));
}

void BLEDevice::setUpdate(const QString& message) { emit updateChanged(message); }

void BLEDevice::unlink()
{
    // UI always expects disconnect() signal when calling this signal
    // TODO what is really needed is to extend state() to a multi value
    // and thus allowing UI to keep track of mController progress in addition to
    // device scan progress

    if (mController->state() != QLowEnergyController::UnconnectedState) {
        mController->disconnectFromDevice();
    } else {
        deviceDisconnected();
    }
}

void BLEDevice::deviceDisconnected()
{
    qWarning() << "Disconnect from device";
    mState = State::DISCONNECTED;
    emit disconnected();

    if (mAutoReLink) {
        QTimer::singleShot(msTimer, this, &BLEDevice::onTryReLink);
    }
}

void BLEDevice::serviceDetailsDiscovered(QLowEnergyService::ServiceState newState)
{
    if (newState != QLowEnergyService::ServiceDiscovered) {
        // do not hang in "Scanning for characteristics" mode forever
        // in case the service discovery failed
        // We have to queue the signal up to give UI time to even enter
        // the above mode
        if (newState != QLowEnergyService::DiscoveringServices) {
            QMetaObject::invokeMethod(this, "characteristicsDiscoveryFinished", Qt::QueuedConnection);
        }
        return;
    }

    auto service = qobject_cast<QLowEnergyService*>(sender());
    if (!service)
        return;

    //! [les-chars]
    const QList<QLowEnergyCharacteristic> chars = service->characteristics();
    for (const QLowEnergyCharacteristic& ch : chars) {
        auto cInfo = new CharacteristicInfo(ch);
        mCharacteristics.append(cInfo);
    }
    //! [les-chars]

    emit characteristicsDiscoveryFinished();
    setUpdate("Service discovered");
}

bool BLEDevice::hasControllerError() const
{
    return (mController && mController->error() != QLowEnergyController::NoError);
}
BLEDevice::State BLEDevice::state() { return mState; }

bool BLEDevice::operator==(const BLEDevice& rhs) const
{
    return mCurrentDevice.deviceUuid() == rhs.mCurrentDevice.deviceUuid();
}
bool BLEDevice::operator==(const QBluetoothDeviceInfo& rhs) const
{
    return mCurrentDevice.deviceUuid() == rhs.deviceUuid();
}
void BLEDevice::onTryReLink() { link(); }
void BLEDevice::setAutoRelink(bool enabled) { mAutoReLink = enabled; }
