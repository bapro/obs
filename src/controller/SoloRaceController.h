/*
 * SoloRaceController.h
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#pragma once
#include "Runner.h"
#include <RaceController.h>

class SoloRaceController : public RaceController {
    Q_OBJECT

public:
    SoloRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent = 0);
    virtual ~SoloRaceController();

protected:
    /**
     * Inherited from RacController
     */
    virtual void SelectItem(const QModelIndex& p_index);
    virtual void AddNewItem();
    virtual void FindItemByTag(const QString& p_tag);

    void OpenEditRunner(RunnerPtr p_runner);

    virtual QDataStream& Serialize(QDataStream& p_out);

    virtual QDataStream& Deserialize(QDataStream& p_in);
};
