/*
 * RaceController.h
 *
 *  Created on: 8 mai 2016
 *      Author: baptiste
 */

#pragma once
#include "DialogSafetyCheck.h"
#include "IDetector.h"
#include "RaceModel.h"
#include "qwidget.h"
#include "ui_RaceView.h"
#include <QProgressDialog>
#include <qsortfilterproxymodel.h>

class QSortFilterProxyModel;
class ServerBinder;

class RaceController : public QWidget {
    Q_OBJECT
public:
    RaceController(RaceModelPtr model, IDetector& detector, ServerBinder& serverBinder, QWidget* arent = 0);
    virtual ~RaceController();

    void SaveRace(const QString& p_raceFile);

    void SetRaceConfiguration(const RaceConfiguration& p_raceConfiguration);

    RaceConfiguration& GetRaceConfiguration();

    void DisconnectRFIDDriver();

    void ConnectRFIDDRiver();

    void SetFilePath(const QString& p_filePath);

    void AddButtonToPluginLayout(QToolButton* p_pluginButton);

    virtual QDataStream& Serialize(QDataStream& p_out) = 0;

    virtual QDataStream& Deserialize(QDataStream& p_in);

    virtual bool Merge(RaceController* p_controller);

    void UpdateTimes(const int& p_raceId, const std::list<SplitTime>& p_times);

    void UpdateStates(const int& p_raceId, const std::map<int, CompetitorState>& p_states);

    void AddCompetitors(const std::list<ICompetitorPtr>& p_list);

protected:
    virtual void SelectItem(const QModelIndex& p_index) = 0;
    virtual void AddNewItem() = 0;
    virtual void FindItemByTag(const QString& p_tag) = 0;
    /**
     *	@brief set all buttons enabled/disabled depending on the race state.
     */
    void SetButtonsEnableOnRaceState(const bool& p_raceStarted);

protected :
    void OnItemSelected(const QModelIndex& p_index);
    void OnItemClicked(const QModelIndex& p_index);
    void OnAddItem();
    void OnLink();
    void OnAutoTools();
    void OnRaceStart();
    void OnRaceStop();
    void OnUpdateRaceTime(QTime p_time);
    void OnAutoSendTime();
    void OnAutoSaveRace();
    void OnNewArrival(QString p_tag, QTime time);
    void OnFindItemByTag(QString p_tag);
    void OnFindByTagCanceled();
    void OnShortCutDelete();
    void OnClearLiveTiming();
    void OnSyncCompetitors();
    void updateNetworkState(const bool& networkStatus);

protected:
    Ui_RaceView m_ui;
    RaceConfiguration m_raceConfig;
    RaceModelPtr m_model;
    QSortFilterProxyModel* m_proxySortModel;
    IDetector& mDetector;
    ServerBinder& mServer;
    DialogSafetyCheck m_safetyCheckView;
    QTimer m_timerSave;
    bool m_safetyCheckByPass;
    bool m_fastTrack = false;
    QString m_raceFilePath;
    QProgressDialog m_progress;
    std::mutex mSaveRaceLock;
};
