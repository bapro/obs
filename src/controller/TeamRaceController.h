/*
 * TeamRaceController.h
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#pragma once

#include "Team.h"
#include <RaceController.h>

class TeamRaceController : public RaceController {
public:
    TeamRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent = 0);
    virtual ~TeamRaceController();

protected:
    /**
     * Inherited from RacController
     */
    virtual void SelectItem(const QModelIndex& p_index);
    virtual void AddNewItem();
    virtual void FindItemByTag(const QString& p_tag);

    void OpenEditTeam(TeamPtr p_team);

    virtual QDataStream& Serialize(QDataStream& p_out);

    virtual QDataStream& Deserialize(QDataStream& p_in);
};
