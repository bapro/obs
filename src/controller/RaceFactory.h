/*
 * RaceFactory.h
 *
 *  Created on: 1 oct. 2016
 *      Author: baptiste
 */

#pragma once

#include "IDetector.h"
#include "RaceConfiguration.h"
#include "qwidget.h"

class RaceController;

class RaceFactory {
public:
    RaceFactory();
    virtual ~RaceFactory();

    static RaceController* MakeController(
        const RaceConfiguration& raceConfig,
        IDetector& detector,
        ServerBinder& serverBinder,
        QWidget* parent);
};
