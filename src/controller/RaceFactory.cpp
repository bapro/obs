/*
 * RaceFactory.cpp
 *
 *  Created on: 1 oct. 2016
 *      Author: baptiste
 */

#include "RaceConfiguration.h"
#include "RaceController.h"
#include "SoloRaceController.h"
#include "TeamRaceController.h"
#include "TimeTrialRaceController.h"
#include <RaceFactory.h>

RaceFactory::RaceFactory() {}

RaceFactory::~RaceFactory() {}

RaceController* RaceFactory::MakeController(
    const RaceConfiguration& raceConfig,
    IDetector& detector,
    ServerBinder& serverBinder,
    QWidget* parent)
{
    RaceController* retcontroller = 0;
    switch (raceConfig.GetRaceType()) {
    case Race::SOLO: {
        retcontroller = new SoloRaceController(detector, serverBinder, parent);
        break;
    }
    case Race::TEAM: {
        retcontroller = new TeamRaceController(detector, serverBinder, parent);
        break;
    }
    case Race::TIME_TRIAL: {
        retcontroller = new TimeTrialRaceController(detector, serverBinder, parent);
        break;
    }
    case Race::UNDEFINED: {
        qDebug() << "ERROR";
        break;
    }
    }
    retcontroller->SetRaceConfiguration(raceConfig);
    return retcontroller;
}
