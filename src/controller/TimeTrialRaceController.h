/*
 * TimeTrialRaceController.h
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#pragma once

#include "Runner.h"
#include <RaceController.h>

class DialogTimeTrial;

class TimeTrialRaceController : public RaceController {
public:
    TimeTrialRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent = 0);
    virtual ~TimeTrialRaceController();

public slots:
    void OnDisplayTimeTrialView();

protected:
    /**
     * Inherited from RacController
     */
    virtual void SelectItem(const QModelIndex& p_index);
    virtual void AddNewItem();
    virtual void FindItemByTag(const QString& p_tag);

    void OpenEditRunner(RunnerPtr p_runner);

    virtual QDataStream& Serialize(QDataStream& p_out);

    virtual QDataStream& Deserialize(QDataStream& p_in);

private:
    DialogTimeTrial* m_dialogTT;
};
