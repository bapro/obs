/*
 * RaceController.cpp
 *
 *  Created on: 8 mai 2016
 *      Author: baptiste
 */

#include "qinputdialog.h"
#include <RaceController.h>
#include <qdebug.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qshortcut.h>

#include "CSVGenerator.h"
#include "DialogEditRFIDConf.h"
#include "DialogEditRaceConf.h"
#include "DialogEditStartedTime.h"
#include "DialogResults.h"
#include "RaceConfiguration.h"
#include "ResultGenerator.h"
#include "ServerBinder.h"
#include "qmenu.h"

const int INTERVAL_SAVE = 30000;

RaceController::RaceController(RaceModelPtr model, IDetector& detector, ServerBinder& serverBinder, QWidget* parent)
    : QWidget(parent)
    , m_ui()
    , m_raceConfig()
    , m_model(model)
    , m_proxySortModel(new QSortFilterProxyModel(this))
    , mDetector(detector)
    , mServer(serverBinder)
    , m_safetyCheckView(this)
    , m_timerSave()
    , m_safetyCheckByPass(false)
    , m_raceFilePath()
    , m_progress(tr("Waiting for Tag"), tr("Cancel"), 0, 0, this)
{
    m_ui.setupUi(this);

    m_proxySortModel->setSourceModel(m_model.get());
    m_ui.m_tableView->setModel(m_proxySortModel);
    m_ui.m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_ui.m_tableView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_ui.m_tableView->setSortingEnabled(true);
    m_ui.m_tableView->verticalHeader()->hide();
    m_ui.m_tableView->horizontalHeader()->setSectionsClickable(true);
    m_ui.m_tableView->horizontalHeader()->setSectionsMovable(true);
    m_ui.m_tableView->sortByColumn(0, Qt::AscendingOrder);

    m_ui.m_start->setText(tr("Start"));
    m_ui.m_stop->setText(tr("Stop"));

    m_progress.close();
    m_progress.setWindowModality(Qt::WindowModal);
    m_progress.setAutoClose(true);

    QShortcut* shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), this);
    connect(shortcut, &QShortcut::activated, this, &RaceController::OnShortCutDelete);

    connect(m_ui.m_tableView, &QTableView::doubleClicked, this, &RaceController::OnItemSelected);
    connect(m_ui.m_tableView, &QTableView::clicked, this, &RaceController::OnItemClicked);
    connect(m_ui.m_addItem, &QToolButton::clicked, this, &RaceController::OnAddItem);
    connect(m_ui.m_link, &QToolButton::clicked, this, &RaceController::OnLink);
    connect(m_ui.m_autoTools, &QToolButton::clicked, this, &RaceController::OnAutoTools);
    connect(m_ui.m_start, &QToolButton::clicked, this, &RaceController::OnRaceStart);
    connect(m_ui.m_stop, &QToolButton::clicked, m_model.get(), &RaceModel::OnRaceStop);
    connect(m_ui.m_stop, &QToolButton::clicked, this, &RaceController::OnRaceStop);
    connect(m_model.get(), &RaceModel::UpdateRaceTime, this, &RaceController::OnUpdateRaceTime);

    connect(&m_timerSave, &QTimer::timeout, this, &RaceController::OnAutoSaveRace);
    connect(&m_timerSave, &QTimer::timeout, this, &RaceController::OnAutoSendTime);
    connect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
    connect(&mServer, &ServerBinder::timeUpdated, this, &RaceController::UpdateTimes);
    connect(&mServer, &ServerBinder::stateUpdated, this, &RaceController::UpdateStates);
    connect(&mServer, &ServerBinder::networkStatus, this, &RaceController::updateNetworkState);

    connect(&m_progress, &QProgressDialog::canceled, this, &RaceController::OnFindByTagCanceled);

    m_safetyCheckView.hide();
    SetButtonsEnableOnRaceState(false);

    mServer.isNetworkAccessible();
}

RaceController::~RaceController()
{
    //	if(m_model->IsRaceStarted())
    //	{
    //		OnAutoSaveRace();
    //	}
}

void RaceController::SaveRace(const QString& p_raceFile)
{
    std::scoped_lock lock(mSaveRaceLock);
    QFile file(p_raceFile);

    if (file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_5_5);

        Serialize(out);

        file.flush();
        file.close();
    } else {
        QMessageBox::warning(
            this,
            tr("Save Error "),
            tr("Failed to save race to: ") + p_raceFile,
            QMessageBox::Ok,
            QMessageBox::NoButton);
    }
}

void RaceController::SetRaceConfiguration(const RaceConfiguration& p_raceConfiguration)
{
    m_raceConfig = p_raceConfiguration;
    m_model->SetRaceConfiguration(p_raceConfiguration);
    m_model->Update();
}

RaceConfiguration& RaceController::GetRaceConfiguration() { return m_raceConfig; }

void RaceController::DisconnectRFIDDriver()
{
    disconnect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
}

void RaceController::ConnectRFIDDRiver()
{
    connect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
}

void RaceController::SetFilePath(const QString& p_filePath) { m_raceFilePath = p_filePath; }

void RaceController::AddButtonToPluginLayout(QToolButton* p_pluginButton)
{
    m_ui.m_layoutPlugin->addWidget(p_pluginButton);
}

QDataStream& RaceController::Deserialize(QDataStream& p_in)
{
    SetButtonsEnableOnRaceState(false);
    if (m_model && m_model->IsRaceStarted()) {
        m_timerSave.start(INTERVAL_SAVE);
        SetButtonsEnableOnRaceState(true);
    }
    return p_in;
}

bool RaceController::Merge(RaceController* p_controller)
{
    bool mergeOK = false;
    if (m_raceConfig.GetRaceType() == p_controller->m_raceConfig.GetRaceType()) {
        m_model->Merge(p_controller->m_model);
        mergeOK = true;
    }
    return mergeOK;
}

void RaceController::UpdateTimes(const int& p_raceId, const std::list<SplitTime>& p_times)
{
    if (p_raceId == m_raceConfig.GetRaceID()) {
        m_model->UpdateTimes(p_times);
    }
}

void RaceController::UpdateStates(const int& p_raceId, const std::map<int, CompetitorState>& p_states)
{
    if (p_raceId == m_raceConfig.GetRaceID()) {
        m_model->UpdateStates(p_states);
    }
}

void RaceController::AddCompetitors(const std::list<ICompetitorPtr>& p_list) { m_model->AddFullList(p_list); }

void RaceController::OnItemSelected(const QModelIndex& p_index)
{
    QModelIndex index = m_proxySortModel->mapToSource(p_index);
    SelectItem(index);
}

void RaceController::OnItemClicked(const QModelIndex& p_index) { (void)p_index; }

void RaceController::OnShortCutDelete()
{
    QModelIndexList listSelected = m_ui.m_tableView->selectionModel()->selectedRows();

    if (listSelected.empty()) {
        QMessageBox::information(this, tr("Delete"), tr("Nothing to delete"), QMessageBox::Ok, QMessageBox::NoButton);
    } else {
        int choice = QMessageBox::warning(
            this,
            tr("Delete"),
            tr("You are about to delete %1 items. Continue ?").arg(listSelected.size()),
            QMessageBox::Ok,
            QMessageBox::Cancel);

        if (choice != QMessageBox::Cancel) {
            std::list<ICompetitorPtr> listToRemove;

            // Map Item to source and build the list to delete
            for (auto indexToRemove : listSelected) {
                QModelIndex index = m_proxySortModel->mapToSource(indexToRemove);
                ICompetitorPtr competitor = m_model->GetItem(index);

                if (competitor) {
                    listToRemove.push_back(competitor);
                }
            }

            m_model->RemoveItems(listToRemove);
        }
    }
}

void RaceController::OnClearLiveTiming()
{
    int ret = QMessageBox::warning(
        this,
        tr("Clear Live Timing "),
        tr("You're going to clear all live timing informations."),
        QMessageBox::Ok | QMessageBox::Cancel,
        QMessageBox::Ok);

    if (ret == QMessageBox::Ok) {
        mServer.clearLiveTiming(m_raceConfig.GetRaceID());
    }
}
void RaceController::OnAddItem() { AddNewItem(); }

void RaceController::OnLink()
{
    QMenu* menu = new QMenu(this);
    QAction* sendTime = new QAction(tr("Force send time"), this);
    QAction* razTime = new QAction(tr("Clear live timing"), this);
    QAction* sync = new QAction(tr("Synchronize competitors"), this);
    menu->addAction(sendTime);
    menu->addAction(razTime);
    menu->addAction(sync);

    QAction* actionSelected = menu->exec(m_ui.m_tableView->mapToGlobal(m_ui.m_autoTools->pos()));

    if (actionSelected == sendTime) {
        OnAutoSendTime();
    } else if (actionSelected == razTime) {
        OnClearLiveTiming();
    } else if (actionSelected == sync) {
        OnSyncCompetitors();
    }
}

void RaceController::OnAutoTools()
{
    QMenu* menu = new QMenu(this);
    QAction* raceConfig = new QAction(tr("Race Configuration"), this);
    QAction* autoset = new QAction(tr("Autoset"), this);
    QAction* autogenerate = new QAction(tr("Auto-generation"), this);
    QAction* fastTrack = new QAction(tr("Fast Chip registration"), this);
    QAction* safetyCheck = new QAction(tr("Safety Check"), this);
    QAction* setRaceStarted = new QAction(tr("Set started time"), this);
    QAction* findTag = new QAction(tr("Find owner tag"), this);
    QAction* exportCSVList = new QAction(tr("Export CSV"), this);
    QAction* exportResults = new QAction(tr("Export results"), this);
    QAction* resultsView = new QAction(tr("Open results view"), this);
    QAction* saveRace = new QAction(tr("Save"), this);

    safetyCheck->setCheckable(true);
    safetyCheck->setChecked(m_safetyCheckByPass);

    fastTrack->setCheckable(true);
    fastTrack->setChecked(m_fastTrack);

    menu->addAction(raceConfig);
    menu->addAction(autoset);
    menu->addAction(autogenerate);
    menu->addAction(fastTrack);

    if (m_raceConfig.IsSafetyCheck()) {
        menu->addAction(safetyCheck);
    }

    menu->addAction(setRaceStarted);
    menu->addAction(findTag);
    menu->addAction(resultsView);
    menu->addAction(exportCSVList);
    menu->addAction(exportResults);
    menu->addAction(saveRace);

    QAction* actionSelected = menu->exec(m_ui.m_tableView->mapToGlobal(m_ui.m_autoTools->pos()));

    if (actionSelected == raceConfig) {
        DialogEditRaceConf raceConf(m_raceConfig, this);
        raceConf.exec();

        if (raceConf.result() == QDialog::Accepted) {
            m_raceConfig = raceConf.GetRaceConfiguration();
            m_model->SetRaceConfiguration(m_raceConfig);
            m_model->Update();
        }
    } else if (actionSelected == autoset) {
        int bibNumber = QInputDialog::getInt(
            m_ui.m_tableView, tr("Autoset Bib number"), tr("Starting bib number:"), 1, 1);
        m_model->AutoSetBibNumber(bibNumber);
    } else if (actionSelected == autogenerate) {
        int itemNumber = QInputDialog::getInt(m_ui.m_tableView, tr("Auto-generation"), tr("Number to generate:"), 1, 1);
        m_model->AutoGenerateItems(itemNumber);
    } else if (actionSelected == fastTrack) {
        m_fastTrack = !m_fastTrack;
    } else if (actionSelected == safetyCheck) {
        m_safetyCheckByPass = !m_safetyCheckByPass;
    } else if (actionSelected == setRaceStarted) {
        DialogEditStartedTime* diagGetTime = new DialogEditStartedTime(this);

        if (QDialog::Accepted == diagGetTime->exec()) {
            m_safetyCheckByPass = false;
            m_model->SetStartedTime(diagGetTime->GetTimeEdit());

            if (!m_timerSave.isActive()) {
                OnAutoSaveRace();
                m_timerSave.start(INTERVAL_SAVE);
                SetButtonsEnableOnRaceState(true);
            }
        }
    } else if (actionSelected == findTag) {
        disconnect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
        connect(&mDetector, &IDetector::NewTag, this, &RaceController::OnFindItemByTag);
        m_progress.open();
    } else if (actionSelected == exportCSVList) {
        QString fileName = QFileDialog::getSaveFileName(m_ui.m_tableView, tr("Save Results"), "", tr("Files (*.csv)"));

        CSVGenerator gene(fileName);
        gene.Generate(m_model);
    } else if (actionSelected == exportResults) {
        QString fileName = QFileDialog::getSaveFileName(m_ui.m_tableView, tr("Save Results"), "", tr("Files (*.csv)"));

        // ResultGenerator
        ResultGenerator gene(m_model->GetAllItems(), m_model->GetRaceConfiguration());
        gene.SaveToCsv(fileName);
    } else if (actionSelected == resultsView) {
        DialogResults* result = new DialogResults(this);
        result->SetModel(m_model.get());
        result->show();
    } else if (actionSelected == saveRace) {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save file"), "", tr("Files (*.obs)"));

        if (!fileName.isEmpty()) {
            m_raceFilePath = fileName;
            OnAutoSaveRace();
        }
    }
}

void RaceController::OnRaceStart()
{
    mServer.clearLiveTiming(m_raceConfig.GetRaceID());
    m_safetyCheckByPass = false;
    m_model->StartRace();

    if (!m_timerSave.isActive()) {
        OnAutoSaveRace();
        m_timerSave.start(INTERVAL_SAVE);
    } // Start timer to save all race every minute
    SetButtonsEnableOnRaceState(true);
}

void RaceController::OnRaceStop() { SetButtonsEnableOnRaceState(false); }

void RaceController::OnUpdateRaceTime(QTime p_time) { m_ui.m_labelChrono->setText(p_time.toString()); }

void RaceController::OnAutoSaveRace()
{
    if (m_raceFilePath.isEmpty()) {
        m_raceFilePath = m_raceConfig.GetRaceName() + (QTime::currentTime().toString().append(".obs"));
        m_raceFilePath.replace(' ', "_");
        m_raceFilePath.replace(':', "-");
    }
    SaveRace(m_raceFilePath);
}

void RaceController::OnAutoSendTime() { mServer.sendTime(m_model->GetAllItems(), m_raceConfig); }

void RaceController::OnNewArrival(QString p_tag, QTime time)
{
    if (m_safetyCheckByPass) {
        ICompetitorPtr competitor = m_model->SafetyCheck(p_tag);

        if (competitor && competitor->IsValid()) {
            m_safetyCheckView.SetCompetitor(competitor);
        }
    } else {
        std::scoped_lock lock(mSaveRaceLock);
        SplitTime newTime(m_raceConfig.GetCurrentBaseStation().GetId(), p_tag);
        newTime.SetTime(time);
        ICompetitorPtr competitor = m_model->AddSplitTimeToItem(newTime);

        if (competitor && competitor->IsValid()) {
            m_safetyCheckView.SetCompetitor(competitor);
            m_safetyCheckView.SetRank(m_model->GetRank(competitor, m_raceConfig.GetCurrentBaseStation()));
        }
    }
}
void RaceController::OnFindItemByTag(QString p_tag)
{
    m_progress.reset();
    disconnect(&mDetector, &IDetector::NewTag, this, &RaceController::OnFindItemByTag);
    connect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
    FindItemByTag(p_tag);
}

void RaceController::OnFindByTagCanceled()
{
    disconnect(&mDetector, &IDetector::NewTag, this, &RaceController::OnFindItemByTag);
    connect(&mDetector, &IDetector::NewTag, this, &RaceController::OnNewArrival);
}

void RaceController::SetButtonsEnableOnRaceState(const bool& p_raceStarted)
{
    m_ui.m_start->setEnabled(!p_raceStarted);
    m_ui.m_stop->setEnabled(p_raceStarted);
}
void RaceController::OnSyncCompetitors() { mServer.syncCompetitors(m_model->GetAllItems(), m_raceConfig); }
void RaceController::updateNetworkState(const bool& networkStatus)
{
    QPalette pal = m_ui.m_link->palette();
    if (!networkStatus) {
        m_ui.m_link->setStyleSheet("background-color: #ff0000");
    } else {
        m_ui.m_link->setStyleSheet("background-color: #0066cc");
    }
}
