/*
 * SoloRaceController.cpp
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#include "DialogEditRunner.h"
#include "SoloRaceModel.h"
#include "qmessagebox.h"
#include <SoloRaceController.h>

SoloRaceController::SoloRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent)
    : RaceController(std::make_shared<SoloRaceModel>(), detector, serverBinder, parent)
{
}

SoloRaceController::~SoloRaceController() {}

void SoloRaceController::SelectItem(const QModelIndex& p_index)
{
    OpenEditRunner(std::dynamic_pointer_cast<Runner>(m_model->GetItem(p_index)));
}

void SoloRaceController::AddNewItem()
{
    DialogEditRunner editRunner(m_model, this);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editRunner, &DialogEditRunner::OnNewRFIDTag);

    RunnerPtr runner = std::dynamic_pointer_cast<Runner>(m_model->GetNextEmptyItem());
    editRunner.SetRunner(*runner);
    editRunner.exec();

    if (editRunner.result() == QDialog::Accepted) {
        *runner = editRunner.GetEditedRunner();
        m_model->AddItem(runner);
    }
    ConnectRFIDDRiver();
}

void SoloRaceController::OpenEditRunner(RunnerPtr p_runner)
{
    DialogEditRunner editRunner(m_model, this);
    editRunner.SetRunner(*p_runner);
    editRunner.SetFastTrackEnabled(m_fastTrack);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editRunner, &DialogEditRunner::OnNewRFIDTag);

    editRunner.exec();

    if (editRunner.result() == QDialog::Accepted) {
        editRunner.UpdateRunner();
    }
    ConnectRFIDDRiver();
}

void SoloRaceController::FindItemByTag(const QString& p_tag)
{
    RunnerPtr runner = std::dynamic_pointer_cast<Runner>(m_model->GetItem(p_tag));
    if (runner) {
        OpenEditRunner(runner);
    } else {
        QMessageBox::warning(
            this, tr("Not Found"), tr("RFID Tag not associated : ") + p_tag, QMessageBox::Ok, QMessageBox::NoButton);
    }
}

QDataStream& SoloRaceController::Serialize(QDataStream& p_out)
{
    p_out << m_raceConfig;
    p_out << std::dynamic_pointer_cast<SoloRaceModel>(m_model);
    return p_out;
}

QDataStream& SoloRaceController::Deserialize(QDataStream& p_in)
{
    p_in >> std::dynamic_pointer_cast<SoloRaceModel>(m_model);
    RaceController::Deserialize(p_in);
    return p_in;
}
