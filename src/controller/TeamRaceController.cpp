/*
 * TeamRaceController.cpp
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#include "DialogEditTeam.h"
#include "TeamRaceModel.h"
#include "qmessagebox.h"
#include <TeamRaceController.h>

TeamRaceController::TeamRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent)
    : RaceController(std::make_shared<TeamRaceModel>(), detector, serverBinder, parent)
{
}

TeamRaceController::~TeamRaceController() {}

void TeamRaceController::SelectItem(const QModelIndex& p_index)
{
    OpenEditTeam(std::dynamic_pointer_cast<Team>(m_model->GetItem(p_index)));
}

void TeamRaceController::AddNewItem()
{
    DialogEditTeam editTeam(m_model, this);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editTeam, &DialogEditTeam::OnNewRFIDTag);

    TeamPtr team = std::dynamic_pointer_cast<Team>(m_model->GetNextEmptyItem());

    editTeam.SetTeam(*team);
    editTeam.exec();

    if (editTeam.result() == QDialog::Accepted) {
        *team = editTeam.GetEditedTeam();
        m_model->AddItem(team);
    }
    ConnectRFIDDRiver();
}

void TeamRaceController::OpenEditTeam(TeamPtr p_team)
{
    DialogEditTeam editTeam(m_model, this);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editTeam, &DialogEditTeam::OnNewRFIDTag);

    editTeam.SetTeam(*p_team);
    editTeam.exec();

    if (editTeam.result() == QDialog::Accepted) {
        *p_team = editTeam.GetEditedTeam();
        m_model->UpdateItem(p_team);
    }
    ConnectRFIDDRiver();
}

void TeamRaceController::FindItemByTag(const QString& p_tag)
{
    TeamPtr team = std::dynamic_pointer_cast<Team>(m_model->GetItem(p_tag));
    if (team) {
        OpenEditTeam(team);
    } else {
        QMessageBox::warning(
            this, tr("Not Found"), tr("RFID Tag not associated : ") + p_tag, QMessageBox::Ok, QMessageBox::NoButton);
    }
}

QDataStream& TeamRaceController::Serialize(QDataStream& p_out)
{
    p_out << m_raceConfig;
    //		out << TagUnicityManager::instance();
    p_out << std::dynamic_pointer_cast<TeamRaceModel>(m_model);
    return p_out;
}

QDataStream& TeamRaceController::Deserialize(QDataStream& p_in)
{
    p_in >> std::dynamic_pointer_cast<TeamRaceModel>(m_model);
    RaceController::Deserialize(p_in);
    return p_in;
}
