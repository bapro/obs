/*
 * TimeTrialRaceController.cpp
 *
 *  Created on: 3 ao�t 2016
 *      Author: bapti
 */

#include "DialogEditRunner.h"
#include "DialogTimeTrial.h"
#include "TimeTrialRaceModel.h"
#include "qmessagebox.h"
#include "qtoolbutton.h"
#include <TimeTrialRaceController.h>

TimeTrialRaceController::TimeTrialRaceController(IDetector& detector, ServerBinder& serverBinder, QWidget* parent)
    : RaceController(std::make_shared<TimeTrialRaceModel>(), detector, serverBinder, parent)
    , m_dialogTT(new DialogTimeTrial(this))
{
    QToolButton* tt = new QToolButton(this);
    tt->setObjectName("TimeTrialView");
    AddButtonToPluginLayout(tt);

    connect(tt, &QToolButton::clicked, this, &TimeTrialRaceController::OnDisplayTimeTrialView);
    connect(
        (TimeTrialRaceModel*)m_model.get(),
        &TimeTrialRaceModel::NewIntervalRunner,
        m_dialogTT,
        &DialogTimeTrial::OnIntervalRunner);
}

TimeTrialRaceController::~TimeTrialRaceController() {}

void TimeTrialRaceController::OnDisplayTimeTrialView()
{
    if (m_dialogTT->isVisible()) {
        m_dialogTT->hide();
    } else {
        if (!m_model->IsRaceStarted()) {
            TimeTrialRaceModelPtr TTRace = std::static_pointer_cast<TimeTrialRaceModel>(m_model);
            m_dialogTT->SetRunner(TTRace->GetFirstRunner());
        }
        m_dialogTT->show();
    }
}

void TimeTrialRaceController::SelectItem(const QModelIndex& p_index)
{
    OpenEditRunner(std::dynamic_pointer_cast<Runner>(m_model->GetItem(p_index)));
}

void TimeTrialRaceController::AddNewItem()
{
    DialogEditRunner editRunner(m_model, this);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editRunner, &DialogEditRunner::OnNewRFIDTag);

    RunnerPtr runner = std::dynamic_pointer_cast<Runner>(m_model->GetNextEmptyItem());
    editRunner.SetRunner(*runner);
    editRunner.exec();

    if (editRunner.result() == QDialog::Accepted) {
        *runner = editRunner.GetEditedRunner();
        m_model->AddItem(runner);
    }
    ConnectRFIDDRiver();
}

void TimeTrialRaceController::OpenEditRunner(RunnerPtr p_runner)
{
    DialogEditRunner editRunner(m_model, this);
    editRunner.SetRunner(*p_runner);

    DisconnectRFIDDriver();
    connect(&mDetector, &IDetector::NewTag, &editRunner, &DialogEditRunner::OnNewRFIDTag);

    editRunner.exec();

    if (editRunner.result() == QDialog::Accepted) {
        editRunner.UpdateRunner();
    }
    ConnectRFIDDRiver();
}

void TimeTrialRaceController::FindItemByTag(const QString& p_tag)
{
    RunnerPtr runner = std::static_pointer_cast<Runner>(m_model->GetItem(p_tag));
    if (runner) {
        OpenEditRunner(runner);
    } else {
        QMessageBox::warning(
            this, tr("Not Found"), tr("RFID Tag not associated : ") + p_tag, QMessageBox::Ok, QMessageBox::NoButton);
    }
}

QDataStream& TimeTrialRaceController::Serialize(QDataStream& p_out)
{
    p_out << m_raceConfig;
    p_out << std::dynamic_pointer_cast<TimeTrialRaceModel>(m_model);
    return p_out;
}

QDataStream& TimeTrialRaceController::Deserialize(QDataStream& p_in)
{
    p_in >> std::static_pointer_cast<TimeTrialRaceModel>(m_model);
    RaceController::Deserialize(p_in);
    return p_in;
}
