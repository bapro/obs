/*
 * SoloRaceModel.h
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_MODEL_SOLORACEMODEL_H_
#define SRC_MODEL_SOLORACEMODEL_H_

#include "Runner.h"
#include "SplitTime.h"
#include "qobject.h"
#include <RaceModel.h>
#include <qmap.h>
#include <vector>

class SoloRaceModel : public RaceModel {
    Q_OBJECT

public:
    SoloRaceModel(QObject* p_parent = 0);
    virtual ~SoloRaceModel();

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    virtual QVariant GetDataFromColumn(const ICompetitorPtr p_runr, const int& p_column) const override;

    virtual void AutoGenerateItems(const int& p_count) override;

    virtual ICompetitorPtr GetNextEmptyItem() override;

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const SoloRaceModel& p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, SoloRaceModel& p_raceModel);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<SoloRaceModel> p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<SoloRaceModel> p_raceModel);

private:
    int m_version;
};

#endif /* SRC_MODEL_SOLORACEMODEL_H_ */
