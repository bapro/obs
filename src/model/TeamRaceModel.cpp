/*
 * TeamModel.cpp
 *
 *  Created on: 30 avr. 2016
 *      Author: baptiste
 */

#include "qapplication.h"
#include "qdebug.h"
#include <QDataStream>
#include <TeamRaceModel.h>

TeamRaceModel::TeamRaceModel(QObject* p_parent)
    : RaceModel(p_parent)
    , m_version(1)
{
    m_colCount = 4;
}

TeamRaceModel::~TeamRaceModel() {}

QVariant TeamRaceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant ret = QVariant::Invalid;

    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: {
                ret = tr("Bib Number");
                break;
            }
            case 1: {
                ret = tr("Team Name");
                break;
            }
            case 2: {
                ret = tr("Sexe");
                break;
            }
            case 3: {
                ret = tr("Race time");
                break;
            }
            default:
                ret = tr("Unknown");
                break;
            }

            if (section >= m_colCount) {
                unsigned int split = (unsigned int)section - (unsigned int)m_colCount;
                if (split < m_dynamicBaseStationInfos.size()) {
                    ret = m_dynamicBaseStationInfos.at(split).first;
                }
            }
        }
    }
    return ret;
}
QVariant TeamRaceModel::GetDataFromColumn(const ICompetitorPtr p_competitor, const int& p_column) const
{
    QVariant retVar;

    TeamPtr team = std::dynamic_pointer_cast<Team>(p_competitor);

    if (team) {
        switch (p_column) {
        case 0: {
            retVar = team->GetBibNumber();
            break;
        }
        case 1: {
            retVar = team->GetTeamName();
            break;
        }
        case 2: {
            retVar = ICompetitor::SexeToString(team->GetSexe());
            break;
        }
        case 3: {
            QTime t = team->GetRaceTime();
            retVar = t.toString();
            break;
        }
        default:
            break;
        }

        if (p_column >= m_colCount) {
            unsigned int split = (unsigned int)p_column - (unsigned int)m_colCount;
            if (split < m_dynamicBaseStationInfos.size()) {
                int baseId = m_dynamicBaseStationInfos.at(split).second.GetId();
                if (!m_dynamicBaseStationInfos.at(split).first.contains(tr("Rank"))) {
                    retVar = team->GetSplitTimeFromBaseStation(baseId).GetTime().toString();
                } else {
                    int rank = GetRank(team, m_dynamicBaseStationInfos.at(split).second);
                    if (rank != -1) {
                        retVar = rank;
                    }
                }
            }
        }
    }
    return retVar;
}

void TeamRaceModel::AutoGenerateItems(const int& p_count)
{
    emit layoutAboutToBeChanged();
    for (int nb = 0; nb < p_count; ++nb) {
        TeamPtr team = std::make_shared<Team>(ComputeNextDbId());
        m_items.push_back(team);
    }
    emit layoutChanged();

    AutoSetBibNumber(1);
}
ICompetitorPtr TeamRaceModel::GetNextEmptyItem()
{
    TeamPtr team = std::make_shared<Team>(ComputeNextDbId());
    team->SetBibNumber(ComputeNextBib());
    return team;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const TeamRaceModel& p_raceModel)
{
    p_out << p_raceModel.m_version;

    QVector<Team> vectToCopy;

    for (ICompetitorPtr competitor : p_raceModel.m_items) {
        TeamPtr rnr = std::dynamic_pointer_cast<Team>(competitor);
        vectToCopy.push_back(*rnr);
    }
    p_out << vectToCopy;

    p_out << static_cast<const RaceModel&>(p_raceModel);
    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, TeamRaceModel& p_raceModel)
{
    p_in >> p_raceModel.m_version;

    QVector<Team> vectToCopy;
    p_in >> vectToCopy;

    for (const Team& rnr : vectToCopy) {
        p_raceModel.m_items.push_back(std::make_shared<Team>(rnr));
    }

    p_in >> static_cast<RaceModel&>(p_raceModel);
    return p_in;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TeamRaceModel> p_raceModel)
{
    return p_out << *p_raceModel;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TeamRaceModel> p_raceModel) { return p_in >> *p_raceModel; }
