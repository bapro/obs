/*
 * TeamModel.h
 *
 *  Created on: 30 avr. 2016
 *      Author: baptiste
 */

#ifndef SRC_MODEL_TEAMRACEMODEL_H_
#define SRC_MODEL_TEAMRACEMODEL_H_

#include "Team.h"
#include "qobject.h"
#include <RaceModel.h>

class TeamRaceModel : public RaceModel {
    Q_OBJECT

public:
    TeamRaceModel(QObject* p_parent = 0);
    virtual ~TeamRaceModel();

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    virtual QVariant GetDataFromColumn(const ICompetitorPtr p_competitor, const int& p_column) const;

    virtual void AutoGenerateItems(const int& p_count);

    virtual ICompetitorPtr GetNextEmptyItem();

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const TeamRaceModel& p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, TeamRaceModel& p_raceModel);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TeamRaceModel> p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TeamRaceModel> p_raceModel);

private:
    int m_version;
};

#endif /* SRC_MODEL_TEAMRACEMODEL_H_ */
