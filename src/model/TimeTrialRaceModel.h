/*
 * TimeTrialRaceModel.h
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_MODEL_TIMETRIALRACEMODEL_H_
#define SRC_MODEL_TIMETRIALRACEMODEL_H_

#include "Runner.h"
#include "SplitTime.h"
#include "qobject.h"
#include <SoloRaceModel.h>
#include <qmap.h>
#include <qtimer.h>
#include <vector>

Q_DECLARE_METATYPE(Runner);

class TimeTrialRaceModel : public SoloRaceModel {
    Q_OBJECT

public:
    TimeTrialRaceModel(QObject* p_parent = 0);
    virtual ~TimeTrialRaceModel();

    virtual void StartRace();

    virtual void StopRace();

    virtual void SetStartedTime(const QTime& p_time);

    Runner GetFirstRunner() const;

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, const TimeTrialRaceModel& p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, TimeTrialRaceModel& p_raceModel);

    // ostream, << overloading
    friend QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TimeTrialRaceModel> p_raceModel);

    // istream, >> overloading
    friend QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TimeTrialRaceModel> p_raceModel);

private slots:
    void OnNewInterval();
    void OnStartChronoInterval();

signals:
    void NewIntervalRunner(Runner p_runer, int p_interval, int p_currentPos, int p_totalNumber);

private:
    int m_version;
    QTimer m_timerUpdate;
    std::list<RunnerPtr> m_trialCache;
};

typedef std::shared_ptr<TimeTrialRaceModel> TimeTrialRaceModelPtr;

#endif /* SRC_MODEL_TIMETRIALRACEMODEL_H_ */
