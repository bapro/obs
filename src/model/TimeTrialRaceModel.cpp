/*
 * TimeTrialRaceModel.cpp
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#include "Sorting.h"
#include "qdebug.h"
#include <QDataStream>
#include <src/model/TimeTrialRaceModel.h>

TimeTrialRaceModel::TimeTrialRaceModel(QObject* p_parent)
    : SoloRaceModel(p_parent)
    , m_version(1)
    , m_timerUpdate()
{
    connect(&m_timerUpdate, &QTimer::timeout, this, &TimeTrialRaceModel::OnNewInterval);
}

TimeTrialRaceModel::~TimeTrialRaceModel() {}

void TimeTrialRaceModel::StartRace()
{
    for (ICompetitorPtr item : m_items) {
        m_trialCache.push_back(std::static_pointer_cast<Runner>(item));
    }
    m_trialCache.sort(BibNumberSorter());

    m_ranking.clear();
    for (auto base : m_raceConfig.GetBaseStations()) {
        std::pair<int, std::map<int, int>> p = std::make_pair(base.GetId(), std::map<int, int>());
        m_ranking.insert(p);
    }

    m_chrono.Start();
    m_timerUpdate.start(m_raceConfig.GetInterval() * 1000);
    emit NewIntervalRunner(*(m_trialCache.front()), m_raceConfig.GetInterval(), 1, (int)m_items.size());
}
void TimeTrialRaceModel::StopRace()
{
    RaceModel::StopRace();
    m_timerUpdate.stop();
}

void TimeTrialRaceModel::OnNewInterval()
{
    emit layoutAboutToBeChanged();
    RunnerPtr item = m_trialCache.front();
    item->SetState(CompetitorState::RUNNING);
    item->ClearSplitTimes();

    SplitTime splitStart(RaceConfiguration::BASE_STATION_START, item->GetChipNumber());
    item->AddSplitTime(splitStart);

    emit layoutChanged();
    m_trialCache.pop_front();

    // Emit new one coming
    if (!m_trialCache.empty()) {
        int currentPos = (int)m_items.size() - (int)m_trialCache.size() + 1;
        emit NewIntervalRunner(*m_trialCache.front(), m_raceConfig.GetInterval(), currentPos, (int)m_items.size());
    } else {
        m_timerUpdate.stop();
    }
}

void TimeTrialRaceModel::SetStartedTime(const QTime& p_time)
{
    emit layoutAboutToBeChanged();

    m_chrono.Stop();
    m_timerUpdate.stop();

    QTime time(p_time);
    for (ICompetitorPtr item : m_items) {
        m_trialCache.push_back(std::static_pointer_cast<Runner>(item));
    }
    m_trialCache.sort(BibNumberSorter());

    m_ranking.clear();
    for (auto base : m_raceConfig.GetBaseStations()) {
        std::pair<int, std::map<int, int>> p = std::make_pair(base.GetId(), std::map<int, int>());
        m_ranking.insert(p);
    }

    int elapsedNow = 0;
    for (auto it = m_trialCache.begin(); it != m_trialCache.end();) {
        RunnerPtr runr = *it;
        elapsedNow = time.msecsTo(QTime::currentTime());
        if (elapsedNow >= 0) {
            if ((m_raceConfig.IsSafetyCheck() && runr->GetState() == CompetitorState::CHECKED)
                || !m_raceConfig.IsSafetyCheck() || !m_raceConfig.IsCurrentMainStation()) {
                runr->SetState(CompetitorState::RUNNING);
                SplitTime splitStart(RaceConfiguration::BASE_STATION_START, runr->GetChipNumber());
                splitStart.SetTime(time);

                runr->UpdateSplitTime(splitStart);
                it = m_trialCache.erase(it++);
            } else {
                ++it;
            }
            time = time.addSecs(m_raceConfig.GetInterval());
        } else {
            break;
        }
    }
    m_chrono.SetStartedTime(p_time);
    emit layoutChanged();
    if (!m_trialCache.empty()) {
        int currentPos = (int)m_items.size() - (int)m_trialCache.size() + 1;
        emit NewIntervalRunner(*(m_trialCache.front()), m_raceConfig.GetInterval(), currentPos, (int)m_items.size());

        disconnect(&m_timerUpdate, &QTimer::timeout, this, &TimeTrialRaceModel::OnNewInterval);
        connect(&m_timerUpdate, &QTimer::timeout, this, &TimeTrialRaceModel::OnStartChronoInterval);
        m_timerUpdate.start(std::abs(elapsedNow));
    }
}

Runner TimeTrialRaceModel::GetFirstRunner() const
{
    std::list<RunnerPtr> cacheRunners;
    for (ICompetitorPtr item : m_items) {
        cacheRunners.push_back(std::static_pointer_cast<Runner>(item));
    }
    cacheRunners.sort(BibNumberSorter());
    return *cacheRunners.front();
}
void TimeTrialRaceModel::OnStartChronoInterval()
{
    m_timerUpdate.stop();
    disconnect(&m_timerUpdate, &QTimer::timeout, this, &TimeTrialRaceModel::OnStartChronoInterval);

    connect(&m_timerUpdate, &QTimer::timeout, this, &TimeTrialRaceModel::OnNewInterval);
    m_timerUpdate.start(m_raceConfig.GetInterval() * 1000);

    OnNewInterval();
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const TimeTrialRaceModel& p_raceModel)
{
    p_out << p_raceModel.m_version;

    QVector<Runner> vectToCopy;

    for (ICompetitorPtr competitor : p_raceModel.m_items) {
        RunnerPtr rnr = std::dynamic_pointer_cast<Runner>(competitor);
        vectToCopy.push_back(*rnr);
    }
    p_out << vectToCopy;
    p_out << static_cast<const RaceModel&>(p_raceModel);

    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, TimeTrialRaceModel& p_raceModel)
{
    p_in >> p_raceModel.m_version;

    QVector<Runner> vectToCopy;
    p_in >> vectToCopy;

    for (const Runner& rnr : vectToCopy) {
        p_raceModel.m_items.push_back(std::make_shared<Runner>(rnr));
    }
    p_in >> static_cast<RaceModel&>(p_raceModel);
    return p_in;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<TimeTrialRaceModel> p_raceModel)
{
    return p_out << *p_raceModel;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<TimeTrialRaceModel> p_raceModel)
{
    return p_in >> *p_raceModel;
}
