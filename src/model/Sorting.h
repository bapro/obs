/*
 * Sorting.h
 *
 *  Created on: 4 avr. 2016
 *      Author: baptiste
 */

#ifndef SRC_MODEL_SORTING_H_
#define SRC_MODEL_SORTING_H_

#include "BaseStation.h"
#include "ICompetitor.h"
#include "RaceConfiguration.h"
#include "RaceType.h"

struct CompareRaceTimeAtBaseStation {
    CompareRaceTimeAtBaseStation(const BaseStation& p_base, const Race::RaceType& p_racetype)
        : m_baseStation(p_base)
        , m_raceType(p_racetype)
    {
    }

    bool operator()(const ICompetitorPtr p_lhs, const ICompetitorPtr p_rhs) const
    {
        bool ret = false;
        SplitTime s1lhs = p_lhs->GetSplitTimeFromBaseStation(m_baseStation.GetId());
        SplitTime s1rhs = p_rhs->GetSplitTimeFromBaseStation(m_baseStation.GetId());

        int elapsedLhs = s1lhs.GetTime().elapsed();
        int elapsedRhs = s1rhs.GetTime().elapsed();

        if (m_raceType == Race::TIME_TRIAL && m_baseStation.GetId() != RaceConfiguration::BASE_STATION_START) {
            elapsedLhs -= p_lhs->GetSplitTimeFromBaseStation(m_baseStation.GetId() - 1).GetTime().elapsed();
            elapsedRhs -= p_rhs->GetSplitTimeFromBaseStation(m_baseStation.GetId() - 1).GetTime().elapsed();
        }

        if (!s1lhs.IsValid()) {
            ret = false;
        } else if (!s1rhs.IsValid()) {
            ret = true;
        } else if (
            (p_rhs->GetState() == CompetitorState::ABORTED || p_rhs->GetState() == CompetitorState::DISQUALIFIED
             || p_rhs->GetState() == CompetitorState::NOT_CHECKED)
            && (p_lhs->GetState() == CompetitorState::ABORTED || p_lhs->GetState() == CompetitorState::DISQUALIFIED
                || p_rhs->GetState() == CompetitorState::NOT_CHECKED)) {
            int e1 = p_lhs->GetState();
            int e2 = p_rhs->GetState();
            ret = (e1 < e2);
        } else if (
            p_lhs->GetState() == CompetitorState::ABORTED || p_lhs->GetState() == CompetitorState::DISQUALIFIED
            || p_rhs->GetState() == CompetitorState::NOT_CHECKED) {
            ret = false;
        } else if (
            p_rhs->GetState() == CompetitorState::ABORTED || p_rhs->GetState() == CompetitorState::DISQUALIFIED
            || p_rhs->GetState() == CompetitorState::NOT_CHECKED) {
            ret = true;
        } else {
            ret = elapsedRhs < elapsedLhs;
        }
        return ret;
    }

private:
    const BaseStation m_baseStation;
    const Race::RaceType m_raceType;
};

struct BibNumberSorter {
    BibNumberSorter() {}

    bool operator()(ICompetitorPtr p_lhs, ICompetitorPtr p_rhs)
    {
        return p_lhs->GetBibNumber() < p_rhs->GetBibNumber();
    }
};

#endif /* SRC_MODEL_SORTING_H_ */
