/*
 * SoloRaceModel.cpp
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#include "qdebug.h"
#include <QDataStream>
#include <src/model/SoloRaceModel.h>

SoloRaceModel::SoloRaceModel(QObject* p_parent)
    : RaceModel(p_parent)
    , m_version(1)
{
    m_colCount = 6;
    //	Update();
}

SoloRaceModel::~SoloRaceModel() {}

QVariant SoloRaceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant ret = QVariant::Invalid;

    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: {
                ret = tr("Bib Number");
                break;
            }
            case 1: {
                ret = tr("Last Name");
                break;
            }
            case 2: {
                ret = tr("First Name");
                break;
            }
            case 3: {
                ret = tr("Category");
                break;
            }
            case 4: {
                ret = tr("Sexe");
                break;
            }
            case 5: {
                ret = tr("Race time");
                break;
            }
            default:
                ret = tr("Unknown");
                break;
            }

            if (section >= m_colCount) {
                unsigned int split = (unsigned int)section - (unsigned int)m_colCount;
                if (split < m_dynamicBaseStationInfos.size()) {
                    ret = m_dynamicBaseStationInfos.at(split).first;
                }
            }
        }
        //		else if(orientation == Qt::Vertical)
        //		{
        //			ss << "V_" << section;
        //			return QString(ss.str().c_str());
        //		}
    }

    return ret;
}

QVariant SoloRaceModel::GetDataFromColumn(const ICompetitorPtr p_competitor, const int& p_column) const
{
    QVariant retVar;

    RunnerPtr runr = std::dynamic_pointer_cast<Runner>(p_competitor);

    if (runr) {
        switch (p_column) {
        case 0: {
            retVar = runr->GetBibNumber();
            break;
        }
        case 1: {
            retVar = runr->m_lastName;
            break;
        }
        case 2: {
            retVar = runr->m_firstName;
            break;
        }
        case 3: {
            retVar = Category::CategoryToString(runr->GetCategory());
            break;
        }
        case 4: {
            retVar = Runner::SexeToString(runr->GetSexe());
            break;
        }
        case 5: {
            QTime t = runr->GetRaceTime();
            retVar = t.toString();
            break;
        }
        default:
            break;
        }

        if (p_column >= m_colCount) {
            unsigned int split = (unsigned int)p_column - (unsigned int)m_colCount;

            if (split < m_dynamicBaseStationInfos.size()) {
                int baseId = m_dynamicBaseStationInfos.at(split).second.GetId();

                if (!m_dynamicBaseStationInfos.at(split).first.contains(tr("Rank"))) {
                    retVar = runr->GetSplitTimeFromBaseStation(baseId).GetTime().toString();
                } else {
                    int rank = GetRank(runr, m_dynamicBaseStationInfos.at(split).second);
                    if (rank != -1) {
                        retVar = rank;
                    }
                }
            }
        }
    } else {
        qDebug() << "Impossible to dynamic_cast to runnerPtr";
    }
    return retVar;
}

void SoloRaceModel::AutoGenerateItems(const int& p_count)
{
    emit layoutAboutToBeChanged();
    for (int nb = 0; nb < p_count; ++nb) {
        RunnerPtr runr = std::make_shared<Runner>(ComputeNextDbId());
        m_items.push_back(runr);
    }
    emit layoutChanged();
    AutoSetBibNumber(1);
}

ICompetitorPtr SoloRaceModel::GetNextEmptyItem()
{
    RunnerPtr runr = std::make_shared<Runner>(ComputeNextDbId());
    runr->SetBibNumber(ComputeNextBib());
    return runr;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, const SoloRaceModel& p_raceModel)
{
    p_out << p_raceModel.m_version;

    QVector<Runner> vectToCopy;

    for (ICompetitorPtr competitor : p_raceModel.m_items) {
        RunnerPtr rnr = std::static_pointer_cast<Runner>(competitor);
        vectToCopy.push_back(*rnr);
    }
    p_out << vectToCopy;
    p_out << static_cast<const RaceModel&>(p_raceModel);

    return p_out;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, SoloRaceModel& p_raceModel)
{
    p_in >> p_raceModel.m_version;

    QVector<Runner> vectToCopy;
    p_in >> vectToCopy;

    for (const Runner& rnr : vectToCopy) {
        p_raceModel.m_items.push_back(std::make_shared<Runner>(rnr));
    }
    p_in >> static_cast<RaceModel&>(p_raceModel);
    return p_in;
}

// ostream, << overloading
QDataStream& operator<<(QDataStream& p_out, std::shared_ptr<SoloRaceModel> p_raceModel)
{
    return p_out << *p_raceModel;
}

// istream, >> overloading
QDataStream& operator>>(QDataStream& p_in, std::shared_ptr<SoloRaceModel> p_raceModel) { return p_in >> *p_raceModel; }
