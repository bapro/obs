/*
 * ccDown.h
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#pragma once

#include "ServerBinder.h"
#include "ui_OBS.h"

class QSortFilterProxyModel;
class RaceController;
class IDetector;

class OBSMainView : public QMainWindow {
    Q_OBJECT

public:
    OBSMainView(QWidget* parent = 0);
    virtual ~OBSMainView();

    void onOpenFile();
    void onSaveAsFile();
    void onLoadFromDatabase();
    void onClearLiveTiming();
    void onRFIDConfiguration();
    void onBLEConfiguration();
    void onShowResults();
    void onNewRace();
    void onImportRace();
    void onCloseTab(int p_tab);
    void onMergeRace();
    void onShortCutRefresh();

private:
    RaceController* loadRaceFromFile(const QString& p_filePath);

private:
    Ui_OBS m_ui;
    ServerBinder mServer;
    std::unique_ptr<IDetector> mGenericDetector;
    std::vector<std::unique_ptr<IDetector>> mDetectors;
    std::vector<RaceController*> m_raceControllers;
};
