/*
 * DialogEditStartedTime.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGEDITSTARTEDTIME_H_
#define SRC_DIALOGEDITSTARTEDTIME_H_

#include "Runner.h"
#include "ui_EditStartedTime.h"
#include <qdialog.h>

class DialogEditStartedTime : public QDialog {
    Q_OBJECT

public:
    DialogEditStartedTime(QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogEditStartedTime();

    QTime GetTimeEdit() { return m_ui.m_editTime->time(); }
    //	virtual void accept();

private:
    Ui_EditStartedTime m_ui;
};

#endif /* SRC_DIALOGEDITSTARTEDTIME_H_ */
