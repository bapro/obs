/*
 * DialogEditTeam.h
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGEDITTEAM_H_
#define SRC_DIALOGEDITTEAM_H_

#include "RaceConfiguration.h"
#include "Team.h"
#include "WidgetRunner.h"
#include "ui_EditTeam.h"
#include "vector"
#include <qdialog.h>
#include <src/interface/RaceModel.h>

class TagUnicityManager;

class DialogEditTeam : public QDialog {
    Q_OBJECT

public:
    DialogEditTeam(RaceModelPtr p_raceModel, QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogEditTeam();
    /**
     * @brief mandatory
     */
    void SetTeam(const Team& p_team);

    Team GetEditedTeam();

private:
    void GenerateBaseStationTime();
    void GetUpdateBaseStationTime();
    void GenerateRelayViews();
    void GetRunnerFromRelayViews();

public slots:
    void OnNewRFIDTag(QString p_rfidTag);
    void OnMoveDown();
    void OnMoveUp();

private:
    Ui_EditTeam m_ui;
    //	std::vector<*WidgetRunner> m_runnerTabs;
    Team m_currentTeam;
    RaceModelPtr m_raceModel;
    RaceConfiguration m_raceConfig;
    const TagUnicityManager& m_tagManager;
};

#endif /* SRC_DialogEditTeam_H_ */
