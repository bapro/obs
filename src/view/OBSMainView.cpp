/*
 * ccDown.cpp
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#include "OBSMainView.h"
#include <QInputDialog>
#include <QtCore>
#include <qdebug.h>
#include <qelapsedtimer.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qshortcut.h>
#include <qtimer.h>
#include <qwidget.h>

#include "DetectorFactory.h"
#include "DialogEditBLEConf.h"
#include "DialogEditRFIDConf.h"
#include "DialogEditRaceConf.h"
#include "DialogResults.h"
#include "IDetector.h"
#include "JsonRaceImporter.h"
#include "RaceFactory.h"
#include "ResultGenerator.h"
#include "SoloRaceController.h"
#include <src/data/Settings.h>

namespace {
class GenericDetector : public IDetector {
public:
    GenericDetector() = default;
    virtual ~GenericDetector() = default;

signals:
    void NewTag(QString, QTime);
};
}
OBSMainView::OBSMainView(QWidget* p_parent)
    : QMainWindow(p_parent)
    , m_ui()
    , mServer(
          Settings::instance()->value<QString>(SETTINGS_HTTP_SERVER, ""),
          Settings::instance()->value<QString>(SETTINGS_HTTP_HASH, ""))
    , mGenericDetector(std::make_unique<GenericDetector>())
    , mDetectors()
    , m_raceControllers()

{
    m_ui.setupUi(this);

    mDetectors.push_back(DetectorFactory::buildFromSettings());
    mDetectors.push_back(DetectorFactory::buildBluetooth());

    for (const auto& detector : mDetectors) {
        connect(detector.get(), &IDetector::NewTag, mGenericDetector.get(), &IDetector::NewTag);
    }

    connect(m_ui.m_actionFileOpen, &QAction::triggered, this, &OBSMainView::onOpenFile);
    connect(m_ui.m_actionFileSaveAs, &QAction::triggered, this, &OBSMainView::onSaveAsFile);
    connect(m_ui.m_actionLoadDB, &QAction::triggered, this, &OBSMainView::onLoadFromDatabase);
    connect(m_ui.m_actionMerge, &QAction::triggered, this, &OBSMainView::onMergeRace);
    connect(m_ui.m_actionClear_Live_Timing, &QAction::triggered, this, &OBSMainView::onClearLiveTiming);
    connect(m_ui.m_actionRFIDConfiguration, &QAction::triggered, this, &OBSMainView::onRFIDConfiguration);
    connect(m_ui.m_actionBLEConfiguration, &QAction::triggered, this, &OBSMainView::onBLEConfiguration);
    connect(m_ui.m_actionResults, &QAction::triggered, this, &OBSMainView::onShowResults);
    connect(m_ui.m_actionNewRace, &QAction::triggered, this, &OBSMainView::onNewRace);
    connect(m_ui.m_actionImportRace, &QAction::triggered, this, &OBSMainView::onImportRace);
    connect(m_ui.m_tabWidget, &QTabWidget::tabCloseRequested, this, &OBSMainView::onCloseTab);

    m_ui.m_tabWidget->setTabsClosable(true);

    QShortcut* shortcut = new QShortcut(QKeySequence(Qt::Key_F5), this);
    shortcut->setContext(Qt::ApplicationShortcut);
    connect(shortcut, &QShortcut::activated, this, &OBSMainView::onShortCutRefresh);

    mServer.isNetworkAccessible();
    //	QShortcut *shortcutRfid = new QShortcut(QKeySequence(Qt::Key_F6), this);
    //	shortcutRfid->setContext(Qt::ApplicationShortcut);
    //	connect(shortcutRfid,&QShortcut::activated,&mDetector, &RFIDReader::SimulateRfidTag);

    const auto serverUrl = Settings::instance()->value<QString>(SETTINGS_HTTP_SERVER, "");
    const auto hash = Settings::instance()->value<QString>(SETTINGS_HTTP_HASH, "");
    if (serverUrl.isEmpty() || hash.isEmpty()) {
        QMessageBox::warning(
            this,
            tr("Server setup"),
            tr("Server connexion is not properly setup."),
            QMessageBox::Ok,
            QMessageBox::NoButton);
    }

    qDebug() << "SERVER: " << serverUrl;
    qDebug() << "Hash: " << hash;
}

OBSMainView::~OBSMainView() {}

void OBSMainView::onOpenFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Files (*.obs)"));
    qDebug() << "Open file: " << fileName;

    RaceController* newController = loadRaceFromFile(fileName);

    if (newController != 0) {
        m_ui.m_tabWidget->addTab(newController, newController->GetRaceConfiguration().GetRaceName());
        m_raceControllers.push_back(newController);
    }
}

RaceController* OBSMainView::loadRaceFromFile(const QString& p_filePath)
{
    QFile file(p_filePath);

    RaceController* newController = 0;

    if (file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_5_5);

        RaceConfiguration config;
        in >> config;

        newController = RaceFactory::MakeController(config, *mGenericDetector, mServer, this);
        newController->SetFilePath(p_filePath);
        newController->Deserialize(in);

        file.close();
    } else {
        QMessageBox::warning(this, tr("Import File "), "Import failed", QMessageBox::Ok, QMessageBox::NoButton);
    }
    return newController;
}

void OBSMainView::onSaveAsFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save file"), "", tr("Files (*.obs)"));

    RaceController* raceCtrl = dynamic_cast<RaceController*>(m_ui.m_tabWidget->currentWidget());
    if (raceCtrl && !fileName.isEmpty()) {
        raceCtrl->SaveRace(fileName);
    }
}

void OBSMainView::onLoadFromDatabase() {}

void OBSMainView::onClearLiveTiming()
{
    int ret = QMessageBox::warning(
        this,
        tr("Clear Live Timing "),
        tr("You're going to clear all live timing informations."),
        QMessageBox::Ok | QMessageBox::Cancel,
        QMessageBox::Ok);

    if (ret == QMessageBox::Ok) {
        int raceID = QInputDialog::getInt(this, "Clear Live Timing", "Race ID", -1, 0, 100, 1);
        mServer.clearLiveTiming(raceID);
    }
}

void OBSMainView::onRFIDConfiguration()
{
    for (auto raceCtrl : m_raceControllers) {
        raceCtrl->DisconnectRFIDDriver();
    }

    auto itFind = std::find_if(mDetectors.begin(), mDetectors.end(), [](const auto& detector) {
        return dynamic_cast<RFIDReader*>(detector.get());
    });

    if (itFind != mDetectors.end()) {
        const auto rfidReader = static_cast<RFIDReader*>(itFind->get());
        DialogEditRFIDConf rfidConf(*rfidReader, this);
        rfidConf.exec();

        if (rfidConf.result() == QDialog::Accepted) {
        }

        for (auto raceCtrl : m_raceControllers) {
            raceCtrl->ConnectRFIDDRiver();
        }
    }
}

void OBSMainView::onBLEConfiguration()
{

    for (auto raceCtrl : m_raceControllers) {
        raceCtrl->DisconnectRFIDDriver();
    }

    auto itFind = std::find_if(mDetectors.begin(), mDetectors.end(), [](const auto& detector) {
        return dynamic_cast<BLEReader*>(detector.get());
    });

    if (itFind != mDetectors.end()) {
        const auto bleReader = static_cast<BLEReader*>(itFind->get());
        DialogEditBLEConf bleConf(*bleReader, this);
        bleConf.exec();

        if (bleConf.result() == QDialog::Accepted) {
        }

        for (auto raceCtrl : m_raceControllers) {
            raceCtrl->ConnectRFIDDRiver();
        }
    }
}

void OBSMainView::onShowResults() {}
void OBSMainView::onNewRace()
{
    DialogEditRaceConf raceConf(this);
    raceConf.exec();

    if (raceConf.result() == QDialog::Accepted) {
        RaceController* newController = RaceFactory::MakeController(
            raceConf.GetRaceConfiguration(), *mGenericDetector, mServer, this);
        m_ui.m_tabWidget->addTab(newController, newController->GetRaceConfiguration().GetRaceName());
        m_raceControllers.push_back(newController);
    }
}

void OBSMainView::onImportRace()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Files (*.json)"));
    qDebug() << "Open file: " << fileName;

    JsonRaceImporter importer(fileName);
    RaceConfiguration raceConfig(importer.GetRaceConfiguration());
    DialogEditRaceConf raceConf(raceConfig, this);
    raceConf.exec();

    if (raceConf.result() == QDialog::Accepted) {
        RaceController* newController = RaceFactory::MakeController(
            raceConf.GetRaceConfiguration(), *mGenericDetector, mServer, this);
        newController->AddCompetitors(importer.GetCompetitors());

        m_ui.m_tabWidget->addTab(newController, newController->GetRaceConfiguration().GetRaceName());
        m_raceControllers.push_back(newController);
    }
}

void OBSMainView::onCloseTab(int p_tabIndex)
{
    RaceController* raceCtrl = dynamic_cast<RaceController*>(m_ui.m_tabWidget->widget(p_tabIndex));
    m_ui.m_tabWidget->removeTab(p_tabIndex);

    delete raceCtrl;
}

void OBSMainView::onMergeRace()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Files (*.obs)"));
    qDebug() << "Open file: " << fileName;

    RaceController* newController = loadRaceFromFile(fileName);
    RaceController* raceCtrl = dynamic_cast<RaceController*>(m_ui.m_tabWidget->currentWidget());
    bool ok = raceCtrl->Merge(newController);

    if (!ok) {
        QMessageBox::warning(this, tr("Race Merge "), tr("Impossible to merge."), QMessageBox::Ok, QMessageBox::Ok);
    }
}

void OBSMainView::onShortCutRefresh()
{
    QFile File("src/style_white.css");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    qApp->setStyleSheet(StyleSheet);
    qDebug() << "ShortCut done";
}
