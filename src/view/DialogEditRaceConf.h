/*
 * DialogEditRaceConf.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGEDITRACECONF_H_
#define SRC_DIALOGEDITRACECONF_H_

#include "RaceConfiguration.h"
#include "ui_RaceConf.h"
#include <qdialog.h>
class QRadioButton;

class DialogEditRaceConf : public QDialog {
    Q_OBJECT

public:
    DialogEditRaceConf(const RaceConfiguration& p_raceConfig, QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    DialogEditRaceConf(QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);

    virtual ~DialogEditRaceConf();

    virtual void accept();

    RaceConfiguration GetRaceConfiguration();

private:
    void Init();
    void AddEditStationToUI(const QString& p_stationName, const bool& p_checked);

private slots:
    void OnAddEditStationToUI();
    void OnRaceTypeChanged();
    void OnBaseStationChanged(int p_checked);
    void OnSplitBaseStation(int p_checked);

private:
    Ui_RaceConf m_ui;
    RaceConfiguration m_raceConfig;
    std::vector<QLineEdit*> m_lineEditStations;
    std::vector<QCheckBox*> m_radiButtStations;
};

#endif /* SRC_DIALOGEDITRACECONF_H_ */
