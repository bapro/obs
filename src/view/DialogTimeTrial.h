/*
 * DialogTimeTrial.h
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#ifndef SRC_DIALOGTIMETRIAL_H_
#define SRC_DIALOGTIMETRIAL_H_

#include "Runner.h"
#include "ui_TimeTrial.h"
#include <QtCore>
#include <qdialog.h>
#include <qelapsedtimer.h>
#include <qtimer.h>

class DialogTimeTrial : public QDialog {
    Q_OBJECT

public:
    DialogTimeTrial(QWidget* parent = 0);
    virtual ~DialogTimeTrial();

    void SetRunner(const Runner& p_runner);

public slots:
    void OnIntervalRunner(Runner p_runner, int p_interval, int p_currentPos, int p_totalNumber);
    void OnUpdateView();
    void OnDisplayInfos();
    void Stop();

private:
    Ui_TimeTrial m_ui;
    QTimer m_timer;
    QTime m_time;
    int m_interval; // Interval in ms between to runner
    Runner m_runner;
    QString m_countLabel;
};

#endif /* SRC_DIALOGTIMETRIAL_H_ */
