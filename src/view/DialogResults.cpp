/*
 * DialogResults.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include "qradiobutton.h"
#include "qsortfilterproxymodel.h"
#include <DialogResults.h>
#include <QInputDialog>
#include <QVBoxLayout>
#include <RaceConfiguration.h>
#include <qcheckbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qtoolbutton.h>

DialogResults::DialogResults(QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_proxySortModel(new QSortFilterProxyModel(this))
    , m_wdgtColumnHiding(new QWidget(this))
{
    m_ui.setupUi(this);
    setWindowTitle(tr("Results"));
    setWindowFlags(this->windowFlags() | Qt::WindowMaximizeButtonHint);
    m_ui.m_tableResults->setModel(m_proxySortModel);
    m_ui.m_tableResults->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_ui.m_tableResults->setSelectionMode(QAbstractItemView::SingleSelection);
    m_ui.m_tableResults->setSortingEnabled(true);
    m_ui.m_tableResults->verticalHeader()->hide();
    m_ui.m_tableResults->horizontalHeader()->setSectionsClickable(true);
    m_ui.m_tableResults->horizontalHeader()->setSectionsMovable(true);
    m_ui.m_tableResults->setAlternatingRowColors(true);
}

DialogResults::~DialogResults() {}

void DialogResults::SetModel(QAbstractItemModel* p_model)
{
    m_proxySortModel->setSourceModel(p_model);
    m_ui.m_tableResults->update();
    AskForColumns();
}

void DialogResults::AskForColumns()
{
    QVBoxLayout* vBoxLayout = new QVBoxLayout();

    for (int col = 0; col < m_proxySortModel->sourceModel()->columnCount(); ++col) {
        QString colName = m_proxySortModel->headerData(col, Qt::Horizontal, Qt::DisplayRole).toString();
        QCheckBox* checkBox = new QCheckBox(colName);

        // Absolutely not clean but do the work as intended
        checkBox->setObjectName(QString::number(col));

        checkBox->setChecked(true);
        vBoxLayout->addWidget(checkBox);
        connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(OnColumnStateChange(int)));
    }

    QToolButton* buttHide = new QToolButton();
    buttHide->setText(tr("Hide"));
    vBoxLayout->addWidget(buttHide);
    connect(buttHide, SIGNAL(clicked(bool)), this, SLOT(OnColumnHiding()));

    m_wdgtColumnHiding->setLayout(vBoxLayout);
    this->layout()->addWidget(m_wdgtColumnHiding);
}

void DialogResults::OnColumnStateChange(int state)
{
    QCheckBox* checkBox = dynamic_cast<QCheckBox*>(QObject::sender());

    if (checkBox) {
        int col = checkBox->objectName().toInt();
        m_ui.m_tableResults->setColumnHidden(col, state == Qt::Unchecked);
    }
}

void DialogResults::OnColumnHiding() { m_wdgtColumnHiding->hide(); }
