/*
 * DialogEditRaceConf.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGSAFETYCHECK_H_
#define SRC_DIALOGSAFETYCHECK_H_

#include "ICompetitor.h"
#include "Runner.h"
#include "Team.h"
#include "ui_SafetyCheck.h"
#include <qdialog.h>

class QRadioButton;

class DialogSafetyCheck : public QDialog {
    Q_OBJECT

public:
    DialogSafetyCheck(QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogSafetyCheck();

    void SetCompetitor(ICompetitorPtr p_competitor);
    void SetRunner(Runner& p_runner);
    void SetTeam(Team& p_team);
    void SetRank(const int& p_rank);
    virtual void accept();

private:
    void HideRaceInfos();
    void ShowRaceInfos();

private:
    Ui_SafetyCheck m_ui;
};

#endif /* SRC_DIALOGSAFETYCHECK_H_ */
