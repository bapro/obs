/*
 * DialogEditStartedTime.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include "qradiobutton.h"
#include <DialogEditStartedTime.h>
#include <RaceConfiguration.h>
#include <qlabel.h>
#include <qlayout.h>

DialogEditStartedTime::DialogEditStartedTime(QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
{
    m_ui.setupUi(this);
    setWindowTitle(tr("Change started time"));
    m_ui.m_textLabel->setText(tr("Warning you are going to change the started time !"));
}

DialogEditStartedTime::~DialogEditStartedTime() {}

// void DialogSafetyCheck::accept()
//{
////	QDialog::accept();
//	hide();
//}
