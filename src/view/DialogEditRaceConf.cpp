/*
 * DialogEditRaceConf.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include <BaseStation.h>
#include <DialogEditRaceConf.h>
#include <iterator>
#include <qboxlayout.h>
#include <qcheckbox.h>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qnamespace.h>
#include <qobjectdefs.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qstring.h>
#include <vector>

DialogEditRaceConf::DialogEditRaceConf(const RaceConfiguration& p_raceConfig, QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_raceConfig(p_raceConfig)
{
    Init();
}

DialogEditRaceConf::DialogEditRaceConf(QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_raceConfig()
{
    Init();

    m_ui.m_radioSolo->setChecked(true);
    m_ui.m_checkFinish->setChecked(true);
    m_ui.m_checkStart->setChecked(true);
}

void DialogEditRaceConf::Init()
{
    m_ui.setupUi(this);
    //	m_ui.m_radioBox->hide();
    m_ui.m_editRaceName->setText(m_raceConfig.GetRaceName());
    m_ui.m_editRaceDate->setText(m_raceConfig.GetRaceDate());
    m_ui.m_editRaceID->setText(QString::number(m_raceConfig.GetRaceID()));
    m_ui.m_editInterval->setText(QString::number(m_raceConfig.GetInterval()));
    m_ui.m_editNbRunner->setText(QString::number(m_raceConfig.GetNbRunnerInTeam()));
    m_ui.m_safetyCheck->setChecked(m_raceConfig.IsSafetyCheck());

    m_ui.m_radioSolo->setChecked(m_raceConfig.GetRaceType() == Race::SOLO);
    m_ui.m_radioTeam->setChecked(m_raceConfig.GetRaceType() == Race::TEAM);
    m_ui.m_radioChrono->setChecked(m_raceConfig.GetRaceType() == Race::TIME_TRIAL);

    std::vector<BaseStation> stations = m_raceConfig.GetBaseStations();

    if (stations.size() > 0) {
        std::vector<BaseStation>::iterator itStation;

        for (itStation = ++stations.begin(); std::distance(itStation, stations.end()) > 1; ++itStation) {
            BaseStation base = *itStation;
            AddEditStationToUI(base.GetName(), base.IsCurrent());
        }
        m_ui.m_checkStart->setChecked(stations.front().IsCurrent());
        m_ui.m_checkFinish->setChecked(stations.back().IsCurrent());
    }

    OnRaceTypeChanged();

    connect(m_ui.m_addStation, SIGNAL(clicked(bool)), this, SLOT(OnAddEditStationToUI()));
    connect(m_ui.m_radioSolo, SIGNAL(toggled(bool)), this, SLOT(OnRaceTypeChanged()));
    connect(m_ui.m_radioChrono, SIGNAL(toggled(bool)), this, SLOT(OnRaceTypeChanged()));
    connect(m_ui.m_radioTeam, SIGNAL(toggled(bool)), this, SLOT(OnRaceTypeChanged()));
    connect(m_ui.m_checkFinish, SIGNAL(stateChanged(int)), this, SLOT(OnBaseStationChanged(int)));
    connect(m_ui.m_checkStart, SIGNAL(stateChanged(int)), this, SLOT(OnBaseStationChanged(int)));
}
DialogEditRaceConf::~DialogEditRaceConf() {}

void DialogEditRaceConf::accept()
{
    QDialog::accept();
    m_raceConfig.SetRaceName(m_ui.m_editRaceName->text());
    m_raceConfig.SetRaceDate(m_ui.m_editRaceDate->text());
    m_raceConfig.SetSafetyCheck(m_ui.m_safetyCheck->isChecked());
    m_raceConfig.SetRaceID(m_ui.m_editRaceID->text().toInt());
    m_raceConfig.SetInterval(m_ui.m_editInterval->text().toInt());
    m_raceConfig.SetNbRunnerInTeam(m_ui.m_editNbRunner->text().toInt());

    int baseId = 0;
    m_raceConfig.ClearBaseStations();

    // Add start base station
    BaseStation baseStart(tr("Start Station"), RaceConfiguration::BASE_STATION_START);
    baseStart.SetIsCurrent(m_ui.m_checkStart->isChecked());
    m_raceConfig.AddBaseStation(baseStart);

    // Iterate over all base
    for (auto lineEdit : m_lineEditStations) {
        QString baseName = lineEdit->text();

        if (baseName.isEmpty()) {
            baseName = tr("Unknown");
        }

        bool isChecked = m_radiButtStations.at((unsigned int)baseId)->isChecked();
        // Start base Id to 1 - 0 is for start base station
        BaseStation base(baseName, ++baseId);
        base.SetIsCurrent(isChecked);

        m_raceConfig.AddBaseStation(base);
    }

    // Add Finish Base Station
    BaseStation base(tr("Finish Station"), ++baseId);
    base.SetIsCurrent(m_ui.m_checkFinish->isChecked());
    m_raceConfig.AddBaseStation(base);

    // RaceType
    if (m_ui.m_radioSolo->isChecked()) {
        m_raceConfig.SetRaceType(Race::SOLO);
    } else if (m_ui.m_radioTeam->isChecked()) {
        m_raceConfig.SetRaceType(Race::TEAM);
    } else if (m_ui.m_radioChrono->isChecked()) {
        m_raceConfig.SetRaceType(Race::TIME_TRIAL);
    }
}

void DialogEditRaceConf::AddEditStationToUI(const QString& p_stationName, const bool& p_checked)
{
    QHBoxLayout* localLayout = new QHBoxLayout();
    QString labelTxt = QString("Station %1").arg(QString::number(m_lineEditStations.size() + 1));

    QLabel* labelStation = new QLabel(labelTxt, this);

    QLineEdit* lineEdit = new QLineEdit(this);
    lineEdit->setText(p_stationName);

    QCheckBox* checkBox = new QCheckBox(this);
    checkBox->setChecked(p_checked);

    localLayout->addWidget(labelStation);
    localLayout->addWidget(lineEdit);

    m_ui.m_gridlayoutBox->addWidget(checkBox, (int)m_lineEditStations.size() + 1, 0);
    m_ui.m_gridlayoutBox->addLayout(localLayout, (int)m_lineEditStations.size() + 1, 1);

    m_lineEditStations.push_back(lineEdit);
    m_radiButtStations.push_back(checkBox);

    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(OnSplitBaseStation(int)));
    //	connect(checkBox, SIGNAL(stateChanged(int)),this,SLOT(OnCheckBoxChanged(int)) );
}

void DialogEditRaceConf::OnAddEditStationToUI() { AddEditStationToUI(QString(), false); }

RaceConfiguration DialogEditRaceConf::GetRaceConfiguration() { return m_raceConfig; }

void DialogEditRaceConf::OnRaceTypeChanged()
{
    // RaceType
    if (m_ui.m_radioSolo->isChecked()) {
        m_ui.m_labelInterval->hide();
        m_ui.m_editInterval->hide();

        m_ui.m_labelNbRunner->hide();
        m_ui.m_editNbRunner->hide();
    } else if (m_ui.m_radioTeam->isChecked()) {
        m_ui.m_labelInterval->hide();
        m_ui.m_editInterval->hide();

        m_ui.m_editNbRunner->show();
        m_ui.m_labelNbRunner->show();
    } else if (m_ui.m_radioChrono->isChecked()) {
        m_ui.m_labelNbRunner->hide();
        m_ui.m_editNbRunner->hide();

        m_ui.m_labelInterval->show();
        m_ui.m_editInterval->show();
    }
}

void DialogEditRaceConf::OnBaseStationChanged(int p_checked)
{
    if (p_checked == Qt::Checked) {
        for (QCheckBox* radioButton : m_radiButtStations) {
            radioButton->setChecked(false);
        }
    }
}

void DialogEditRaceConf::OnSplitBaseStation(int p_checked)
{
    if (p_checked == Qt::Checked) {

        for (QCheckBox* radioButton : m_radiButtStations) {
            if (sender() != radioButton) {
                radioButton->setChecked(false);
            }
            m_ui.m_checkFinish->setChecked(false);
            m_ui.m_checkStart->setChecked(false);
        }
    }
}
