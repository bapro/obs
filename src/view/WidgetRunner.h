/*
 * WidgetRunner.h
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_WIDGETRUNNER_H_
#define SRC_WIDGETRUNNER_H_

#include "Runner.h"
#include "ui_RunnerInfos.h"
#include <qwidget.h>

class WidgetRunner : public QWidget {
    Q_OBJECT

public:
    WidgetRunner(QWidget* p_parent = 0);
    virtual ~WidgetRunner();
    /**
     * @brief mandatory
     */
    void SetRunner(const Runner& p_runner);

    Runner GetEditedRunner();

    Ui_RunnerInfos m_ui;

private:
    Runner m_currentRunner;
};

#endif /* SRC_WIDGETRUNNER_H_ */
