/*
 * DialogEditRFIDConf.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGEDITRFIDCONF_H_
#define SRC_DIALOGEDITRFIDCONF_H_

#include "RFIDReader.h"
#include "ui_RFIDConf.h"
#include <qdialog.h>

class DialogEditRFIDConf : public QDialog {
    Q_OBJECT

public:
    DialogEditRFIDConf(RFIDReader& p_RFIDDriver, QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogEditRFIDConf();

    virtual void accept();

public slots:
    void OnConnectReader();
    void OnNewRFIDTag(QString p_rfidTag, QTime time);
    void OnNewDataRead(QString p_rfidTag);
    void OnBeepStateChanged(int p_state);

private:
    Ui_RFIDReaderConf m_ui;
    RFIDReader& m_rfidDriver;
    QList<QSerialPortInfo> m_serialsPortInfo;
};

#endif /* SRC_DIALOGEDITRFIDCONF_H_ */
