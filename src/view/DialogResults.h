/*
 * DialogResults.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGRESULTS_H_
#define SRC_DIALOGRESULTS_H_

#include "ui_Results.h"
#include <qdialog.h>

class QSortFilterProxyModel;

class DialogResults : public QDialog {
    Q_OBJECT

public:
    DialogResults(QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogResults();

    void SetModel(QAbstractItemModel* p_model);

    void AskForColumns();

public slots:
    void OnColumnStateChange(int state);
    void OnColumnHiding();

private:
    Ui_Results m_ui;
    QSortFilterProxyModel* m_proxySortModel;
    QWidget* m_wdgtColumnHiding;
};

#endif /* SRC_DIALOGRESULTS_H_ */
