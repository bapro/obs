/*
 * DialogEditRunner.h
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#ifndef SRC_DIALOGEDITRUNNER_H_
#define SRC_DIALOGEDITRUNNER_H_

#include "RaceConfiguration.h"
#include "RaceModel.h"
#include "Runner.h"
#include "ui_EditRunner.h"
#include <qdialog.h>

class TagUnicityManager;

class DialogEditRunner : public QDialog {
    Q_OBJECT

public:
    DialogEditRunner(RaceModelPtr p_raceModel, QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogEditRunner();
    /**
     * @brief mandatory
     */
    void SetRunner(const Runner& p_runner);

    void SetFastTrackEnabled(const bool& p_fastMode);

    Runner GetEditedRunner();

    void UpdateRunner();

private:
    void GenerateBaseStationTime();
    void GetUpdateBaseStationTime();

public slots:
    void OnNewRFIDTag(QString p_rfidTag);

private slots:
    void OnNext();
    void OnPrevious();
    void OnDateChanged(const QDate& p_date);

signals:
    void nextRunner();

private:
    Ui_EditRunner m_ui;
    Runner m_currentRunner;
    RaceConfiguration m_raceConfig;
    const TagUnicityManager& m_tagManager;
    RaceModelPtr m_raceModel;
    bool m_fastTrack = false;
};

#endif /* SRC_DIALOGEDITRUNNER_H_ */
