/*
 * WidgetRunner.cpp
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#include "RaceConfiguration.h"
#include "TagUnicityManager.h"
#include "qdatetimeedit.h"
#include "qdebug.h"
#include <WidgetRunner.h>

WidgetRunner::WidgetRunner(QWidget* p_parent)
    : QWidget(p_parent)
    , m_ui()
    , m_currentRunner()
{
    m_ui.setupUi(this);
    m_ui.m_comboSexe->insertItem(Runner::MALE, QString("M"));
    m_ui.m_comboSexe->insertItem(Runner::FEMALE, QString("F"));

    m_ui.m_editCategory->setEnabled(false);

    // NOT USED YET
    m_ui.m_editZIP->hide();
    m_ui.m_labelCityCode->hide();
}

WidgetRunner::~WidgetRunner() {}

void WidgetRunner::SetRunner(const Runner& p_runner)
{
    m_currentRunner = p_runner;
    m_ui.m_editFirstName->setText(m_currentRunner.m_firstName);
    m_ui.m_editLastName->setText(m_currentRunner.m_lastName);
    m_ui.m_editBirth->setDate(m_currentRunner.m_birthDate);
    m_ui.m_editCategory->setText(Category::CategoryToString(m_currentRunner.GetCategory()));
    m_ui.m_editCity->setText(m_currentRunner.m_city);
    m_ui.m_editClub->setText(m_currentRunner.m_club);
    m_ui.m_editEMail->setText(m_currentRunner.m_email);
    m_ui.m_editTel->setText(m_currentRunner.m_phoneNumber);
    m_ui.m_comboSexe->setCurrentIndex(m_currentRunner.GetSexe());
}

Runner WidgetRunner::GetEditedRunner()
{
    m_currentRunner.m_firstName = m_ui.m_editFirstName->text();
    m_currentRunner.m_lastName = m_ui.m_editLastName->text();
    m_currentRunner.m_birthDate = m_ui.m_editBirth->date();
    m_currentRunner.m_city = m_ui.m_editCity->text();
    m_currentRunner.m_club = m_ui.m_editClub->text();
    m_currentRunner.m_email = m_ui.m_editEMail->text();
    m_currentRunner.m_phoneNumber = m_ui.m_editTel->text();

    // special treatment
    m_currentRunner.SetCategory(Category::CategoryFromDate(m_currentRunner.m_birthDate));
    m_currentRunner.SetSexe((Runner::eSexe)m_ui.m_comboSexe->currentIndex());

    return m_currentRunner;
}
