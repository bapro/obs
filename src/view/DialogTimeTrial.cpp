/*
 * DialogTimeTrial.cpp
 *
 *  Created on: 21 nov. 2015
 *      Author: baptiste
 */

#include <DialogTimeTrial.h>
#include <qdebug.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include <qmessagebox.h>
#include <qthread.h>
#include <thread>

DialogTimeTrial::DialogTimeTrial(QWidget* p_parent)
    : QDialog(p_parent)
    , m_ui()
    , m_timer(this)
    , m_interval()
    , m_runner()
    , m_countLabel()
{
    m_ui.setupUi(this);
    setWindowTitle(tr("Time Trial"));
    setWindowFlags(this->windowFlags() | Qt::WindowMaximizeButtonHint);

    m_ui.m_labelNameTT->setText("-");
    m_ui.m_labelNumberTT->setText("-");
    m_ui.m_labelChronoTT->setEnabled(true);
    m_ui.m_count->setText("-/-");

    connect(&m_timer, SIGNAL(timeout()), this, SLOT(OnUpdateView()));
}

DialogTimeTrial::~DialogTimeTrial() {}

void DialogTimeTrial::SetRunner(const Runner& p_runner)
{
    m_runner = p_runner;
    OnDisplayInfos();
}

void DialogTimeTrial::OnIntervalRunner(Runner p_runner, int p_interval, int p_currentPos, int p_totalNumber)
{
    m_time.restart();
    m_interval = p_interval * 1000;

    bool isValid = m_timer.isActive();
    m_runner = p_runner;

    m_countLabel = QString::number(p_currentPos) + "/" + QString::number(p_totalNumber);

    if (!isValid) {
        m_timer.start(10);
        OnDisplayInfos();
    } else {
        QTimer::singleShot(1000, this, &DialogTimeTrial::OnDisplayInfos);
        m_ui.m_labelChronoTT->setText(QString("0:00"));
        disconnect(&m_timer, SIGNAL(timeout()), this, SLOT(OnUpdateView()));
    }
}

void DialogTimeTrial::OnDisplayInfos()
{
    m_ui.m_labelNumberTT->setText(QString::number(m_runner.GetBibNumber()));
    m_ui.m_labelNameTT->setText(m_runner.m_lastName + " " + m_runner.m_firstName);
    m_ui.m_count->setText(m_countLabel);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(OnUpdateView()));
}

void DialogTimeTrial::OnUpdateView()
{
    m_ui.m_labelChronoTT->setEnabled(true);

    int leftTime = m_interval - m_time.elapsed();
    if (leftTime < 0) {
        leftTime = 0;
    }
    int scdLeft = leftTime / 1000;
    leftTime = (leftTime - scdLeft * 1000) / 100;
    QString number = QString("%1:%2%3").arg(QString::number(scdLeft)).arg(QString::number(leftTime)).arg(QString("0"));
    m_ui.m_labelChronoTT->setText(number);

    // Enable/disable labelChrono -> skinning
    if (scdLeft <= 3 && scdLeft % 2 == 1) {
        m_ui.m_labelChronoTT->setEnabled(false);
    }
}

void DialogTimeTrial::Stop()
{
    qDebug() << "Timer Stopped";
    m_timer.stop();
    m_ui.m_labelNameTT->setText("-");
    m_ui.m_labelNumberTT->setText("-");
    m_ui.m_count->setText("-/-");
    //	QMessageBox::information(this, tr("End"), "Completed", QMessageBox::Ok, QMessageBox::NoButton);
}
