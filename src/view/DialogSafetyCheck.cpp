/*
 * DialogSafetyCheck.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include "qradiobutton.h"
#include <DialogSafetyCheck.h>
#include <RaceConfiguration.h>
#include <qlabel.h>
#include <qlayout.h>

DialogSafetyCheck::DialogSafetyCheck(QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
{
    m_ui.setupUi(this);
    HideRaceInfos();
}

DialogSafetyCheck::~DialogSafetyCheck() {}
void DialogSafetyCheck::SetCompetitor(ICompetitorPtr p_competitor)
{
    RunnerPtr runner = std::dynamic_pointer_cast<Runner>(p_competitor);
    TeamPtr team = std::dynamic_pointer_cast<Team>(p_competitor);

    if (runner) {
        SetRunner(*runner);
    } else if (team) {
        SetTeam(*team);
    }
}

void DialogSafetyCheck::SetRunner(Runner& p_runner)
{
    show();
    m_ui.stackedWidget->setCurrentWidget(m_ui.m_pageRunner);
    m_ui.m_firstName->setText(p_runner.m_firstName);
    m_ui.m_lastName->setText(p_runner.m_lastName);
    m_ui.m_bibNumber->setText(QString::number(p_runner.GetBibNumber()));
    m_ui.m_time->setText(p_runner.GetRaceTime().toString());
    HideRaceInfos();
    m_ui.m_typeLabel->setText("Solo");
    m_ui.m_typeLabel->setStyleSheet("QLabel {background-color : #11C8ED; font-size: 30px; color: black;}");
}

void DialogSafetyCheck::SetTeam(Team& p_team)
{
    show();
    HideRaceInfos();
    m_ui.stackedWidget->setCurrentWidget(m_ui.m_pageTeam);
    m_ui.m_teamName->setText(p_team.GetTeamName());
    m_ui.m_bibNumberTeam->setText(QString::number(p_team.GetBibNumber()));
    m_ui.m_time->setText(p_team.GetRaceTime().toString());
    m_ui.m_typeLabel->setText("Team");
    m_ui.m_typeLabel->setStyleSheet("QLabel {background-color : #5AED11; font-size: 30px; color: black;}");
}

void DialogSafetyCheck::SetRank(const int& p_rank)
{
    ShowRaceInfos();
    m_ui.m_Rank->setText(QString::number(p_rank));
}

void DialogSafetyCheck::accept()
{
    //	QDialog::accept();
    hide();
}

void DialogSafetyCheck::HideRaceInfos()
{
    m_ui.m_labelTime->hide();
    m_ui.m_time->hide();
    m_ui.m_Rank->hide();
    m_ui.m_labelRank->hide();
}

void DialogSafetyCheck::ShowRaceInfos()
{
    m_ui.m_labelTime->show();
    m_ui.m_time->show();
    m_ui.m_Rank->show();
    m_ui.m_labelRank->show();
}
