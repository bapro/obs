/*
 * DialogEditTeam.cpp
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#include "TagUnicityManager.h"
#include "qdatetimeedit.h"
#include "qdebug.h"
#include <DialogEditTeam.h>

DialogEditTeam::DialogEditTeam(RaceModelPtr p_raceModel, QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_currentTeam()
    , m_raceModel(p_raceModel)
    , m_raceConfig(p_raceModel->GetRaceConfiguration())
    , m_tagManager(p_raceModel->GetTagUnicityManager())
{
    m_ui.setupUi(this);

    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::NOT_CHECKED, CompetitorState::StateToString(CompetitorState::NOT_CHECKED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::CHECKED, CompetitorState::StateToString(CompetitorState::CHECKED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::RUNNING, CompetitorState::StateToString(CompetitorState::RUNNING));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::DISQUALIFIED, CompetitorState::StateToString(CompetitorState::DISQUALIFIED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::ABORTED, CompetitorState::StateToString(CompetitorState::ABORTED));

    connect(m_ui.m_pushDown, SIGNAL(clicked(bool)), this, SLOT(OnMoveDown()));
    connect(m_ui.m_pushUp, SIGNAL(clicked(bool)), this, SLOT(OnMoveUp()));
}

DialogEditTeam::~DialogEditTeam() {}

void DialogEditTeam::SetTeam(const Team& p_team)
{
    m_currentTeam = p_team;
    setWindowTitle(windowTitle() + QString(" [%1]").arg(m_currentTeam.GetDatabaseId()));
    m_ui.m_editTeamName->setText(m_currentTeam.GetTeamName());
    m_ui.m_editRaceNm->setText(QString::number(m_currentTeam.GetBibNumber()));
    m_ui.m_editChipNm->setText(m_currentTeam.GetChipNumber());
    m_ui.m_comboRunnerState->setCurrentIndex(m_currentTeam.GetState());

    GenerateRelayViews();
    GenerateBaseStationTime();
}

Team DialogEditTeam::GetEditedTeam()
{
    m_currentTeam.SetTeamName(m_ui.m_editTeamName->text());
    m_currentTeam.SetBibNumber(m_ui.m_editRaceNm->text().isEmpty() ? -1 : m_ui.m_editRaceNm->text().toInt());
    m_currentTeam.SetChipNumber(m_ui.m_editChipNm->text());

    // special treatment
    m_currentTeam.SetState((CompetitorState::eState)m_ui.m_comboRunnerState->currentIndex());

    GetRunnerFromRelayViews();
    GetUpdateBaseStationTime();

    m_currentTeam.ComputeSexeAndCategory();

    return m_currentTeam;
}

void DialogEditTeam::OnNewRFIDTag(QString p_rfidTag)
{
    if (!m_tagManager.IsAlreadyUsed(p_rfidTag) || m_currentTeam.GetChipNumber() == p_rfidTag) {
        m_ui.m_editChipNm->setText(p_rfidTag);
    } else {
        m_ui.m_editChipNm->setText("ID Tag Already used");
    }
}

void DialogEditTeam::OnMoveDown()
{
    int nbItems = m_ui.m_listRelay->count();
    int row = m_ui.m_listRelay->currentRow();
    if (row < (nbItems - 1)) {
        QListWidgetItem* item = m_ui.m_listRelay->takeItem(row);

        m_ui.m_listRelay->insertItem(row + 1, item);
        m_ui.m_listRelay->setCurrentRow(row + 1);
    }
}

void DialogEditTeam::OnMoveUp()
{
    int row = m_ui.m_listRelay->currentRow();

    if (row > 0) {
        QListWidgetItem* item = m_ui.m_listRelay->takeItem(row);

        m_ui.m_listRelay->insertItem(row - 1, item);
        m_ui.m_listRelay->setCurrentRow(row - 1);
    }
}

void DialogEditTeam::GenerateRelayViews()
{
    int nbRunr = 1;
    for (auto member : m_currentTeam.GetTeamMembers()) {
        WidgetRunner* wdtRunr = new WidgetRunner(this);
        wdtRunr->SetRunner(member);
        QString relay = QString("Relay_") + QString::number(nbRunr);
        m_ui.teamTab->addTab(wdtRunr, relay);

        QListWidgetItem* item = new QListWidgetItem(
            QString::number(nbRunr) + " - " + member.m_lastName + QString(" ") + member.m_firstName);
        item->setData(Qt::UserRole, nbRunr);
        m_ui.m_listRelay->addItem(item);

        nbRunr++;
    }
}

void DialogEditTeam::GetRunnerFromRelayViews()
{
    m_currentTeam.RemoveAllTeamMembers();

    for (int indexList = 0; indexList < m_ui.m_listRelay->count(); ++indexList) {
        QListWidgetItem* item = m_ui.m_listRelay->item(indexList);
        int nbRnr = item->data(Qt::UserRole).toInt();

        WidgetRunner* wdtRunr = dynamic_cast<WidgetRunner*>(m_ui.teamTab->widget(2 + (nbRnr - 1)));

        if (wdtRunr) {
            Runner rnr = wdtRunr->GetEditedRunner();
            m_currentTeam.AddTeamMember(rnr);
        }
    }
}

void DialogEditTeam::GenerateBaseStationTime()
{
    int index = 0;

    // Iterate over all Base Station
    for (auto base : m_raceConfig.GetBaseStations()) {
        QLabel* labl = new QLabel(base.GetName(), this);
        m_ui.m_layoutGrid->addWidget(labl, ++index, 0);

        QDateTimeEdit* timeEdit = new QDateTimeEdit(
            m_currentTeam.GetSplitTimeFromBaseStation(base.GetId()).GetTime(), this);
        timeEdit->setDisplayFormat("hh:mm::ss");
        m_ui.m_layoutGrid->addWidget(timeEdit, index, 1);
    }
}

void DialogEditTeam::GetUpdateBaseStationTime()
{
    int index = 0;

    for (auto base : m_raceConfig.GetBaseStations()) {
        if (m_ui.m_layoutGrid->itemAtPosition(++index, 1)) {
            QDateTimeEdit* timeEdit = dynamic_cast<QDateTimeEdit*>(
                m_ui.m_layoutGrid->itemAtPosition(index, 1)->widget());
            if (timeEdit) {
                if (timeEdit->time() != QTime(0, 0, 0, 0)) {
                    qDebug() << " Set time " << timeEdit->time().toString() << "   to base: " << base.GetId();
                    SplitTime splt(base.GetId(), m_currentTeam.GetChipNumber());
                    splt.SetTime(timeEdit->time());
                    m_currentTeam.UpdateSplitTime(splt);
                    m_raceModel->ComputeRanking();
                }
            }
        }
    }
}
