/*
 * DialogEditRFIDConf.cpp
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#include "DialogEditRFIDConf.h"
#include "qdebug.h"
#include "qserialportinfo.h"
#include "qstringlist.h"

DialogEditRFIDConf::DialogEditRFIDConf(RFIDReader& p_RFIDDriver, QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_rfidDriver(p_RFIDDriver)
    , m_serialsPortInfo()
{
    m_ui.setupUi(this);
    m_ui.m_editTest->setVisible(false);
    m_ui.m_labelIsConnected->setVisible(false);

    if (p_RFIDDriver.IsConnected()) {
        m_ui.m_editTest->setVisible(true);
        m_ui.m_labelIsConnected->setVisible(true);

        QString portConnected(m_ui.m_labelIsConnected->text() + ": " + p_RFIDDriver.GetSerialPortName());
        m_ui.m_labelIsConnected->setText(portConnected);
    }

    m_ui.m_checkBeep->setChecked(p_RFIDDriver.IsBeepOnRead());
    m_serialsPortInfo = p_RFIDDriver.GetSerialAvailabe();
    int index = 0;
    for (auto info : m_serialsPortInfo) {
        m_ui.m_comboReader->addItem(info.description(), QVariant::fromValue(index++));
    }
    connect(m_ui.m_connectReader, &QPushButton::clicked, this, &DialogEditRFIDConf::OnConnectReader);
    connect(m_ui.m_checkBeep, &QCheckBox::stateChanged, this, &DialogEditRFIDConf::OnBeepStateChanged);
    connect(m_ui.m_clearBuffer, &QPushButton::clicked, &p_RFIDDriver, &RFIDReader::ClearBuffer);
    connect(&p_RFIDDriver, &RFIDReader::NewTag, this, &DialogEditRFIDConf::OnNewRFIDTag);
    connect(&p_RFIDDriver, &RFIDReader::NewDataRead, this, &DialogEditRFIDConf::OnNewDataRead);
}

DialogEditRFIDConf::~DialogEditRFIDConf() {}

void DialogEditRFIDConf::accept() { QDialog::accept(); }

void DialogEditRFIDConf::OnConnectReader()
{
    int indexSerial = m_ui.m_comboReader->currentData().toInt();

    if (QSerialPort::NoError == m_rfidDriver.SetSerialPort(m_serialsPortInfo.at(indexSerial))) {
        m_ui.m_editTest->setVisible(true);
        m_ui.m_labelIsConnected->setVisible(true);
    } else {
        m_ui.m_editTest->setVisible(false);
        m_ui.m_labelIsConnected->setVisible(false);
    }
}

void DialogEditRFIDConf::OnNewRFIDTag(QString p_rfidTag, QTime time)
{
    m_ui.m_editTest->clear();
    m_ui.m_editTest->setText(time.toString() + " - " + p_rfidTag);
}

void DialogEditRFIDConf::OnNewDataRead(QString p_rfidTag)
{
    QString content = m_ui.m_debugConsole->toPlainText();
    m_ui.m_debugConsole->clear();
    p_rfidTag.replace("\n", ".");
    content += "\n>" + p_rfidTag;
    m_ui.m_debugConsole->setText(content);
}

void DialogEditRFIDConf::OnBeepStateChanged(int p_state) { m_rfidDriver.SetBeepOnRead(p_state == Qt::Checked); }
