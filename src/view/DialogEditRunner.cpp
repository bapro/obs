/*
 * DialogEditRunner.cpp
 *
 *  Created on: 15 févr. 2016
 *      Author: baptiste
 */

#include "DialogEditRunner.h"
#include "TagUnicityManager.h"
#include "qdatetimeedit.h"
#include "qdebug.h"
#include "qshortcut.h"
#include <DialogEditRunner.h>

DialogEditRunner::DialogEditRunner(RaceModelPtr p_raceModel, QWidget* p_parent, Qt::WindowFlags p_flag)
    : QDialog(p_parent, p_flag)
    , m_ui()
    , m_currentRunner()
    , m_raceConfig(p_raceModel->GetRaceConfiguration())
    , m_tagManager(p_raceModel->GetTagUnicityManager())
    , m_raceModel(p_raceModel)
{
    m_ui.setupUi(this);
    m_ui.m_comboSexe->insertItem(Runner::MALE, QString("M"));
    m_ui.m_comboSexe->insertItem(Runner::FEMALE, QString("F"));

    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::NOT_CHECKED, CompetitorState::StateToString(CompetitorState::NOT_CHECKED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::CHECKED, CompetitorState::StateToString(CompetitorState::CHECKED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::RUNNING, CompetitorState::StateToString(CompetitorState::RUNNING));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::DISQUALIFIED, CompetitorState::StateToString(CompetitorState::DISQUALIFIED));
    m_ui.m_comboRunnerState->insertItem(
        CompetitorState::ABORTED, CompetitorState::StateToString(CompetitorState::ABORTED));

    m_ui.m_editCategory->setEnabled(false);

    connect(m_ui.m_nextRunner, &QPushButton::clicked, this, &DialogEditRunner::OnNext);
    connect(m_ui.m_previousRunner, &QPushButton::clicked, this, &DialogEditRunner::OnPrevious);

    // dateChanged(const QDate &date)
    connect(m_ui.m_editBirth, &QDateEdit::dateChanged, this, &DialogEditRunner::OnDateChanged);
    connect(this, &DialogEditRunner::nextRunner, this, &DialogEditRunner::OnNext);

    m_ui.m_previousRunner->hide();

    QShortcut* shortcut = new QShortcut(QKeySequence(tr("Ctrl+return")), this);
    connect(shortcut, &QShortcut::activated, this, &DialogEditRunner::OnNext);
}

DialogEditRunner::~DialogEditRunner() {}

void DialogEditRunner::SetRunner(const Runner& p_runner)
{
    m_currentRunner = p_runner;
    setWindowTitle(windowTitle() + QString(" [%1]").arg(m_currentRunner.GetDatabaseId()));
    m_ui.m_editFirstName->setText(m_currentRunner.m_firstName);
    m_ui.m_editLastName->setText(m_currentRunner.m_lastName);
    m_ui.m_editBirth->setDate(m_currentRunner.m_birthDate);
    m_ui.m_editCategory->setText(Category::CategoryToString(m_currentRunner.GetCategory()));
    m_ui.m_editChipNm->setText(m_currentRunner.GetChipNumber());
    m_ui.m_editCity->setText(m_currentRunner.m_city);
    m_ui.m_editClub->setText(m_currentRunner.m_club);
    m_ui.m_editLicense->setText(m_currentRunner.m_license);
    m_ui.m_editEMail->setText(m_currentRunner.m_email);
    m_ui.m_editRaceNm->setText(QString::number(m_currentRunner.GetBibNumber()));
    m_ui.m_editTel->setText(m_currentRunner.m_phoneNumber);
    m_ui.m_comboSexe->setCurrentIndex(m_currentRunner.GetSexe());
    m_ui.m_comboRunnerState->setCurrentIndex(m_currentRunner.GetState());

    GenerateBaseStationTime();

    // Enable / Disable Next
    if (!m_raceModel->GetNextItem(p_runner.GetDatabaseId())) {
        m_ui.m_nextRunner->setEnabled(false);
    }
}

Runner DialogEditRunner::GetEditedRunner()
{
    m_currentRunner.m_firstName = m_ui.m_editFirstName->text();
    m_currentRunner.m_lastName = m_ui.m_editLastName->text();
    m_currentRunner.m_birthDate = m_ui.m_editBirth->date();
    m_currentRunner.m_city = m_ui.m_editCity->text();
    m_currentRunner.m_club = m_ui.m_editClub->text();
    m_currentRunner.m_license = m_ui.m_editLicense->text();
    m_currentRunner.m_email = m_ui.m_editEMail->text();
    m_currentRunner.m_phoneNumber = m_ui.m_editTel->text();

    // special treatment
    m_currentRunner.SetChipNumber(m_ui.m_editChipNm->text());
    m_currentRunner.SetBibNumber(m_ui.m_editRaceNm->text().isEmpty() ? -1 : m_ui.m_editRaceNm->text().toInt());
    m_currentRunner.SetCategory(Category::CategoryFromDate(m_currentRunner.m_birthDate));
    m_currentRunner.SetSexe((Runner::eSexe)m_ui.m_comboSexe->currentIndex());
    m_currentRunner.SetState((CompetitorState::eState)m_ui.m_comboRunnerState->currentIndex());

    GetUpdateBaseStationTime();

    return m_currentRunner;
}
void DialogEditRunner::UpdateRunner()
{
    Runner editedRunner(GetEditedRunner());
    RunnerPtr rnr = std::dynamic_pointer_cast<Runner>(m_raceModel->GetItem(editedRunner.GetDatabaseId()));

    if (rnr) {
        *rnr = editedRunner;
        m_raceModel->UpdateItem(rnr);
    }
}
void DialogEditRunner::OnNewRFIDTag(QString p_rfidTag)
{
    if (!m_tagManager.IsAlreadyUsed(p_rfidTag) || m_currentRunner.GetChipNumber() == p_rfidTag) {
        m_ui.m_editChipNm->setText(p_rfidTag);
        if (m_fastTrack) {
            emit nextRunner();
        }
    } else {
        m_ui.m_editChipNm->setText("ID Tag Already used");
    }
}

void DialogEditRunner::GenerateBaseStationTime()
{
    int index = 0;

    // Iterate over all Base Station
    for (auto base : m_raceConfig.GetBaseStations()) {
        QLabel* labl = new QLabel(base.GetName(), this);
        m_ui.m_layoutGrid->addWidget(labl, ++index, 0);

        QDateTimeEdit* timeEdit = new QDateTimeEdit(
            m_currentRunner.GetSplitTimeFromBaseStation(base.GetId()).GetTime(), this);
        timeEdit->setDisplayFormat("hh:mm::ss");
        m_ui.m_layoutGrid->addWidget(timeEdit, index, 1);
    }
}

void DialogEditRunner::GetUpdateBaseStationTime()
{
    int index = 0;

    for (auto base : m_raceConfig.GetBaseStations()) {
        if (m_ui.m_layoutGrid->itemAtPosition(++index, 1)) {
            QDateTimeEdit* timeEdit = dynamic_cast<QDateTimeEdit*>(
                m_ui.m_layoutGrid->itemAtPosition(index, 1)->widget());
            if (timeEdit) {
                if (timeEdit->time() != QTime(0, 0, 0, 0)) {
                    qDebug() << " Set time " << timeEdit->time().toString() << "   to base: " << base.GetId();
                    SplitTime splt(base.GetId(), m_currentRunner.GetChipNumber());
                    splt.SetTime(timeEdit->time());
                    m_currentRunner.UpdateSplitTime(splt);
                    m_raceModel->ComputeRanking();
                }
            }
        }
    }
}

void DialogEditRunner::OnNext()
{
    UpdateRunner();
    RunnerPtr nextRunner = std::dynamic_pointer_cast<Runner>(m_raceModel->GetNextItem(m_currentRunner.GetDatabaseId()));
    //	qDebug()<<"Current: "<<m_currentRunner.GetDatabaseId() << "Next: "<<nextRunner->GetDatabaseId();
    if (nextRunner) {
        SetRunner(*nextRunner);
        if (!m_raceModel->GetNextItem(nextRunner->GetDatabaseId())) {
            m_ui.m_nextRunner->setEnabled(false);
        }
    }
}

void DialogEditRunner::OnPrevious() {}

void DialogEditRunner::OnDateChanged(const QDate& p_date)
{
    m_ui.m_editCategory->setText(Category::CategoryToString(Category::CategoryFromDate(p_date)));
}
void DialogEditRunner::SetFastTrackEnabled(const bool& p_fastMode) { m_fastTrack = p_fastMode; }
