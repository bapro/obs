/*
 * DialogEditBLEConf.h
 *
 *  Created on: 7 mars 2016
 *      Author: baptiste
 */

#pragma once

#include "RFIDReader.h"
#include "src/bluetooth/BLEDiscovery.h"
#include "ui_BLEConf.h"
#include <qdialog.h>
#include <src/bluetooth/BLEDiscovery.h>
#include <src/driver/BLEReader.h>

class DialogEditBLEConf : public QDialog {
    Q_OBJECT

public:
    DialogEditBLEConf(BLEReader &reader, QWidget* p_parent = 0, Qt::WindowFlags p_flag = 0);
    virtual ~DialogEditBLEConf();

    virtual void accept() override;

public slots:
    void OnDeviceDiscoveryFinished();
    void OnConnect();
    void OnConnected();
    void OnDisconnected();
    void OnDisconnect();
    void OnRefresh();
    void OnIndexChanged(int index);
    void OnNotification(QString, QTime);

//    void OnNewRFIDTag(QString p_rfidTag);
//    void OnNewDataRead(QString p_rfidTag);
//    void OnBeepStateChanged(int p_state);

private:
    void enableConnect();
    void enableDisconnect();

    Ui_BLEReaderConf m_ui;
    BLEDiscovery mBLEDiscoFever;
    BLEReader& mReader;
};

