/*
 * RFIDReader.cpp
 *
 *  Created on: 28 févr. 2016
 *      Author: baptiste
 */

#include "QtSerialPort/qserialportinfo.h"
#include "qapplication.h"
#include "qdebug.h"
#include <RFIDReader.h>
#include <thread>

#ifdef _WIN32
#include <windows.h>
#else
#include "QtMultimedia/qsound.h"
#endif

const QString PARALLAX_VENDOR_ID = "1027";
const QString PARALLAX_PRODUCT_ID = "24577";

RFIDReader::RFIDReader()
    : m_serialPort()
    , m_buffer()
    , m_beepOnRead(true)
    , m_latestTag()
{
    connect(this, &RFIDReader::NewTag, this, &RFIDReader::OnNewTag);
    for (auto info : QSerialPortInfo::availablePorts()) {
        if (info.vendorIdentifier() == PARALLAX_VENDOR_ID.toInt()
            && info.productIdentifier() == PARALLAX_PRODUCT_ID.toInt()) {
            InitSerialPort(info);
            break;
        }
    }
}

RFIDReader::~RFIDReader() {}

void RFIDReader::ClearBuffer() { m_buffer.clear(); }

void RFIDReader::SimulateRfidTag()
{
    QString newTag = "000111";
    emit NewTag(newTag, QTime::currentTime());
}

QSerialPort::SerialPortError RFIDReader::SetSerialPort(const QSerialPortInfo& p_serialInfo)
{
    return InitSerialPort(p_serialInfo);
}

QList<QSerialPortInfo> RFIDReader::GetSerialAvailabe() { return QSerialPortInfo::availablePorts(); }

void RFIDReader::OnNewData()
{
    QByteArray data = m_serialPort->readAll();
    QString strData(data);

    m_buffer.append(strData);
    if (m_buffer.contains("\n") && m_buffer.size() > 8) {
        //        qDebug() << "Before: [" << m_buffer << "]";
        QStringList strList = m_buffer.split("\n", QString::SkipEmptyParts);
        m_buffer.clear();
        for (auto str : strList) {
            if (str.endsWith("\r")) {
                str.remove("\r");
                str.remove("\n");
                str.remove(0, 2);

                bool status = false;
                QString val(QString::number(str.toUInt(&status, 16)));

                if (status) {
                    str = val;
                }
                emit NewTag(str, QTime::currentTime());
                qDebug() << "RFID: " << str;
            } else {
                m_buffer.append("\n").append(str);
            }
        }
        //        qDebug() << "After: [" << m_buffer << "]";
    }
    emit NewDataRead(m_buffer);
}

void RFIDReader::OnNewTag(QString p_strTag)
{
    if (m_beepOnRead) {
        if (m_latestTag != p_strTag || m_timerBeep.elapsed() > 1000) {
            m_latestTag = p_strTag;
            m_timerBeep.restart();

#ifdef _WIN32
            PlaySound(TEXT("audio\\beep.wav"), NULL, SND_FILENAME | SND_ASYNC);
#else
            QSound::play(":/qrc_audio/rc/audio/beep.wav");
#endif
        }
    }
}
bool RFIDReader::IsConnected()
{
    bool ret = false;

    if (m_serialPort) {
        ret = m_serialPort->isOpen();
        ret &= m_serialPort->error() == QSerialPort::NoError;
    }
    return ret;
}
QSerialPort::SerialPortError RFIDReader::InitSerialPort(const QSerialPortInfo& p_serialInfo)
{
    if (IsConnected()) {
        m_serialPort->flush();
        m_serialPort->close();
    }

    m_serialPort = new QSerialPort(p_serialInfo, this);
    m_serialPort->setBaudRate(QSerialPort::Baud2400, QSerialPort::Input);
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->open(QIODevice::ReadOnly);

    connect(m_serialPort, &QSerialPort::readyRead, this, &RFIDReader::OnNewData);
    qDebug() << "Connected : " << m_serialPort->portName();
    qDebug() << "Is Connected: " << IsConnected();

    if (IsConnected()) {
        m_timerBeep.start();
#ifdef _WIN32
        PlaySound(TEXT("audio\\beep.wav"), NULL, SND_FILENAME | SND_ASYNC);
#else
        QSound::play(":/qrc_audio/rc/audio/beep.wav");
#endif
    }
    return m_serialPort->error();
}

void RFIDReader::SetBeepOnRead(const bool& p_beep) { m_beepOnRead = p_beep; }

QString RFIDReader::GetSerialPortName() const
{
    QString portName;
    if (m_serialPort) {
        portName = m_serialPort->portName();
    }
    return portName;
}
