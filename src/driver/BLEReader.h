// (C) king.com Ltd 2020

#pragma once

#include "IDetector.h"
#include <QObject>
#include <bluetooth/BLEDevice.h>

class BLEReader : public IDetector {
    Q_OBJECT
public:
    BLEReader() = default;
    ~BLEReader() = default;

    void connect(const QBluetoothDeviceInfo& device);
    void disconnect(const QBluetoothDeviceInfo& device);
    bool isConnected(const QBluetoothDeviceInfo& device);
    void disconnect();

public slots:
    void OnServicesDiscoveryFinished(BLEDevice* device);
    void OnCharacteristicsDiscoveryFinished(BLEDevice* device);
    void OnNotification(QLowEnergyCharacteristic, QByteArray);

Q_SIGNALS:
    void updateChanged(QString);
    void deviceConnected();
    void deviceDisconnected();

private:
    std::list<BLEDevice> mDevices;
};
