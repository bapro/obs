// (C) king.com Ltd 2019

#include "DetectorFactory.h"
#include "BLEReader.h"
#include "RFIDReader.h"
#include "Settings.h"

namespace{
const QString RFID_125("RFID_125KHZ");
const QString RFID_SIMU("SIMULATOR");
}

std::unique_ptr<IDetector> DetectorFactory::buildFromSettings()
{
    const auto detectorType = Settings::instance()->value<QString>(SETTINGS_DETECTOR_TYPE, RFID_125);
    if(detectorType == RFID_125){
        return std::make_unique<RFIDReader>();
    }
    else if(detectorType == RFID_SIMU){
        //TODO
        return nullptr;
    }

    return nullptr;
}

std::unique_ptr<IDetector> DetectorFactory::buildBluetooth()
{
    return std::make_unique<BLEReader>();
}
