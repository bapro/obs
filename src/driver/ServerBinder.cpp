/*
 * ServerBinder.cpp
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#include "ServerBinder.h"
#include "ICompetitor.h"
#include "RaceConfiguration.h"
#include <qjsonarray.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qnetworkrequest.h>
#include <qurlquery.h>
#include <src/data/Settings.h>

namespace {

const QString HASH_PARAM("hash");
const QString UPDATE_TIME_PARAM = "updateTime";
const QString SYNC_COMPETITORS_PARAM = "syncCompetitors";
const QString CLEAR_TIME_PARAM = "clearTime";

const QString PING_PARAM = "pingMe";
const QString PING_REPLY = "pingBack";
const QString TIME_PARAM = "time";

const QString JSON_REQUEST_TYPE = "request";
const QString JSON_STATUS = "status";
const QString JSON_TEAM_NAME = "teamName";
const QString JSON_ID = "id";
const QString JSON_RACE_ID = "raceId";
const QString JSON_RACE_TIMES = "times";
const QString JSON_COMPETITORS = "competitors";
const QString JSON_BIBNUMBER = "bibNumber";
const QString JSON_FIRST_NAME = "firstName";
const QString JSON_LAST_NAME = "lastName";
const QString JSON_TEL = "tel";
const QString JSON_EMAIL = "email";
const QString JSON_SEXE = "gender";
const QString JSON_BIRTH_DATE = "birthDate";
const QString JSON_CITY = "city";
const QString JSON_CLUB = "club";
const QString JSON_LICENCE = "licence";
const QString JSON_TIME = "time";
const QString JSON_BASE_STATION = "baseID";
const QString JSON_STATE = "state";
const QString JSON_STATE_COUNTER = "stateCounter";
const QString JSON_TIME_BASE_STATION = "split_";

}

ServerBinder::ServerBinder(const QString& server, const QString& hash)
    : QObject()
    , mNetworkManager()
    , mNetworkRequest()
    , mHashKey(hash)
{
    connect(&mNetworkManager, &QNetworkAccessManager::finished, this, &ServerBinder::onReplyFinished);
    connect(&mNetworkManager, &QNetworkAccessManager::sslErrors, this, &ServerBinder::onSSLError);

    mNetworkRequest.setUrl(QUrl(server));
    mNetworkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
}

ServerBinder::~ServerBinder() {}

void ServerBinder::clearLiveTiming(const int& p_raceId)
{
    QUrlQuery postData;
    postData.addQueryItem(CLEAR_TIME_PARAM, QString::number(p_raceId));

    doRequest(postData);
}

void ServerBinder::sendTime(const std::vector<ICompetitorPtr>& p_timeList, const RaceConfiguration& p_conf)
{
    QJsonObject rootDoc;
    rootDoc[JSON_RACE_ID] = p_conf.GetRaceID();

    QJsonArray arrayJson;

    for (ICompetitorPtr runr : p_timeList) {
        // If main station then force add started time
        if (p_conf.IsCurrentMainStation()) {
            QJsonObject objStart = createJsonForIMeasurable(runr, RaceConfiguration::BASE_STATION_START);

            if (!objStart.empty()) {
                arrayJson.append(objStart);
            }
        }

        int baseId = p_conf.GetCurrentBaseStation().GetId();
        QJsonObject obj = createJsonForIMeasurable(runr, baseId);

        if (!obj.empty()) {
            arrayJson.append(obj);
        }
    }

    if (!arrayJson.empty()) {
        rootDoc[JSON_RACE_TIMES] = arrayJson;
        QJsonDocument jsonDoc(rootDoc);
        QString json(jsonDoc.toJson(QJsonDocument::Compact));

        QUrlQuery postData;
        postData.addQueryItem(UPDATE_TIME_PARAM, json);

        doRequest(postData);
    }

    // Ask for update
    QUrlQuery postData;
    postData.addQueryItem(TIME_PARAM, QString::number(p_conf.GetRaceID()));

    doRequest(postData);
}

void ServerBinder::syncCompetitors(const std::vector<ICompetitorPtr>& p_competitors, const RaceConfiguration& p_conf)
{
    QJsonObject rootDoc;
    rootDoc[JSON_RACE_ID] = p_conf.GetRaceID();

    QJsonArray arrayJson;

    for (ICompetitorPtr runr : p_competitors) {
        QJsonObject obj;
        obj.insert(JSON_ID, runr->GetDatabaseId());
        obj.insert(JSON_BIBNUMBER, runr->GetBibNumber());

        if (p_conf.GetRaceType() == Race::TEAM) {
            TeamPtr team = std::dynamic_pointer_cast<Team>(runr);
            obj.insert(JSON_TEAM_NAME, team->GetTeamName());
        } else {
            RunnerPtr runerPtr = std::dynamic_pointer_cast<Runner>(runr);
            obj.insert(JSON_FIRST_NAME, runerPtr->m_firstName);
            obj.insert(JSON_LAST_NAME, runerPtr->m_lastName);
            obj.insert(JSON_BIRTH_DATE, runerPtr->m_birthDate.toString("dd/MM/yyyy"));
            obj.insert(JSON_SEXE, (runerPtr->GetSexe() == ICompetitor::eSexe::MALE) ? "M" : "F");
        }
        arrayJson.append(obj);
    }

    if (!arrayJson.empty()) {
        rootDoc[JSON_COMPETITORS] = arrayJson;
        QJsonDocument jsonDoc(rootDoc);
        QString json(jsonDoc.toJson(QJsonDocument::Compact));

        QUrlQuery postData;
        postData.addQueryItem(SYNC_COMPETITORS_PARAM, json);

        doRequest(postData);
        qDebug() << "Http Request json: " << json;
    }
}

void ServerBinder::isNetworkAccessible()
{
    QUrlQuery postData;
    postData.addQueryItem(PING_PARAM, "1");
    doRequest(postData);
}

QJsonObject ServerBinder::createJsonForIMeasurable(ICompetitorPtr p_measurable, const int& p_baseID)
{
    QJsonObject obj;
    SplitTime splt = p_measurable->GetSplitTimeFromBaseStation(p_baseID);

    if (splt.GetBaseId() != -1) {
        obj.insert(JSON_ID, p_measurable->GetDatabaseId());
        obj.insert(JSON_TIME, splt.GetTime().toString());
        obj.insert(JSON_BASE_STATION, p_baseID);
        obj.insert(JSON_STATE, p_measurable->GetState());
        obj.insert(JSON_STATE_COUNTER, p_measurable->GetCompetitorState().GetStateCount());
    }

    return obj;
}

void ServerBinder::processServerAnswer(const QByteArray& data)
{
    const auto dataStr = QString(data);
    if (dataStr.isEmpty()) {
        return;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(dataStr.toUtf8());
    auto requestType = jsonDoc.object().value(JSON_REQUEST_TYPE).toString();

    if (requestType == TIME_PARAM) {
        processWebsiteUpdateTime(jsonDoc);
    } else if (requestType == PING_PARAM) {
        qInfo() << "Ping replied - Everything is setup";
    } else if (requestType == CLEAR_TIME_PARAM) {
        auto status = jsonDoc.object().value(JSON_STATUS).toString();
        qDebug() << "Clear live timing status:" << status;
    } else {
        qWarning() << "Not processing:" << dataStr;
    }
}

void ServerBinder::processWebsiteUpdateTime(const QJsonDocument& jsonDoc)
{
    qDebug() << "Receive update time: [" << jsonDoc << "]";
    std::list<SplitTime> times;
    int raceID = jsonDoc.object().value(JSON_RACE_ID).toString().toInt();

    QJsonArray arrayJson = jsonDoc.object().value(JSON_RACE_TIMES).toArray();

    std::map<int, CompetitorState> runnersState;
    for (auto raw : arrayJson) {
        QJsonObject obj = raw.toObject();

        int idRunner = obj.value(JSON_ID).toString().toInt();
        std::map<int, QTime> splitTimes;

        CompetitorState state(
            (CompetitorState::eState)obj.value(JSON_STATE).toString().toInt(),
            obj.value(JSON_STATE_COUNTER).toString().toInt());
        runnersState[idRunner] = state;

        for (auto split : obj.keys()) {
            if (split.contains(JSON_TIME_BASE_STATION)) {
                QString keyTime(split);
                int baseID = split.remove(JSON_TIME_BASE_STATION).toInt() - 1;
                splitTimes[baseID] = QTime::fromString(obj.value(keyTime).toString());
            }
        }
        for (auto tm : splitTimes) {
            if (tm.second.isValid()) {
                SplitTime splt(tm.first, idRunner);
                splt.SetTime(tm.second);

                times.push_back(splt);
            }
        }
    }

    // emit signals with updated list
    emit stateUpdated(raceID, runnersState);
    emit timeUpdated(raceID, times);
}

void ServerBinder::onReplyFinished(QNetworkReply* p_networkRequest)
{
    if (p_networkRequest->error() == QNetworkReply::NoError) {
        emit networkStatus(true);
        if (p_networkRequest->url().toString() == mNetworkRequest.url().toString()) {
            processServerAnswer(p_networkRequest->readAll());
        } else {
            qDebug()
                << "Url received not catched: " << p_networkRequest->url() << "Data: " << p_networkRequest->readAll();
        }
    } else {
        qDebug() << "Network error: " << p_networkRequest->url() << "Error: " << p_networkRequest->errorString();
        emit networkStatus(false);
    }
}

void ServerBinder::onSSLError(QNetworkReply* p_networkRequest, const QList<QSslError>& p_errors)
{
    qWarning() << "Error code: " << p_networkRequest->error();
    qWarning() << "Error SSL: " << p_errors;
}

void ServerBinder::doRequest(QUrlQuery& urlParam)
{
    urlParam.addQueryItem(HASH_PARAM, mHashKey);
    mNetworkManager.post(mNetworkRequest, urlParam.toString(QUrl::FullyEncoded).toUtf8());
}
