// (C) king.com Ltd 2019

#pragma once
#include <memory>
class IDetector;

namespace DetectorFactory{
    std::unique_ptr<IDetector> buildFromSettings();
    std::unique_ptr<IDetector> buildBluetooth();
}





