// (C) king.com Ltd 2019

#pragma once

#include <qdatetime.h>
#include <qobject.h>

class IDetector : public QObject {
    Q_OBJECT
public:
    IDetector() = default;
    virtual ~IDetector() = default;

signals:
    void NewTag(QString, QTime);
};