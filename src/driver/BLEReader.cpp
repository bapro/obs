// (C) king.com Ltd 2020

#include "BLEReader.h"

namespace {
const QString BLE_SERVICE_TAG("0x2a46");
}

void BLEReader::connect(const QBluetoothDeviceInfo& device)
{
    auto itFind = std::find_if(mDevices.begin(), mDevices.end(), [device](const auto& dev) {
        return dev == device;
    });

    if (itFind == mDevices.end()) {
        auto& bleDevice = mDevices.emplace_back(device);
        QObject::connect(&bleDevice, &BLEDevice::servicesDiscoveryFinished, [this, devicePtr = &bleDevice]() {
            BLEReader::OnServicesDiscoveryFinished(devicePtr);
        });
        QObject::connect(&bleDevice, &BLEDevice::characteristicsDiscoveryFinished, [this, devicePtr = &bleDevice]() {
            BLEReader::OnCharacteristicsDiscoveryFinished(devicePtr);
        });
        QObject::connect(&bleDevice, &BLEDevice::updateChanged, [this](QString text) { emit updateChanged(text); });
        QObject::connect(&bleDevice, &BLEDevice::disconnected, this, &BLEReader::deviceDisconnected);
        bleDevice.link();
    } else {
        itFind->link();
    }
}

void BLEReader::disconnect(const QBluetoothDeviceInfo& device)
{
    auto itFind = std::find_if(mDevices.begin(), mDevices.end(), [device](const auto& dev) {
        return dev == device;
    });

    if (itFind != mDevices.end()) {
        itFind->setAutoRelink(false);
        itFind->unlink();
    } else {
        qWarning() << "Unable to disconnect : device not found";
    }
}
void BLEReader::disconnect()
{
    for (auto& bleDevice : mDevices) {
        bleDevice.setAutoRelink(false);
        bleDevice.unlink();
    }
}

void BLEReader::OnServicesDiscoveryFinished(BLEDevice* device)
{
    for (auto srv : device->getServices()) {

        //        auto content = m_ui.m_debugConsole->toPlainText();
        //        m_ui.m_debugConsole->setText(content.append(
        //            srv->getName() + "-" + srv->getType() + "-" + srv->getUuid() + " - "
        //            + QString::number(srv->service()->state()) + "\r"));

        if (srv->getUuid() == BLE_SERVICE_TAG) {
            device->connectToService(srv);
        }
    }
}

void BLEReader::OnCharacteristicsDiscoveryFinished(BLEDevice* device)
{
    auto characs = device->getCharacteristics();
    for (auto charac : characs) {

        //        auto content = m_ui.m_debugConsole->toPlainText();
        //        m_ui.m_debugConsole->setText(
        //            content.append(charac->getName() + "-" + charac->getValue() + "-" + charac->getUuid() + "\r"));
        auto notif = charac->getCharacteristic().descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        if (notif.isValid()) {
            qDebug() << "Connected to tag notification";
            auto srv = device->getServices().front()->service();
            QObject::connect(srv, &QLowEnergyService::characteristicChanged, this, &BLEReader::OnNotification);
            srv->writeDescriptor(notif, QByteArray::fromHex("0100"));
            device->setAutoRelink(true);
        }
    }
    emit deviceConnected();
}

void BLEReader::OnNotification(QLowEnergyCharacteristic, QByteArray data)
{
    //    auto content = m_ui.m_debugConsole->toPlainText();
    //    m_ui.m_debugConsole->setText(charac.uuid().toString() + " - " + data);
    //    bool status = false;
    //    QString val(QString::number(data.toUInt(&status,16)));
    QString strData(data);
    strData.remove(0, 2);
    bool status = false;
    auto nHex = strData.toULongLong(&status, 16);
    QString val(QString::number(nHex));
    qDebug() << "data" << val;

    emit NewTag(val, QTime::currentTime());
}
bool BLEReader::isConnected(const QBluetoothDeviceInfo& bleInfo)
{
    auto itFind = std::find_if(
        mDevices.begin(), mDevices.end(), [bleInfo](const auto& device) { return device == bleInfo; });

    if (itFind != mDevices.end()) {
        return itFind->state() == BLEDevice::State::CONNECTED;
    }
    return false;
}
