/*
 * ServerBinder.h
 *
 *  Created on: 5 févr. 2016
 *      Author: baptiste
 */

#pragma once

#include "QtNetwork/qnetworkaccessmanager.h"
#include "QtNetwork/qnetworkreply.h"
#include "QtNetwork/qsslsocket.h"
#include "qobject.h"
#include <list>
#include <map>
#include <memory>
#include <mutex>

#include "CompetitorState.h"
#include "Runner.h"
#include "Team.h"

class RaceConfiguration;
typedef std::map<int, CompetitorState> MapIdCompetitorState;

Q_DECLARE_METATYPE(std::list<Runner>);
Q_DECLARE_METATYPE(std::list<Team>);
Q_DECLARE_METATYPE(std::list<SplitTime>);
Q_DECLARE_METATYPE(MapIdCompetitorState);

class ServerBinder final : public QObject {
    Q_OBJECT

public:
    ServerBinder(const QString& server, const QString& hash);
    ~ServerBinder();

    void sendTime(const std::vector<ICompetitorPtr>& p_timeList, const RaceConfiguration& p_conf);

    void clearLiveTiming(const int& p_raceId);

    void syncCompetitors(const std::vector<ICompetitorPtr>& p_competitors, const RaceConfiguration& p_conf);

    void isNetworkAccessible();

signals:
    void timeUpdated(const int& p_raceId, const std::list<SplitTime>& p_timesList);
    void stateUpdated(const int& p_raceId, const MapIdCompetitorState& p_updateStateMap);
    void networkStatus(bool);

private:
    QJsonObject createJsonForIMeasurable(ICompetitorPtr p_measurable, const int& p_baseID);

    void processServerAnswer(const QByteArray& data);

    void processWebsiteUpdateTime(const QJsonDocument& jsonDoc);

    void doRequest(QUrlQuery& urlParam);

    void onReplyFinished(QNetworkReply* p_networkRequest);

    void onSSLError(QNetworkReply* p_networkRequest, const QList<QSslError>& p_errors);

private:
    QNetworkAccessManager mNetworkManager;
    QNetworkRequest mNetworkRequest;
    QString mHashKey;
};
