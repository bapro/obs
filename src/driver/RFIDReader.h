/*
 * RFIDReader.h
 *
 *  Created on: 28 févr. 2016
 *      Author: baptiste
 */

#pragma once

#include "IDetector.h"
#include <QtSerialPort/qserialport.h>
#include <qdatetime.h>

class RFIDReader : public IDetector {
    Q_OBJECT
public:
    RFIDReader();
    virtual ~RFIDReader();

    QSerialPort::SerialPortError SetSerialPort(const QSerialPortInfo& p_serialInfo);

    bool IsConnected();

    QSerialPort::SerialPortError InitSerialPort(const QSerialPortInfo& p_serialInfo);

    QList<QSerialPortInfo> GetSerialAvailabe();

    void SetBeepOnRead(const bool& p_beep);

    bool IsBeepOnRead() { return m_beepOnRead; }

    QString GetSerialPortName() const;

signals:
    void NewDataRead(QString p_rfidTag);

public slots:
    void ClearBuffer();
    void SimulateRfidTag();

private slots:
    void OnNewData();
    void OnNewTag(QString p_strTag);

private:
    QSerialPort* m_serialPort;
    QString m_buffer;
    bool m_beepOnRead;
    QString m_latestTag;
    QTime m_timerBeep;
};
