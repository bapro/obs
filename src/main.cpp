#include <QMessageBox>
#include <qapplication.h>
#include <qfile.h>
#include <qtranslator.h>
#include <qpushbutton.h>

#include "OBSMainView.h"
#include "Settings.h"

const QString logFilePath = "obs.log";

bool stLogToFile = false;

void customMessageOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    QHash<QtMsgType, QString> msgLevelHash({ { QtDebugMsg, "Debug" },
                                             { QtInfoMsg, "Info" },
                                             { QtWarningMsg, "Warning" },
                                             { QtCriticalMsg, "Critical" },
                                             { QtFatalMsg, "Fatal" } });
    QByteArray localMsg = msg.toLocal8Bit();
    QTime time = QTime::currentTime();
    QString formattedTime = time.toString("hh:mm:ss.zzz");
    QByteArray formattedTimeMsg = formattedTime.toLocal8Bit();
    QString logLevelName = msgLevelHash[type];
    QByteArray logLevelMsg = logLevelName.toLocal8Bit();

    if (stLogToFile) {
        QString txt = QString("%1 %2: %3 (%4)").arg(formattedTime, logLevelName, msg, context.file);
        QFile outFile(logFilePath);
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);
        ts << txt << endl;
        outFile.close();
    } else {
        fprintf(
            stderr,
            "%s %s: %s (%s:%u, %s)\n",
            formattedTimeMsg.constData(),
            logLevelMsg.constData(),
            localMsg.constData(),
            context.file,
            context.line,
            context.function);
        fflush(stderr);
    }

    if (type == QtFatalMsg)
        abort();
}

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    QApplication::setApplicationDisplayName("OBS - Timing");
    QApplication::setApplicationName("OBSTiming");
    QApplication::setApplicationVersion("1.0");
    QApplication::setOrganizationName("RobertCorp");

    const auto logCustom = Settings::instance()->value<bool>(SETTINGS_LOG_CUSTOM, true);
    if (logCustom) {
        stLogToFile = Settings::instance()->value<bool>(SETTINGS_LOG_FILE, true);
        qInstallMessageHandler(customMessageOutput);
    }

    //    QString styleName = Settings::instance()->value<QString>(SETTINGS_SKIN_STYLE, "white");
    // Set style black sheet
    //    if(styleName.compare("black") == 0)
    //    {
    //		QFile File(":/qStyle/style_dark.css");
    //		File.open(QFile::ReadOnly);
    //		QString StyleSheet = QLatin1String(File.readAll());
    //		qApp->setStyleSheet(StyleSheet);
    //    }
    //    if(styleName.compare("white") == 0)
    //    {
    QFile File(":/qStyle/style_white.css");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    qApp->setStyleSheet(StyleSheet);
    //    }

    // Style sheet
    //    QFile File("src/style_white.css");
    //	File.open(QFile::ReadOnly);
    //	QString StyleSheet = QLatin1String(File.readAll());
    //	qApp->setStyleSheet(StyleSheet);

    QTranslator translator;
    translator.load(":/qLanguages/rc/languages/OBS_fr.qm");
    app.installTranslator(&translator);

    OBSMainView mainApp;
    mainApp.show();

    return app.exec();
}
